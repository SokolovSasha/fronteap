(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/app.component.css":
/*!***********************************!*\
  !*** ./src/app/app.component.css ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "#esb-header {\r\n  text-align: left;\r\n  color: #0079c1;\r\n}\r\n\r\n#canvas {\r\n  border: 1px solid black;\r\n  position: absolute;\r\n  z-index: 10000;\r\n}\r\n\r\n#flake {\r\n  color: #fff;\r\n  position: absolute;\r\n  font-size: 25px;\r\n  top: -50px;\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvYXBwLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxnQkFBZ0I7RUFDaEIsY0FBYztBQUNoQjs7QUFFQTtFQUNFLHVCQUF1QjtFQUN2QixrQkFBa0I7RUFDbEIsY0FBYztBQUNoQjs7QUFHQTtFQUNFLFdBQVc7RUFDWCxrQkFBa0I7RUFDbEIsZUFBZTtFQUNmLFVBQVU7QUFDWiIsImZpbGUiOiJzcmMvYXBwL2FwcC5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiI2VzYi1oZWFkZXIge1xyXG4gIHRleHQtYWxpZ246IGxlZnQ7XHJcbiAgY29sb3I6ICMwMDc5YzE7XHJcbn1cclxuXHJcbiNjYW52YXMge1xyXG4gIGJvcmRlcjogMXB4IHNvbGlkIGJsYWNrO1xyXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICB6LWluZGV4OiAxMDAwMDtcclxufVxyXG5cclxuXHJcbiNmbGFrZSB7XHJcbiAgY29sb3I6ICNmZmY7XHJcbiAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gIGZvbnQtc2l6ZTogMjVweDtcclxuICB0b3A6IC01MHB4O1xyXG59XHJcbiJdfQ== */"

/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<nav class=\"navbar navbar-expand-lg navbar-light bg-light\">\r\n  <a class=\"navbar-brand\" routerLink=\"/\">\r\n    <img alt=\"\" src=\"assets/images/logo-svg.svg\">\r\n  </a>\r\n  <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarSupportedContent\"\r\n          aria-controls=\"navbarSupportedContent\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">\r\n    <span class=\"navbar-toggler-icon\"></span>\r\n  </button>\r\n  <div class=\"collapse navbar-collapse\" id=\"navbarSupportedContent\">\r\n    <ul class=\"navbar-nav mr-auto\">\r\n      <li class=\"nav-item\">\r\n        <a class=\"nav-link\" routerLink=\"/scout\" routerLinkActive=\"active\">СКАУТ </a>\r\n      </li>\r\n      <li class=\"nav-item\">\r\n        <a class=\"nav-link\" routerLink=\"/crm\" routerLinkActive=\"active\">Личный кабинет</a>\r\n      </li>\r\n      <li class=\"nav-item\">\r\n      <a class=\"nav-link\" routerLink=\"/mc\" routerLinkActive=\"active\">Мобильная карта</a>\r\n      </li>\r\n      <li class=\"nav-item\">\r\n        <a class=\"nav-link\" routerLink=\"/etran\" routerLinkActive=\"active\">Этран</a>\r\n      </li>\r\n      <li class=\"nav-item\">\r\n        <a class=\"nav-link\" routerLink=\"/ofd\" routerLinkActive=\"active\">ОФД</a>\r\n      </li>\r\n      <li class=\"nav-item\">\r\n        <a class=\"nav-link\" routerLink=\"/monitoring\" routerLinkActive=\"active\">Мониторинг</a>\r\n      </li>\r\n      <li class=\"nav-item\">\r\n        <a class=\"nav-link\" routerLink=\"/wiki/esb\" routerLinkActive=\"active\">Wiki ESB</a>\r\n      </li>\r\n    </ul>\r\n  </div>\r\n\r\n  <h1 id=\"esb-header\" class=\"esb-header\">ESB сервисная шина</h1>\r\n  <!--<img alt=\"\" src=\"assets/images/snow_tree.png\"  height=\"60px\">-->\r\n\r\n  <div class=\"btn-group dropleft\">\r\n    <button type=\"button\" class=\"btn btn-secondary dropdown-toggle\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">\r\n      {{this.authService.roles?.preferred_username || \"Аккаунт\"}}\r\n    </button>\r\n    <div class=\"dropdown-menu\">\r\n      <li class=\"dropdown-item\"><a routerLink=\"/signin\">Войти DMZ => KSPD</a></li>\r\n      <li class=\"dropdown-item\"><a routerLink=\"/signin_dmz\">Войти DMZ</a></li>\r\n      <li class=\"dropdown-item\"><a routerLink=\"/signin_kspd\">Войти KSPD</a></li>\r\n      <li class=\"dropdown-item\"><a routerLink=\"/signin2\">Двухэтапная</a></li>\r\n        <li *ngFor=\"let item of authService.arrayRoles\" class=\"dropdown-item\"><a style=\"cursor: pointer;\">{{item}}</a>\r\n        </li>\r\n        <li><a\r\n                (click)=\"authService.getRoles()\"\r\n                *ngIf=\"authService.isAuthenticated()\"\r\n                class=\"dropdown-item\"\r\n                class=\"nav-item dropdown-item\">Обновить роли</a>\r\n        </li>\r\n        <li><a\r\n                (click)=\"authService.logout()\"\r\n                *ngIf=\"authService.isAuthenticated()\"\r\n                class=\"dropdown-item\"\r\n                class=\"nav-item dropdown-item\"><b>Выйти</b></a>\r\n        </li>\r\n    </div>\r\n  </div>\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n</nav>\r\n\r\n<router-outlet></router-outlet>\r\n\r\n\r\n\r\n\r\n"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_auth_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./services/auth.service */ "./src/app/services/auth.service.ts");



var AppComponent = /** @class */ (function () {
    function AppComponent(authService) {
        this.authService = authService;
        console.warn("Можешь лучше - пиши нам esb_drcp@gazprom-neft.ru");
    }
    AppComponent.prototype.updateMenuLabel = function () {
        if (this.authService.roles != null && this.authService.roles.preferred_username != null) {
            this.menuLabel = this.authService.roles.preferred_username;
        }
        else {
            this.menuLabel = 'Управление';
        }
    };
    AppComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.css */ "./src/app/app.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_auth_service__WEBPACK_IMPORTED_MODULE_2__["AuthService"]])
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var primeng_primeng__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! primeng/primeng */ "./node_modules/primeng/primeng.js");
/* harmony import */ var primeng_primeng__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(primeng_primeng__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/platform-browser/animations */ "./node_modules/@angular/platform-browser/fesm5/animations.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _services_scout_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./services/scout.service */ "./src/app/services/scout.service.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var primeng_table__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! primeng/table */ "./node_modules/primeng/table.js");
/* harmony import */ var primeng_table__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(primeng_table__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _pages_crm_crm_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./pages/crm/crm.component */ "./src/app/pages/crm/crm.component.ts");
/* harmony import */ var _pages_scout_scout_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./pages/scout/scout.component */ "./src/app/pages/scout/scout.component.ts");
/* harmony import */ var _pages_scout_unit_picker_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./pages/scout/unit-picker.component */ "./src/app/pages/scout/unit-picker.component.ts");
/* harmony import */ var _services_crm_service__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./services/crm.service */ "./src/app/services/crm.service.ts");
/* harmony import */ var primeng_toast__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! primeng/toast */ "./node_modules/primeng/toast.js");
/* harmony import */ var primeng_toast__WEBPACK_IMPORTED_MODULE_15___default = /*#__PURE__*/__webpack_require__.n(primeng_toast__WEBPACK_IMPORTED_MODULE_15__);
/* harmony import */ var _pages_mc_mc_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./pages/mc/mc.component */ "./src/app/pages/mc/mc.component.ts");
/* harmony import */ var _services_mc_service__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./services/mc.service */ "./src/app/services/mc.service.ts");
/* harmony import */ var _pages_preloader_preloader_component__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./pages/preloader/preloader.component */ "./src/app/pages/preloader/preloader.component.ts");
/* harmony import */ var _auth_signin_signin_component__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ./auth/signin/signin.component */ "./src/app/auth/signin/signin.component.ts");
/* harmony import */ var _services_auth_service__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ./services/auth.service */ "./src/app/services/auth.service.ts");
/* harmony import */ var _pages_404_pagenotfound_component__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ./pages/404/pagenotfound.component */ "./src/app/pages/404/pagenotfound.component.ts");
/* harmony import */ var _pages_monitoring_monitoring_component__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! ./pages/monitoring/monitoring.component */ "./src/app/pages/monitoring/monitoring.component.ts");
/* harmony import */ var _pages_wiki_wiki_module__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! ./pages/wiki/wiki.module */ "./src/app/pages/wiki/wiki.module.ts");
/* harmony import */ var _auth_signin2_signin2_component__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! ./auth/signin2/signin2.component */ "./src/app/auth/signin2/signin2.component.ts");
/* harmony import */ var _pages_etran_etran_component__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(/*! ./pages/etran/etran.component */ "./src/app/pages/etran/etran.component.ts");
/* harmony import */ var _services_etran_service__WEBPACK_IMPORTED_MODULE_26__ = __webpack_require__(/*! ./services/etran.service */ "./src/app/services/etran.service.ts");
/* harmony import */ var _pages_mailer_mailer_component__WEBPACK_IMPORTED_MODULE_27__ = __webpack_require__(/*! ./pages/mailer/mailer.component */ "./src/app/pages/mailer/mailer.component.ts");
/* harmony import */ var _auth_signindmz_signindmz_component__WEBPACK_IMPORTED_MODULE_28__ = __webpack_require__(/*! ./auth/signindmz/signindmz.component */ "./src/app/auth/signindmz/signindmz.component.ts");
/* harmony import */ var _auth_signinkspd_signinkspd_component__WEBPACK_IMPORTED_MODULE_29__ = __webpack_require__(/*! ./auth/signinkspd/signinkspd.component */ "./src/app/auth/signinkspd/signinkspd.component.ts");
/* harmony import */ var _pages_ofd_ofd_component__WEBPACK_IMPORTED_MODULE_30__ = __webpack_require__(/*! ./pages/ofd/ofd.component */ "./src/app/pages/ofd/ofd.component.ts");
/* harmony import */ var _services_ofd_service__WEBPACK_IMPORTED_MODULE_31__ = __webpack_require__(/*! ./services/ofd.service */ "./src/app/services/ofd.service.ts");
































var routes = [
    { path: '', redirectTo: '/wiki/esb', pathMatch: 'full' },
    { path: 'scout', component: _pages_scout_scout_component__WEBPACK_IMPORTED_MODULE_12__["ScoutComponent"] },
    { path: 'crm', component: _pages_crm_crm_component__WEBPACK_IMPORTED_MODULE_11__["CrmComponent"] },
    { path: 'mc', component: _pages_mc_mc_component__WEBPACK_IMPORTED_MODULE_16__["McComponent"] },
    { path: 'etran', component: _pages_etran_etran_component__WEBPACK_IMPORTED_MODULE_25__["EtranComponent"] },
    { path: 'monitoring', component: _pages_monitoring_monitoring_component__WEBPACK_IMPORTED_MODULE_22__["MonitoringComponent"] },
    { path: 'ofd', component: _pages_ofd_ofd_component__WEBPACK_IMPORTED_MODULE_30__["OfdComponent"] },
    { path: 'signin', component: _auth_signin_signin_component__WEBPACK_IMPORTED_MODULE_19__["SigninComponent"] },
    { path: 'signin2', component: _auth_signin2_signin2_component__WEBPACK_IMPORTED_MODULE_24__["Signin2Component"] },
    { path: 'signin_dmz', component: _auth_signindmz_signindmz_component__WEBPACK_IMPORTED_MODULE_28__["SignindmzComponent"] },
    { path: 'signin_kspd', component: _auth_signinkspd_signinkspd_component__WEBPACK_IMPORTED_MODULE_29__["SigninkspdComponent"] },
    { path: '**', component: _pages_404_pagenotfound_component__WEBPACK_IMPORTED_MODULE_21__["PagenotfoundComponent"] }
];
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_3__["AppComponent"],
                _pages_crm_crm_component__WEBPACK_IMPORTED_MODULE_11__["CrmComponent"],
                _pages_mc_mc_component__WEBPACK_IMPORTED_MODULE_16__["McComponent"],
                _pages_404_pagenotfound_component__WEBPACK_IMPORTED_MODULE_21__["PagenotfoundComponent"],
                _pages_scout_scout_component__WEBPACK_IMPORTED_MODULE_12__["ScoutComponent"],
                _auth_signin_signin_component__WEBPACK_IMPORTED_MODULE_19__["SigninComponent"],
                _pages_scout_unit_picker_component__WEBPACK_IMPORTED_MODULE_13__["UnitPickerComponent"],
                _pages_preloader_preloader_component__WEBPACK_IMPORTED_MODULE_18__["PreloaderComponent"],
                _pages_monitoring_monitoring_component__WEBPACK_IMPORTED_MODULE_22__["MonitoringComponent"],
                _auth_signin2_signin2_component__WEBPACK_IMPORTED_MODULE_24__["Signin2Component"],
                _pages_etran_etran_component__WEBPACK_IMPORTED_MODULE_25__["EtranComponent"],
                _pages_mailer_mailer_component__WEBPACK_IMPORTED_MODULE_27__["MailerComponent"],
                _auth_signindmz_signindmz_component__WEBPACK_IMPORTED_MODULE_28__["SignindmzComponent"],
                _auth_signinkspd_signinkspd_component__WEBPACK_IMPORTED_MODULE_29__["SigninkspdComponent"],
                _pages_ofd_ofd_component__WEBPACK_IMPORTED_MODULE_30__["OfdComponent"]
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["BrowserModule"],
                _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_5__["BrowserAnimationsModule"],
                primeng_primeng__WEBPACK_IMPORTED_MODULE_4__["AccordionModule"],
                primeng_primeng__WEBPACK_IMPORTED_MODULE_4__["CheckboxModule"],
                primeng_primeng__WEBPACK_IMPORTED_MODULE_4__["ChipsModule"],
                primeng_primeng__WEBPACK_IMPORTED_MODULE_4__["InputTextModule"],
                primeng_toast__WEBPACK_IMPORTED_MODULE_15__["ToastModule"],
                primeng_primeng__WEBPACK_IMPORTED_MODULE_4__["PaginatorModule"],
                primeng_primeng__WEBPACK_IMPORTED_MODULE_4__["DialogModule"],
                primeng_table__WEBPACK_IMPORTED_MODULE_9__["TableModule"],
                primeng_primeng__WEBPACK_IMPORTED_MODULE_4__["TabViewModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_8__["HttpClientModule"],
                primeng_primeng__WEBPACK_IMPORTED_MODULE_4__["InputSwitchModule"],
                primeng_primeng__WEBPACK_IMPORTED_MODULE_4__["CalendarModule"],
                primeng_primeng__WEBPACK_IMPORTED_MODULE_4__["PanelModule"],
                primeng_primeng__WEBPACK_IMPORTED_MODULE_4__["PanelMenuModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_6__["FormsModule"],
                primeng_primeng__WEBPACK_IMPORTED_MODULE_4__["ButtonModule"],
                primeng_primeng__WEBPACK_IMPORTED_MODULE_4__["GMapModule"],
                primeng_primeng__WEBPACK_IMPORTED_MODULE_4__["DropdownModule"],
                _pages_wiki_wiki_module__WEBPACK_IMPORTED_MODULE_23__["WikiModule"],
                primeng_primeng__WEBPACK_IMPORTED_MODULE_4__["TooltipModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_10__["RouterModule"].forRoot(routes, { useHash: true })
            ],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_10__["RouterModule"]],
            providers: [_services_auth_service__WEBPACK_IMPORTED_MODULE_20__["AuthService"], _services_scout_service__WEBPACK_IMPORTED_MODULE_7__["ScoutService"], _services_crm_service__WEBPACK_IMPORTED_MODULE_14__["CrmService"], _services_mc_service__WEBPACK_IMPORTED_MODULE_17__["McService"], _services_etran_service__WEBPACK_IMPORTED_MODULE_26__["EtranService"], _services_ofd_service__WEBPACK_IMPORTED_MODULE_31__["OfdService"]],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_3__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/auth/signin/signin.component.css":
/*!**************************************************!*\
  !*** ./src/app/auth/signin/signin.component.css ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2F1dGgvc2lnbmluL3NpZ25pbi5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/auth/signin/signin.component.html":
/*!***************************************************!*\
  !*** ./src/app/auth/signin/signin.component.html ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p-toast [style]=\"{marginTop: '80px'}\"></p-toast>\r\n<div class=\"container-fluid align-self-end\">\r\n    <p-panel header=\"Авторизация\">\r\n        <p-tabView>\r\n    <p-tabPanel header=\"Вход\">\r\n        <div class=\"row\">\r\n            <div class=\"col-12 col-sm-10 col-md-8 col-lg-6 offset-1 offset-md-2\">\r\n                <form (ngSubmit)=\"onSignin(f)\" #f=\"ngForm\">\r\n                    <div class=\"form-group\">\r\n                        <label for=\"email\">Пользователь</label>\r\n                        <input type=\"email\" id=\"email\" name=\"email\" ngModel class=\"form-control\">\r\n                    </div>\r\n                    <div class=\"form-group\">\r\n                        <label for=\"password\">Пароль</label>\r\n                        <input\r\n                                type=\"password\"\r\n                                id=\"password\"\r\n                                name=\"password\"\r\n                                ngModel\r\n                                class=\"form-control\">\r\n                    </div>\r\n                    <button class=\"btn btn-primary\" type=\"submit\" [disabled]=\"!f.valid\">Войти</button>\r\n                </form>\r\n            </div>\r\n        </div>\r\n    </p-tabPanel>\r\n\r\n\r\n            <p-tabPanel header=\"Схема взаимодействия\">\r\n                <img class=\"img-fluid\" id=\"scheme-auth\" src=\"assets/images/scheme-auth.png\">\r\n            </p-tabPanel>\r\n            <p-tabPanel header=\"Инфрастуктура\">\r\n                <img class=\"img-fluid\" id=\"infr-auth\" src=\"assets/images/infr-auth.png\">\r\n            </p-tabPanel>\r\n            <p-tabPanel header=\"Swagger\">\r\n                <div class=\"embed-responsive embed-responsive-16by9 e2e-trusted-url\">\r\n                    <iframe class=\"embed-responsive-item\"\r\n                            [src]=\"trustedSwaggerUrl\"></iframe>\r\n                </div>\r\n            </p-tabPanel>\r\n        </p-tabView>\r\n    </p-panel>\r\n</div>"

/***/ }),

/***/ "./src/app/auth/signin/signin.component.ts":
/*!*************************************************!*\
  !*** ./src/app/auth/signin/signin.component.ts ***!
  \*************************************************/
/*! exports provided: SigninComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SigninComponent", function() { return SigninComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_auth_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/auth.service */ "./src/app/services/auth.service.ts");
/* harmony import */ var primeng_api__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! primeng/api */ "./node_modules/primeng/api.js");
/* harmony import */ var primeng_api__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(primeng_api__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../environments/environment */ "./src/environments/environment.ts");






var SigninComponent = /** @class */ (function () {
    function SigninComponent(authService, sanitizer) {
        this.authService = authService;
        this.sanitizer = sanitizer;
        this._swaggerUrl = _environments_environment__WEBPACK_IMPORTED_MODULE_5__["environment"].authUrl + '/webjars/swagger-ui/index.html?url=/auth/api-doc';
        this.trustedSwaggerUrl = sanitizer.bypassSecurityTrustResourceUrl(this._swaggerUrl);
    }
    SigninComponent.prototype.ngOnInit = function () {
    };
    SigninComponent.prototype.onSignin = function (form) {
        var email = form.value.email;
        var password = form.value.password;
        this.authService.signinUser(email, password);
    };
    SigninComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-signin',
            template: __webpack_require__(/*! ./signin.component.html */ "./src/app/auth/signin/signin.component.html"),
            providers: [primeng_api__WEBPACK_IMPORTED_MODULE_3__["MessageService"]],
            styles: [__webpack_require__(/*! ./signin.component.css */ "./src/app/auth/signin/signin.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_auth_service__WEBPACK_IMPORTED_MODULE_2__["AuthService"], _angular_platform_browser__WEBPACK_IMPORTED_MODULE_4__["DomSanitizer"]])
    ], SigninComponent);
    return SigninComponent;
}());



/***/ }),

/***/ "./src/app/auth/signin2/signin2.component.css":
/*!****************************************************!*\
  !*** ./src/app/auth/signin2/signin2.component.css ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2F1dGgvc2lnbmluMi9zaWduaW4yLmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/auth/signin2/signin2.component.html":
/*!*****************************************************!*\
  !*** ./src/app/auth/signin2/signin2.component.html ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p-toast [style]=\"{marginTop: '80px'}\"></p-toast>\r\n<div class=\"container-fluid align-self-end\">\r\n    <p-panel header=\"Двухэтапная авторизация\">\r\n        <p-tabView>\r\n            <p-tabPanel header=\"Вход\">\r\n                <div class=\"row\">\r\n                    <div class=\"col-12 col-sm-10 col-md-8 col-lg-6 offset-1 offset-md-2\">\r\n                        <div *ngIf=\"!authService.codeMode\">\r\n                            <form (ngSubmit)=\"onSignin2(f)\" #f=\"ngForm\">\r\n                                <div class=\"form-group\">\r\n                                    <label for=\"login\">Пользователь</label>\r\n                                    <input type=\"text\" id=\"login\" name=\"login\" ngModel class=\"form-control\">\r\n                                </div>\r\n                                <div class=\"form-group\">\r\n                                    <label for=\"password\">Пароль</label>\r\n                                    <input\r\n                                            type=\"password\"\r\n                                            id=\"password\"\r\n                                            name=\"password\"\r\n                                            ngModel\r\n                                            class=\"form-control\">\r\n                                </div>\r\n                                <button class=\"btn btn-primary\" type=\"submit\" [disabled]=\"!f.valid\">Войти</button>\r\n                            </form>\r\n                        </div>\r\n\r\n                        <div class=\"col-12 col-sm-10 col-md-8 col-lg-6 offset-1 offset-md-1\"\r\n                             *ngIf=\"authService.codeMode\">\r\n                            <form (ngSubmit)=\"onCodeSubmit(f)\" #f=\"ngForm\">\r\n                                <div class=\"form-group\">\r\n                                    <label for=\"secretCode\">Введите код, отправленный Вам на почту</label>\r\n                                    <input type=\"text\" id=\"secretCode\" name=\"secretCode\" ngModel class=\"form-control\">\r\n                                </div>\r\n\r\n\r\n                                <button class=\"btn btn-success\" type=\"submit\" [disabled]=\"!f.valid\">Подтвердить</button>\r\n\r\n                                <button class=\"btn btn-outline-secondary ml-auto\" type=\"button\" (click)=\"returnToLogin()\">Вернуться к\r\n                                    вводу пароля</button>\r\n\r\n                            </form>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </p-tabPanel>\r\n\r\n\r\n            <p-tabPanel header=\"Схема взаимодействия\">\r\n                <img class=\"img-fluid\" id=\"scheme-auth\" src=\"assets/images/scheme-auth.png\">\r\n            </p-tabPanel>\r\n            <p-tabPanel header=\"Инфрастуктура\">\r\n                <img class=\"img-fluid\" id=\"infr-auth\" src=\"assets/images/infr-auth.png\">\r\n            </p-tabPanel>\r\n            <p-tabPanel header=\"Swagger\">\r\n                <div class=\"embed-responsive embed-responsive-16by9 e2e-trusted-url\">\r\n                    <iframe class=\"embed-responsive-item\"\r\n                            [src]=\"trustedSwaggerUrl\"></iframe>\r\n                </div>\r\n            </p-tabPanel>\r\n        </p-tabView>\r\n    </p-panel>\r\n</div>"

/***/ }),

/***/ "./src/app/auth/signin2/signin2.component.ts":
/*!***************************************************!*\
  !*** ./src/app/auth/signin2/signin2.component.ts ***!
  \***************************************************/
/*! exports provided: Signin2Component */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Signin2Component", function() { return Signin2Component; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_auth_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/auth.service */ "./src/app/services/auth.service.ts");
/* harmony import */ var primeng_api__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! primeng/api */ "./node_modules/primeng/api.js");
/* harmony import */ var primeng_api__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(primeng_api__WEBPACK_IMPORTED_MODULE_3__);




var Signin2Component = /** @class */ (function () {
    function Signin2Component(authService) {
        this.authService = authService;
    }
    Signin2Component.prototype.ngOnInit = function () {
        console.log('CODEMODE: ' + this.authService.codeMode);
    };
    Signin2Component.prototype.onSignin2 = function (form) {
        var login = form.value.login;
        var password = form.value.password;
        this.authService.signin2faUser(login, password);
    };
    Signin2Component.prototype.onCodeSubmit = function (form) {
        var secretCode = form.value.secretCode;
        this.authService.checkToken(secretCode);
    };
    Signin2Component.prototype.returnToLogin = function () {
        console.log('CodeMode Off');
        this.authService.codeMode = false;
        console.log('CodeMode Off');
        if (this.authService.isAuthenticated()) {
            console.log('Запуск разлогина!');
            this.authService.logout();
        }
    };
    Signin2Component = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-signin2',
            template: __webpack_require__(/*! ./signin2.component.html */ "./src/app/auth/signin2/signin2.component.html"),
            providers: [primeng_api__WEBPACK_IMPORTED_MODULE_3__["MessageService"]],
            styles: [__webpack_require__(/*! ./signin2.component.css */ "./src/app/auth/signin2/signin2.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_auth_service__WEBPACK_IMPORTED_MODULE_2__["AuthService"]])
    ], Signin2Component);
    return Signin2Component;
}());



/***/ }),

/***/ "./src/app/auth/signindmz/signindmz.component.css":
/*!********************************************************!*\
  !*** ./src/app/auth/signindmz/signindmz.component.css ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2F1dGgvc2lnbmluZG16L3NpZ25pbmRtei5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/auth/signindmz/signindmz.component.html":
/*!*********************************************************!*\
  !*** ./src/app/auth/signindmz/signindmz.component.html ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p-toast [style]=\"{marginTop: '80px'}\"></p-toast>\n<div class=\"container-fluid align-self-end\">\n  <p-panel header=\"Авторизация в DMZ\">\n    <p-tabView>\n      <p-tabPanel header=\"Вход\">\n        <div class=\"row\">\n          <div class=\"col-12 col-sm-10 col-md-8 col-lg-6 offset-1 offset-md-2\">\n            <form (ngSubmit)=\"onSignin(f)\" #f=\"ngForm\">\n              <div class=\"form-group\">\n                <label for=\"email\">Пользователь</label>\n                <input type=\"email\" id=\"email\" name=\"email\" ngModel class=\"form-control\">\n              </div>\n              <div class=\"form-group\">\n                <label for=\"password\">Пароль</label>\n                <input\n                        type=\"password\"\n                        id=\"password\"\n                        name=\"password\"\n                        ngModel\n                        class=\"form-control\">\n              </div>\n              <button class=\"btn btn-primary\" type=\"submit\" [disabled]=\"!f.valid\">Войти</button>\n            </form>\n          </div>\n        </div>\n      </p-tabPanel>\n\n\n      <p-tabPanel header=\"Схема взаимодействия\">\n        <img class=\"img-fluid\" id=\"scheme-auth\" src=\"assets/images/scheme-auth.png\">\n      </p-tabPanel>\n      <p-tabPanel header=\"Инфрастуктура\">\n        <img class=\"img-fluid\" id=\"infr-auth\" src=\"assets/images/infr-auth.png\">\n      </p-tabPanel>\n      <p-tabPanel header=\"Swagger\">\n        <div class=\"embed-responsive embed-responsive-16by9 e2e-trusted-url\">\n          <iframe class=\"embed-responsive-item\"\n                  [src]=\"trustedSwaggerUrl\"></iframe>\n        </div>\n      </p-tabPanel>\n    </p-tabView>\n  </p-panel>\n</div>"

/***/ }),

/***/ "./src/app/auth/signindmz/signindmz.component.ts":
/*!*******************************************************!*\
  !*** ./src/app/auth/signindmz/signindmz.component.ts ***!
  \*******************************************************/
/*! exports provided: SignindmzComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SignindmzComponent", function() { return SignindmzComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_auth_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/auth.service */ "./src/app/services/auth.service.ts");



var SignindmzComponent = /** @class */ (function () {
    function SignindmzComponent(authService) {
        this.authService = authService;
    }
    SignindmzComponent.prototype.ngOnInit = function () {
    };
    SignindmzComponent.prototype.onSignin = function (form) {
        var email = form.value.email;
        var password = form.value.password;
        this.authService.signinUser(email, password);
    };
    SignindmzComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-signindmz',
            template: __webpack_require__(/*! ./signindmz.component.html */ "./src/app/auth/signindmz/signindmz.component.html"),
            styles: [__webpack_require__(/*! ./signindmz.component.css */ "./src/app/auth/signindmz/signindmz.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_auth_service__WEBPACK_IMPORTED_MODULE_2__["AuthService"]])
    ], SignindmzComponent);
    return SignindmzComponent;
}());



/***/ }),

/***/ "./src/app/auth/signinkspd/signinkspd.component.css":
/*!**********************************************************!*\
  !*** ./src/app/auth/signinkspd/signinkspd.component.css ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2F1dGgvc2lnbmlua3NwZC9zaWduaW5rc3BkLmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/auth/signinkspd/signinkspd.component.html":
/*!***********************************************************!*\
  !*** ./src/app/auth/signinkspd/signinkspd.component.html ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p-toast [style]=\"{marginTop: '80px'}\"></p-toast>\n<div class=\"container-fluid align-self-end\">\n  <p-panel header=\"Авторизация в КСПД\">\n    <p-tabView>\n      <p-tabPanel header=\"Вход\">\n        <div class=\"row\">\n          <div class=\"col-12 col-sm-10 col-md-8 col-lg-6 offset-1 offset-md-2\">\n            <form (ngSubmit)=\"onSignin(f)\" #f=\"ngForm\">\n              <div class=\"form-group\">\n                <label for=\"email\">Пользователь</label>\n                <input type=\"email\" id=\"email\" name=\"email\" ngModel class=\"form-control\">\n              </div>\n              <div class=\"form-group\">\n                <label for=\"password\">Пароль</label>\n                <input\n                        type=\"password\"\n                        id=\"password\"\n                        name=\"password\"\n                        ngModel\n                        class=\"form-control\">\n              </div>\n              <button class=\"btn btn-primary\" type=\"submit\" [disabled]=\"!f.valid\">Войти</button>\n            </form>\n          </div>\n        </div>\n      </p-tabPanel>\n\n\n      <p-tabPanel header=\"Схема взаимодействия\">\n        <img class=\"img-fluid\" id=\"scheme-auth\" src=\"assets/images/scheme-auth.png\">\n      </p-tabPanel>\n      <p-tabPanel header=\"Инфрастуктура\">\n        <img class=\"img-fluid\" id=\"infr-auth\" src=\"assets/images/infr-auth.png\">\n      </p-tabPanel>\n      <p-tabPanel header=\"Swagger\">\n        <div class=\"embed-responsive embed-responsive-16by9 e2e-trusted-url\">\n          <iframe class=\"embed-responsive-item\"\n                  [src]=\"trustedSwaggerUrl\"></iframe>\n        </div>\n      </p-tabPanel>\n    </p-tabView>\n  </p-panel>\n</div>"

/***/ }),

/***/ "./src/app/auth/signinkspd/signinkspd.component.ts":
/*!*********************************************************!*\
  !*** ./src/app/auth/signinkspd/signinkspd.component.ts ***!
  \*********************************************************/
/*! exports provided: SigninkspdComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SigninkspdComponent", function() { return SigninkspdComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_auth_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/auth.service */ "./src/app/services/auth.service.ts");



var SigninkspdComponent = /** @class */ (function () {
    function SigninkspdComponent(authService) {
        this.authService = authService;
    }
    SigninkspdComponent.prototype.ngOnInit = function () {
    };
    SigninkspdComponent.prototype.onSignin = function (form) {
        var email = form.value.email;
        var password = form.value.password;
        this.authService.signInKspd(email, password);
    };
    SigninkspdComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-signinkspd',
            template: __webpack_require__(/*! ./signinkspd.component.html */ "./src/app/auth/signinkspd/signinkspd.component.html"),
            styles: [__webpack_require__(/*! ./signinkspd.component.css */ "./src/app/auth/signinkspd/signinkspd.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_auth_service__WEBPACK_IMPORTED_MODULE_2__["AuthService"]])
    ], SigninkspdComponent);
    return SigninkspdComponent;
}());



/***/ }),

/***/ "./src/app/commons/copypaste/copy-paste.component.css":
/*!************************************************************!*\
  !*** ./src/app/commons/copypaste/copy-paste.component.css ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".command {\r\n    color: #660066;\r\n    background-color: floralwhite;\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tbW9ucy9jb3B5cGFzdGUvY29weS1wYXN0ZS5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0lBQ0ksY0FBYztJQUNkLDZCQUE2QjtBQUNqQyIsImZpbGUiOiJzcmMvYXBwL2NvbW1vbnMvY29weXBhc3RlL2NvcHktcGFzdGUuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi5jb21tYW5kIHtcclxuICAgIGNvbG9yOiAjNjYwMDY2O1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogZmxvcmFsd2hpdGU7XHJcbn1cclxuIl19 */"

/***/ }),

/***/ "./src/app/commons/copypaste/copy-paste.component.html":
/*!*************************************************************!*\
  !*** ./src/app/commons/copypaste/copy-paste.component.html ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class = \"row\">\r\n    <!--<input id=\"cpel\" type=\"text\" pInputText [(ngModel)]=\"inputText\"/>-->\r\n    <label class=\"command\">{{inputText}}</label>\r\n    <p-button (onClick)=\"copypaste(inputText)\"\r\n        icon=\"pi pi-copy\"\r\n        pTooltip=\"скопировать команду\"\r\n        tooltipPosition=\"top\"\r\n              style=\"padding-left: 10px\"\r\n        styleClass=\" btn btn-outline-info\"></p-button>\r\n</div>"

/***/ }),

/***/ "./src/app/commons/copypaste/copy-paste.component.ts":
/*!***********************************************************!*\
  !*** ./src/app/commons/copypaste/copy-paste.component.ts ***!
  \***********************************************************/
/*! exports provided: CopyPasteComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CopyPasteComponent", function() { return CopyPasteComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");



var CopyPasteComponent = /** @class */ (function () {
    function CopyPasteComponent(document) {
        this.document = document;
    }
    CopyPasteComponent.prototype.copypaste = function (text) {
        console.log('Fired: ' + text);
        var textArea = document.createElement("textarea");
        textArea.style.position = 'fixed';
        textArea.style.top = '-999px';
        textArea.style.left = '-999px';
        textArea.style.width = '2em';
        textArea.style.height = '2em';
        textArea.style.padding = '0';
        textArea.style.border = 'none';
        textArea.style.outline = 'none';
        textArea.style.boxShadow = 'none';
        textArea.style.background = 'transparent';
        textArea.value = text;
        document.body.appendChild(textArea);
        textArea.select();
        try {
            this.document.execCommand('copy');
        }
        catch (error) {
            console.log('Невозможно скопировать!' + error);
        }
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", String)
    ], CopyPasteComponent.prototype, "inputText", void 0);
    CopyPasteComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-cp',
            template: __webpack_require__(/*! ./copy-paste.component.html */ "./src/app/commons/copypaste/copy-paste.component.html"),
            styles: [__webpack_require__(/*! ./copy-paste.component.css */ "./src/app/commons/copypaste/copy-paste.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](0, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])(_angular_common__WEBPACK_IMPORTED_MODULE_2__["DOCUMENT"])),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object])
    ], CopyPasteComponent);
    return CopyPasteComponent;
}());



/***/ }),

/***/ "./src/app/model/FltGasStationsListRequest.model.ts":
/*!**********************************************************!*\
  !*** ./src/app/model/FltGasStationsListRequest.model.ts ***!
  \**********************************************************/
/*! exports provided: FltGasStationsListRequest */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FltGasStationsListRequest", function() { return FltGasStationsListRequest; });
var FltGasStationsListRequest = /** @class */ (function () {
    function FltGasStationsListRequest() {
    }
    return FltGasStationsListRequest;
}());



/***/ }),

/***/ "./src/app/model/auth/loginrequest.model.ts":
/*!**************************************************!*\
  !*** ./src/app/model/auth/loginrequest.model.ts ***!
  \**************************************************/
/*! exports provided: LoginrequestModel */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginrequestModel", function() { return LoginrequestModel; });
var LoginrequestModel = /** @class */ (function () {
    function LoginrequestModel() {
    }
    return LoginrequestModel;
}());



/***/ }),

/***/ "./src/app/model/auth/logout-request.model.ts":
/*!****************************************************!*\
  !*** ./src/app/model/auth/logout-request.model.ts ***!
  \****************************************************/
/*! exports provided: LogoutRequestModel */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LogoutRequestModel", function() { return LogoutRequestModel; });
var LogoutRequestModel = /** @class */ (function () {
    function LogoutRequestModel() {
    }
    return LogoutRequestModel;
}());



/***/ }),

/***/ "./src/app/model/azs-details-request.model.ts":
/*!****************************************************!*\
  !*** ./src/app/model/azs-details-request.model.ts ***!
  \****************************************************/
/*! exports provided: AzsDetailsRequest */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AzsDetailsRequest", function() { return AzsDetailsRequest; });
var AzsDetailsRequest = /** @class */ (function () {
    function AzsDetailsRequest() {
    }
    return AzsDetailsRequest;
}());



/***/ }),

/***/ "./src/app/model/etran/invoice-request.model.ts":
/*!******************************************************!*\
  !*** ./src/app/model/etran/invoice-request.model.ts ***!
  \******************************************************/
/*! exports provided: InvoiceRequest */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InvoiceRequest", function() { return InvoiceRequest; });
var InvoiceRequest = /** @class */ (function () {
    function InvoiceRequest() {
    }
    return InvoiceRequest;
}());



/***/ }),

/***/ "./src/app/model/ofd/AuthRequest.ts":
/*!******************************************!*\
  !*** ./src/app/model/ofd/AuthRequest.ts ***!
  \******************************************/
/*! exports provided: AuthRequest */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthRequest", function() { return AuthRequest; });
var AuthRequest = /** @class */ (function () {
    function AuthRequest() {
    }
    return AuthRequest;
}());



/***/ }),

/***/ "./src/app/model/ofd/WidgetStatInfo.ts":
/*!*********************************************!*\
  !*** ./src/app/model/ofd/WidgetStatInfo.ts ***!
  \*********************************************/
/*! exports provided: WidgetStatInfo */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WidgetStatInfo", function() { return WidgetStatInfo; });
var WidgetStatInfo = /** @class */ (function () {
    function WidgetStatInfo() {
    }
    return WidgetStatInfo;
}());



/***/ }),

/***/ "./src/app/model/unit-pick.model.ts":
/*!******************************************!*\
  !*** ./src/app/model/unit-pick.model.ts ***!
  \******************************************/
/*! exports provided: UnitPick */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UnitPick", function() { return UnitPick; });
var UnitPick = /** @class */ (function () {
    function UnitPick() {
    }
    return UnitPick;
}());



/***/ }),

/***/ "./src/app/pages/404/pagenotfound.component.css":
/*!******************************************************!*\
  !*** ./src/app/pages/404/pagenotfound.component.css ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzLzQwNC9wYWdlbm90Zm91bmQuY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/pages/404/pagenotfound.component.html":
/*!*******************************************************!*\
  !*** ./src/app/pages/404/pagenotfound.component.html ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h1>Страница не найдена</h1>\r\n\r\n<br>\r\n\r\n<h4> Страницы с таким URL нет.  </h4>\r\n\r\n<h4>Попробуйте ввести в браузере правильную строку, или используйте меню навигации</h4>\r\n\r\n\r\n\r\n<div class = \"container-fluid\">\r\n    <img class=\"img-fluid\" style=\"width: 10%\" id=\"scheme-crm-azsdetails\" src=\"assets/images/maintain/404.jpg\">\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/pages/404/pagenotfound.component.ts":
/*!*****************************************************!*\
  !*** ./src/app/pages/404/pagenotfound.component.ts ***!
  \*****************************************************/
/*! exports provided: PagenotfoundComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PagenotfoundComponent", function() { return PagenotfoundComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var PagenotfoundComponent = /** @class */ (function () {
    function PagenotfoundComponent() {
    }
    PagenotfoundComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-404',
            template: __webpack_require__(/*! ./pagenotfound.component.html */ "./src/app/pages/404/pagenotfound.component.html"),
            styles: [__webpack_require__(/*! ./pagenotfound.component.css */ "./src/app/pages/404/pagenotfound.component.css")]
        })
    ], PagenotfoundComponent);
    return PagenotfoundComponent;
}());



/***/ }),

/***/ "./src/app/pages/crm/crm.component.css":
/*!*********************************************!*\
  !*** ./src/app/pages/crm/crm.component.css ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2NybS9jcm0uY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/pages/crm/crm.component.html":
/*!**********************************************!*\
  !*** ./src/app/pages/crm/crm.component.html ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p-toast [style]=\"{marginTop: '80px'}\"></p-toast>\r\n<p-panel header=\"Получение данных из CRM Siebel\">\r\n    <p-tabView>\r\n        <p-tabPanel header=\"АЗС\">\r\n\r\n\r\n            <div class=\"form-group container\">\r\n\r\n                <div class=\"row\">\r\n                    <label class=\"control-label col-sm-2\">Дата последнего изменения</label>\r\n                    <div class=\"col-sm-2\">\r\n                        <p-calendar [(ngModel)]=\"dateLastChange\"\r\n                                    required=\"true\" dateFormat=\"dd.mm.yy\"></p-calendar>\r\n                    </div>\r\n                </div>\r\n\r\n                <div class=\"row \">\r\n                    <div class=\"col-sm-2\"></div>\r\n                    <p-checkbox\r\n                            class=\"col-sm-4\"\r\n                            [(ngModel)]=\"fuelCardsFilterSelect\"\r\n                            binary=\"true\"\r\n                            label=\"Приём топливных карт\"></p-checkbox>\r\n                </div>\r\n                <div class=\"row\">\r\n                    <div class=\"col-sm-2\"></div>\r\n                    <p-checkbox\r\n                            class=\"col-sm-4\"\r\n                            [(ngModel)]=\"loyalCardsFilterSelect\"\r\n                            binary=\"true\"\r\n                            label=\"Приём карт лояльности\"></p-checkbox>\r\n                </div>\r\n                <div class=\"row\">\r\n                    <div class=\"col-sm-2\"></div>\r\n                    <p-checkbox\r\n                            class=\"col-sm-4\"\r\n                            [(ngModel)]=\"bankCardsFilterSelect\"\r\n                            binary=\"true\"\r\n                            label=\"Приём банковских карт\"></p-checkbox>\r\n                </div>\r\n                <div class=\"row\">\r\n                    <div class=\"col-sm-2\"></div>\r\n                    <p-button (onClick)=\"stationList($event)\"\r\n                              class=\"col-sm-2\"\r\n                              label=\"Список АЗС\"\r\n                              style=\"padding-left: 20px\"\r\n                              styleClass=\"ui-button-success\"></p-button>\r\n                </div>\r\n                <div class=\"row\">\r\n                    <div class=\"col-sm-2\"></div>\r\n                    <input type=\"text\"\r\n                           class=\"col-sm-4\"\r\n                           pInputText\r\n                           [(ngModel)]=\"azsListcorrId\"\r\n                           placeholder=\"CorrelationId\"/>\r\n                </div>\r\n                <div class=\"row\">\r\n                    <div class=\"col-sm-2\"></div>\r\n                    <p-button (onClick)=\"stationListResult($event)\"\r\n                              class=\"col-sm-2\"\r\n                              label=\"Получить данные по CorId\"\r\n                              style=\"padding-left: 20px\"\r\n                              styleClass=\"ui-button-success\"></p-button>\r\n                </div>\r\n            </div>\r\n\r\n            <!--<p-button (onClick)=\"stationDetailsList($event)\"-->\r\n                      <!--class=\"col-sm-2\"-->\r\n                      <!--label=\"Детальный список АЗС\"-->\r\n                      <!--style=\"padding-left: 20px\"-->\r\n                      <!--styleClass=\"ui-button-success\"></p-button>-->\r\n\r\n            <!--<p-button (onClick)=\"details($event)\"-->\r\n            <!--class=\"col-sm-2\"-->\r\n            <!--label=\"Получение деталей\"-->\r\n            <!--style=\"padding-left: 20px\"-->\r\n            <!--styleClass=\"ui-button-success\"></p-button>-->\r\n\r\n            <p-table (onRowSelect)=\"onRowSelect($event)\"\r\n                     [(selection)]=\"selectedAzs\" [paginator]=\"true\"\r\n                     [responsive]=\"true\"\r\n                     [rowsPerPageOptions]=\"[5,10,20,50]\"\r\n                     [rows]=\"5\"\r\n                     [value]=\"azsList?.data\"\r\n                     dataKey=\"ID\"\r\n                     selectionMode=\"single\"\r\n            >\r\n                <ng-template pTemplate=\"header\">\r\n                    <tr>\r\n                        <th scope=\"col-sm\">ID</th>\r\n                        <th scope=\"col-sm\">ContractName</th>\r\n                        <th scope=\"col-sm\">ContractNumber</th>\r\n                        <th scope=\"col-sm\">RegionCode</th>\r\n                        <th scope=\"col-sm\">AddInfo03</th>\r\n                        <th scope=\"col-sm\">TrCountry</th>\r\n                        <th scope=\"col-sm\">FLTCards</th>\r\n                        <th scope=\"col-sm\">LTYCards</th>\r\n                        <th scope=\"col-sm\">GPBCards</th>\r\n                        <th scope=\"col-sm\">Region</th>\r\n                        <th scope=\"col-sm\">Детально</th>\r\n                    </tr>\r\n                </ng-template>\r\n                <ng-template let-dt let-rowIndex=\"ID\" pTemplate=\"body\">\r\n                    <tr [pSelectableRow]=\"dt\">\r\n                        <td>{{dt.ID}} </td>\r\n                        <td>{{dt.ContractName}}</td>\r\n                        <td>{{dt.ContractNumber}}</td>\r\n                        <td>{{dt.RegionCode}}</td>\r\n                        <td>{{dt.AddInfo03}}</td>\r\n                        <td>{{dt.TrCountry}}</td>\r\n                        <td>{{dt.FLTCards}}</td>\r\n                        <td>{{dt.LTYCards}}</td>\r\n                        <td>{{dt.GPBCards}}</td>\r\n                        <td>{{dt.Region}}</td>\r\n                        <td>\r\n                            <div class=\"row\">\r\n                                <p-button (onClick)=\"onRowButton(dt)\" icon=\"pi pi-question\"\r\n                                          style=\"padding-left: 10px\"></p-button>\r\n                                <p-button (onClick)=\"onRowButtonMap(dt)\" icon=\"pi pi-search\"\r\n                                          style=\"padding-left: 10px\"></p-button>\r\n                            </div>\r\n                        </td>\r\n                    </tr>\r\n                </ng-template>\r\n            </p-table>\r\n\r\n            <p-dialog [(visible)]=\"azsDialogVisible\"\r\n                      [width]=\"1200\"\r\n                      header=\"Детальная информация по АЗС\"\r\n                      positionLeft=\"300\"\r\n                      positionTop=\"50\"\r\n            >\r\n\r\n                <p-accordion>\r\n                    <p-accordionTab [selected]=\"true\" header=\"Основные данные\">\r\n\r\n                        <table *ngIf=\"azsDetails?.data\" class=\"table\">\r\n                            <tbody>\r\n                            <tr>\r\n                                <td>ID WAY4</td>\r\n                                <td>{{azsDetails.data.ID}}</td>\r\n                                <td>ID Siebel</td>\r\n                                <td>{{azsDetails.data.SiebelID}}</td>\r\n                            </tr>\r\n                            <tr>\r\n                                <td>Номер АЗС</td>\r\n                                <td>{{azsDetails.data.GasStationNum}}</td>\r\n                                <td>Наименование</td>\r\n                                <td>{{azsDetails.data.Name}}</td>\r\n                            </tr>\r\n                            <tr>\r\n                                <td>Брэнд</td>\r\n                                <td>{{azsDetails.data.Brand}}</td>\r\n                            </tr>\r\n                            <tr>\r\n                                <td>Дата открытия</td>\r\n                                <td>{{azsDetails.data.OpenDate}}</td>\r\n                                <td>Дата закрытия</td>\r\n                                <td>{{azsDetails.data.CloseDate}}</td>\r\n                            </tr>\r\n                            <tr>\r\n                                <td>Статус</td>\r\n                                <td>{{azsDetails.data.Status}}</td>\r\n                                <td>Тип принадлежности</td>\r\n                                <td>{{azsDetails.data.OwnType}}</td>\r\n                            </tr>\r\n                            <tr>\r\n                                <td>Партнер</td>\r\n                                <td>{{azsDetails.data.Partner}}</td>\r\n                                <td>Тип местоположения</td>\r\n                                <td>{{azsDetails.data.LocationType}}</td>\r\n                            </tr>\r\n                            <tr>\r\n                                <td>Широта</td>\r\n                                <td>{{azsDetails.data.Latitude}}</td>\r\n                                <td>Долгота</td>\r\n                                <td>{{azsDetails.data.Longitude}}</td>\r\n                            </tr>\r\n                            <tr>\r\n                                <td>Адрес</td>\r\n                                <td>{{azsDetails.data.Country}}, {{azsDetails.data.Region}}, {{azsDetails.data.City}},\r\n                                    {{azsDetails.data.Street}}, {{azsDetails.data.House}}\r\n                                    , {{azsDetails.data.Building}}</td>\r\n                            </tr>\r\n                            <tr>\r\n                                <td>Телефон</td>\r\n                                <td>{{azsDetails.data.Phone}}</td>\r\n                                <td>Факс</td>\r\n                                <td>{{azsDetails.data.Fax}}</td>\r\n                            </tr>\r\n                            <tr>\r\n                                <td>Принадлежность АЗС</td>\r\n                                <td>{{azsDetails.data.ITSCGSBelongsTo}}</td>\r\n                                <td>Тип АЗС</td>\r\n                                <td>{{azsDetails.data.AddInfo03}}</td>\r\n                            </tr>\r\n                            <tr>\r\n                                <td>Отделение ГПН</td>\r\n                                <td>{{azsDetails.data.ITSCSecessionGPN}}</td>\r\n                            </tr>\r\n                            <tr>\r\n                                <td>\r\n                                    <p-checkbox\r\n                                            [(ngModel)]=\"fuelCardsAvailable\"\r\n                                            binary=\"true\"\r\n                                            disabled=\"true\"\r\n                                            label=\"Приём топливных карт\"></p-checkbox>\r\n                                </td>\r\n                                <td>\r\n                                    <p-checkbox\r\n                                            [(ngModel)]=\"loyalCardsAvailable\"\r\n                                            binary=\"true\"\r\n                                            disabled=\"true\"\r\n                                            label=\"Приём карт лояльности\"></p-checkbox>\r\n                                </td>\r\n                                <td>\r\n                                    <p-checkbox\r\n                                            [(ngModel)]=\"bankCardsAvailable\"\r\n                                            binary=\"true\"\r\n                                            disabled=\"true\"\r\n                                            label=\"Приём банковских карт\"></p-checkbox>\r\n                                </td>\r\n                            </tr>\r\n                            <tr>\r\n                                <td>\r\n                                    <label class=\"control-label\">Трассы: </label>\r\n                                </td>\r\n                                <td>\r\n                                    <label *ngFor=\"let track of azsDetails?.data?.ListOfTracks\"\r\n                                           class=\"control-label\">{{track?.id}}</label>\r\n                                </td>\r\n                            </tr>\r\n                            </tbody>\r\n                        </table>\r\n                    </p-accordionTab>\r\n                    <p-accordionTab header=\"Терминалы\">\r\n\r\n                        <p-table *ngIf=\"azsDetails?.data?.ListOfTerminals\"\r\n                                 [responsive]=\"true\"\r\n                                 [value]=\"azsDetails?.data?.ListOfTerminals[0].Terminal\"\r\n                        >\r\n                            <ng-template pTemplate=\"header\">\r\n                                <tr>\r\n                                    <th scope=\"col-sm\">Способ подключения</th>\r\n                                    <th scope=\"col-sm\">Статус</th>\r\n                                    <th scope=\"col-sm\">Идентификатор терминала</th>\r\n                                    <th scope=\"col-sm\">Наименование</th>\r\n                                    <th scope=\"col-sm\">Номер</th>\r\n                                    <th scope=\"col-sm\">Тип</th>\r\n                                </tr>\r\n                            </ng-template>\r\n                            <ng-template let-term pTemplate=\"body\">\r\n                                <tr>\r\n                                    <td>{{term.ConnectionType}}</td>\r\n                                    <td>{{term.Status}}</td>\r\n                                    <td>{{term.TerminalId}}</td>\r\n                                    <td>{{term.TerminalName}}</td>\r\n                                    <td>{{term.TerminalNumber}}</td>\r\n                                    <td>{{term.TerminalType}}</td>\r\n                                </tr>\r\n                            </ng-template>\r\n                        </p-table>\r\n                    </p-accordionTab>\r\n                    <p-accordionTab header=\"Сервисы\">\r\n                        <p-table *ngIf=\"azsDetails?.data?.ListOfServices\"\r\n                                 [responsive]=\"true\"\r\n                                 [value]=\"azsDetails?.data?.ListOfServices[0].Service\"\r\n                        >\r\n                            <ng-template pTemplate=\"header\">\r\n                                <tr>\r\n                                    <th scope=\"col-sm\">Id услуги</th>\r\n                                    <th scope=\"col-sm\">Наименование</th>\r\n                                </tr>\r\n                            </ng-template>\r\n                            <ng-template let-serv pTemplate=\"body\">\r\n                                <tr>\r\n                                    <td>{{serv.SiebelID}}</td>\r\n                                    <td>{{serv.Value}}</td>\r\n                                </tr>\r\n                            </ng-template>\r\n                        </p-table>\r\n                    </p-accordionTab>\r\n                </p-accordion>\r\n            </p-dialog>\r\n\r\n\r\n            <!--<p-dialog [(visible)]=\"gMapDialogVisible\"-->\r\n                      <!--header=\"АЗС на карте\"-->\r\n                      <!--positionTop=\"50\">-->\r\n                <!--<p-gmap [options]=\"options\"-->\r\n                        <!--[overlays]=\"overlays\"-->\r\n                        <!--[style]=\"{'width':'640px','height':'640px'}\"></p-gmap>-->\r\n            <!--</p-dialog>-->\r\n        </p-tabPanel>\r\n        <p-tabPanel header=\"Схема взаимодействия\">\r\n        <img class=\"img-fluid\" id=\"scheme-crm-azsdetails\" src=\"assets/images/scheme-crm.png\">\r\n    </p-tabPanel>\r\n        <p-tabPanel header=\"Инфраструктура\">\r\n            <img class=\"img-fluid\" id=\"scheme-infr\" src=\"assets/images/infr-crm.jpg\">\r\n        </p-tabPanel>\r\n        <p-tabPanel header=\"Swagger\">\r\n            <div class=\"embed-responsive embed-responsive-16by9\">\r\n                <iframe class=\"embed-responsive-item\"\r\n                        src=\"https://crmintegration-crmintegration.dmz-apps.demo.rht.ru/webjars/swagger-ui/index.html?url=/crm/api-doc\"></iframe>\r\n            </div>\r\n        </p-tabPanel>\r\n    </p-tabView>\r\n</p-panel>\r\n\r\n\r\n"

/***/ }),

/***/ "./src/app/pages/crm/crm.component.ts":
/*!********************************************!*\
  !*** ./src/app/pages/crm/crm.component.ts ***!
  \********************************************/
/*! exports provided: CrmComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CrmComponent", function() { return CrmComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_crm_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/crm.service */ "./src/app/services/crm.service.ts");
/* harmony import */ var _model_FltGasStationsListRequest_model__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../model/FltGasStationsListRequest.model */ "./src/app/model/FltGasStationsListRequest.model.ts");
/* harmony import */ var _model_azs_details_request_model__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../model/azs-details-request.model */ "./src/app/model/azs-details-request.model.ts");
/* harmony import */ var primeng_api__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! primeng/api */ "./node_modules/primeng/api.js");
/* harmony import */ var primeng_api__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(primeng_api__WEBPACK_IMPORTED_MODULE_5__);






var CrmComponent = /** @class */ (function () {
    function CrmComponent(crmService, messageService) {
        this.crmService = crmService;
        this.messageService = messageService;
        this.fuelCardsAvailable = false;
        this.loyalCardsAvailable = false;
        this.bankCardsAvailable = false;
    }
    CrmComponent.prototype.ngOnInit = function () {
    };
    CrmComponent.prototype.stationList = function () {
        var _this = this;
        var requestBody = new _model_FltGasStationsListRequest_model__WEBPACK_IMPORTED_MODULE_3__["FltGasStationsListRequest"]();
        // requestBody.AmndDate = this.dateLastChange.getMonth() + '/' + this.dateLastChange.getDate() + '/'
        //     + this.dateLastChange.getFullYear();
        requestBody.FLTCards = this.true2Y(this.fuelCardsFilterSelect);
        requestBody.LTYCards = this.true2Y(this.loyalCardsFilterSelect);
        requestBody.GPBCards = this.true2Y(this.bankCardsFilterSelect);
        //mocked fields
        requestBody.ClientIP = '127.0.0.1';
        requestBody.ClientSource = 'WebClientESB';
        requestBody.RequestTime = this.dateLastChange.getMonth() + '/' + this.dateLastChange.getDate() + '/'
            + this.dateLastChange.getFullYear();
        requestBody.languageID = 'RUS';
        requestBody.SessionID = 'N/A';
        requestBody.UserLogin = 'N/A';
        requestBody.ContractID = 'N/A';
        this.crmService.postfltGasStationsList(requestBody).subscribe(function (correlationIdObject) {
            _this.azsListcorrId = correlationIdObject.correlationId;
            console.log('Получение корИд по списку АЗС: ' + correlationIdObject.correlationId);
        }, function (error) {
            console.log('Ошибка обращения к сервису!');
            console.log(error);
        });
    };
    CrmComponent.prototype.stationListResult = function () {
        var _this = this;
        this.crmService.getResultGasStationsList(this.azsListcorrId).subscribe(function (azsListResponse) {
            _this.azsList = azsListResponse;
            if (azsListResponse.code != null) {
                if (azsListResponse.code == 204) {
                    _this.showWarn(azsListResponse.description);
                }
                if (azsListResponse.code == 500) {
                    _this.showError(azsListResponse.description);
                }
                if (azsListResponse.status.code == 0) {
                    _this.showSuccess();
                }
            }
        }, function (error) {
            console.log('Ошибка обращения к сервису!');
            console.log(error);
            _this.showError(error);
        });
    };
    CrmComponent.prototype.stationDetailsList = function () {
        var _this = this;
        this.crmService.postfltGasStationsDetailedList('empty string').subscribe(function (azsDetailsListResponse) {
            _this.azsDetailedList = azsDetailsListResponse;
        }, function (error) {
            console.log('Ошибка обращения к сервису!');
            console.log(error);
            _this.showError(error);
        });
    };
    CrmComponent.prototype.showWarn = function (serviceMessage) {
        this.messageService.add({ severity: 'warn', summary: 'Ошибка получения данных', detail: 'Данные ещё не обработаны: ' +
                serviceMessage });
    };
    CrmComponent.prototype.showSuccess = function () {
        this.messageService.add({ severity: 'success', summary: 'Получение', detail: 'Данные получены!' });
    };
    CrmComponent.prototype.showError = function (serviceMessage) {
        this.messageService.add({ severity: 'error', summary: 'Ошибка', detail: 'Ошибка обращения к сервису: ' +
                serviceMessage });
    };
    CrmComponent.prototype.details = function (inId) {
        var _this = this;
        var requestBody = new _model_azs_details_request_model__WEBPACK_IMPORTED_MODULE_4__["AzsDetailsRequest"]();
        requestBody.ID = inId;
        requestBody.ClientSource = 'WebClientESB';
        requestBody.LTYCards = this.true2Y(this.loyalCardsFilterSelect);
        ;
        requestBody.ClientIP = '127.0.0.1';
        requestBody.UserLogin = 'N/A';
        requestBody.RequestTime = this.dateLastChange.getMonth() + '/' + this.dateLastChange.getDate() + '/'
            + this.dateLastChange.getFullYear();
        requestBody.FLTCards = this.true2Y(this.fuelCardsFilterSelect);
        requestBody.SessionID = 'N/A';
        requestBody.GPBCards = this.true2Y(this.bankCardsFilterSelect);
        requestBody.languageID = 'RUS';
        requestBody.ContractID = 'N/A';
        // requestBody.AmndDate = this.dateLastChange.getMonth() + '/' + this.dateLastChange.getDay() + '/'
        //     + this.dateLastChange.getFullYear();
        this.crmService.azsDetails(requestBody).subscribe(function (azsDetailsResponse) {
            _this.getAszDetails(azsDetailsResponse.correlationId);
        }, function (error) {
            console.log('Ошибка обращения к сервису!');
            console.log(error);
        });
    };
    CrmComponent.prototype.getAszDetails = function (corId) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var i;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        i = 0;
                        _a.label = 1;
                    case 1:
                        if (!(i <= 25)) return [3 /*break*/, 4];
                        console.log('Попытка: ' + i);
                        return [4 /*yield*/, this.delay(1000).then(function (any) {
                                console.log('Делей на попытке: ' + i);
                                _this.crmService.getResultAzsDetails(corId).subscribe(function (azsDetailsResponse) {
                                    _this.azsDetails = azsDetailsResponse;
                                    _this.fuelCardsAvailable = _this.y2True(_this.azsDetails.data.FLTCards);
                                    _this.loyalCardsAvailable = _this.y2True(_this.azsDetails.data.LTYCards);
                                    _this.bankCardsAvailable = _this.y2True(_this.azsDetails.data.GPBCards);
                                    console.log('Данные получены!');
                                    return;
                                    console.log('Выход из цикла');
                                }, function (error) {
                                    console.log('Ошибка обращения к сервису на попытке: ' + i);
                                    console.log(error);
                                });
                            })];
                    case 2:
                        _a.sent();
                        _a.label = 3;
                    case 3:
                        i++;
                        return [3 /*break*/, 1];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    CrmComponent.prototype.delay = function (ms) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, new Promise(function (resolve) { return setTimeout(function () { return resolve(); }, ms); }).then(function () { return console.log("fired"); })];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    CrmComponent.prototype.onRowSelect = function (event) {
        console.log('ROW SELECTED! ' + event.data.ID);
    };
    CrmComponent.prototype.onRowButton = function (data) {
        if (data.ID != null) {
            console.log('Extracting data AZS: ' + data.ID);
            this.details(data.ID);
            this.azsDialogVisible = true;
        }
    };
    CrmComponent.prototype.onRowButtonMap = function (data) {
        // this.gMapDialogVisible = true;
        // console.log('AZS SELECTED! ' + data.ID);
        // this.details(data.ID);
        // let laty :number = Number(this.azsDetails.data.Latitude);
        // let longti :number = Number(this.azsDetails.data.Longitude);
        //
        // this.options = {
        //   center: {lat: laty, lng: longti},
        //   zoom: 20};
        //   this.overlays = [];
    };
    CrmComponent.prototype.y2True = function (value) {
        if (value === 'Y') {
            return true;
        }
        return false;
    };
    CrmComponent.prototype.true2Y = function (value) {
        if (value === true) {
            return 'Y';
        }
        return '';
    };
    CrmComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-crm',
            template: __webpack_require__(/*! ./crm.component.html */ "./src/app/pages/crm/crm.component.html"),
            providers: [primeng_api__WEBPACK_IMPORTED_MODULE_5__["MessageService"]],
            styles: [__webpack_require__(/*! ./crm.component.css */ "./src/app/pages/crm/crm.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_crm_service__WEBPACK_IMPORTED_MODULE_2__["CrmService"], primeng_api__WEBPACK_IMPORTED_MODULE_5__["MessageService"]])
    ], CrmComponent);
    return CrmComponent;
}());



/***/ }),

/***/ "./src/app/pages/etran/etran.component.css":
/*!*************************************************!*\
  !*** ./src/app/pages/etran/etran.component.css ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2V0cmFuL2V0cmFuLmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/pages/etran/etran.component.html":
/*!**************************************************!*\
  !*** ./src/app/pages/etran/etran.component.html ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p-toast [style]=\"{marginTop: '80px'}\"></p-toast>\r\n<p-panel header=\"Получение данных из ЭТРАН\">\r\n  <p-tabView>\r\n    <p-tabPanel header=\"Накладные\">\r\n      <div class=\"row\">\r\n        <div class=\"col-12 col-sm-10 col-md-6 col-lg-6 offset-0 offset-md-1 offset-sm-0\">\r\n\r\n          <form (ngSubmit)=\"onSubmit(f)\" #f=\"ngForm\">\r\n            <div class=\"form-group\">\r\n              <label for=\"invoiceId\">ID накладной</label>\r\n              <input type=\"text\" id=\"invoiceId\" name=\"invoiceId\" ngModel class=\"form-control\">\r\n            </div>\r\n            <div class=\"form-group\">\r\n              <label for=\"invoiceNumber\">Номер накладной</label>\r\n              <input type=\"text\" id=\"invoiceNumber\" name=\"invoiceNumber\" ngModel class=\"form-control\">\r\n            </div>\r\n            <button class=\"btn btn-primary\" type=\"submit\" [disabled]=\"!f.valid\">Получить накладную</button>\r\n          </form>\r\n\r\n          <p-accordion>\r\n            <p-accordionTab [selected]=\"true\" header=\"Накладная\">\r\n\r\n          <table class=\"table-responsive-lg table-bordered\" style=\"margin-top: 20px\">\r\n            <tr>\r\n              <td rowspan=\"11\"><img src=\"assets/images/etran/tank-gpn.svg\" style=\"height: 200px\"></td>\r\n            </tr>\r\n            <tr>\r\n              <td><h6>Id накладной</h6></td>\r\n              <td><h6>{{invoice?.invoiceID?.value}}</h6></td>\r\n            </tr>\r\n            <tr>\r\n              <td><h6>Номер накладной</h6></td>\r\n              <td><h6>{{invoice?.invUNP?.value}}</h6></td>\r\n            </tr>\r\n            <tr>\r\n              <td><h6>Дата создания</h6></td>\r\n              <td><h6>{{invoice?.invDateCreate?.value}}</h6></td>\r\n            </tr>\r\n            <tr>\r\n              <td><h6>Состояние накладной</h6></td>\r\n              <td><h6>{{invoice?.invoiceState?.value}}</h6></td>\r\n            </tr>\r\n            <tr>\r\n              <td><h6>Наименование станции отправителя</h6></td>\r\n              <td><h6>{{invoice?.invFromStationName?.value}}</h6></td>\r\n            </tr>\r\n            <tr>\r\n              <td><h6>Наименование станции назначения</h6></td>\r\n              <td><h6>{{invoice?.invToStationName?.value}}</h6></td>\r\n            </tr>\r\n            <tr>\r\n              <td><h6>Наименование отправителя</h6></td>\r\n              <td><h6>{{invoice?.invSenderName?.value}}</h6></td>\r\n            </tr>\r\n          </table>\r\n\r\n            </p-accordionTab>\r\n\r\n            <p-accordionTab header=\"Грузы\">\r\n                <p-table [responsive]=\"true\"\r\n                         [value]=\"invoice?.invFreight\"\r\n                         class=\"table table-striped table-dark\">\r\n                  <ng-template let-res pTemplate=\"body\">\r\n                    <tr>\r\n                      <td>Наименование груза</td>\r\n                      <td>{{res.freightAdditional?.value}}</td>\r\n                    </tr>\r\n                    <tr>\r\n                      <td>Точное наименованиегруза</td>\r\n                      <td>{{res.freightExactName?.value}}</td>\r\n                    </tr>\r\n                  </ng-template>\r\n                </p-table>\r\n            </p-accordionTab>\r\n\r\n            <p-accordionTab header=\"Вагоны\">\r\n              <p-table [responsive]=\"true\"\r\n                       [value]=\"invoice?.invCar\"\r\n                       class=\"table table-striped table-dark\">\r\n                <ng-template pTemplate=\"header\">\r\n                  <tr>\r\n                    <th scope=\"col-sm\">Тип вагона</th>\r\n                    <th scope=\"col-sm\">Номер вагона</th>\r\n                    <th scope=\"col-sm\">Владелец вагона</th>\r\n                  </tr>\r\n                </ng-template>\r\n                <ng-template let-car pTemplate=\"body\">\r\n                  <tr>\r\n                    <td>{{car?.carTypeName?.value}}</td>\r\n                    <td>{{car?.carNumber?.value}}</td>\r\n                    <td>{{car?.carOwnerName?.value}}</td>\r\n                  </tr>\r\n                </ng-template>\r\n              </p-table>\r\n            </p-accordionTab>\r\n\r\n          </p-accordion>\r\n        </div>\r\n      </div>\r\n\r\n              <!--<td><h6>{{invoice?.invSenderName?.value}}</h6></td>-->\r\n\r\n\r\n\r\n            <!--<tr>-->\r\n              <!--<td><h6>Наименование груза</h6></td>-->\r\n              <!--<td><h6>{{invoice?.invFreight?.freightAdditional?.value}}</h6></td>-->\r\n            <!--</tr>-->\r\n            <!--<tr>-->\r\n              <!--<td><h6>Точное наименования груза</h6></td>-->\r\n              <!--<td><h6>{{invoice?.invFreight?.freightExactName?.value}}</h6></td>-->\r\n            <!--</tr>-->\r\n            <!--<tr>-->\r\n              <!--<td><h6>Номер вагона</h6></td>-->\r\n              <!--<td><h6>{{invoice?.invCar?.carNumber?.value}}</h6></td>-->\r\n            <!--</tr>-->\r\n\r\n        <!--</div>-->\r\n\r\n      <!--</div>-->\r\n      <!--<h6 *ngIf=\"invoice\">{{invoice | json}}</h6>-->\r\n\r\n    </p-tabPanel>\r\n    <p-tabPanel header=\"Схема взаимодействия\">\r\n      <img class=\"img-fluid\" id=\"scheme-etran-azsReport\" src=\"assets/images/etran/etran-scheme.png\">\r\n    </p-tabPanel>\r\n    <p-tabPanel header=\"Инфраструктура\">\r\n      <img class=\"img-fluid\" id=\"scheme-infr\" src=\"assets/images/etran/etran-infr.png\">\r\n    </p-tabPanel>\r\n    <p-tabPanel header=\"Swagger\">\r\n      <div class=\"embed-responsive embed-responsive-16by9 e2e-trusted-url\">\r\n        <iframe class=\"embed-responsive-item\" alt=\"Бэк-энд этрана недоступен\"\r\n                [src]=\"trustedSwaggerUrl\"></iframe>\r\n      </div>\r\n    </p-tabPanel>\r\n  </p-tabView>\r\n\r\n</p-panel>\r\n"

/***/ }),

/***/ "./src/app/pages/etran/etran.component.ts":
/*!************************************************!*\
  !*** ./src/app/pages/etran/etran.component.ts ***!
  \************************************************/
/*! exports provided: EtranComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EtranComponent", function() { return EtranComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var primeng_api__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! primeng/api */ "./node_modules/primeng/api.js");
/* harmony import */ var primeng_api__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(primeng_api__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _services_etran_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/etran.service */ "./src/app/services/etran.service.ts");
/* harmony import */ var _model_etran_invoice_request_model__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../model/etran/invoice-request.model */ "./src/app/model/etran/invoice-request.model.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");







var EtranComponent = /** @class */ (function () {
    function EtranComponent(messageService, etranService, sanitizer) {
        this.messageService = messageService;
        this.etranService = etranService;
        this.sanitizer = sanitizer;
        this._swaggerUrl = _environments_environment__WEBPACK_IMPORTED_MODULE_5__["environment"].etranUrl + '/webjars/swagger-ui/index.html?url='
            + _environments_environment__WEBPACK_IMPORTED_MODULE_5__["environment"].etranUrl + '/etran/api-doc';
        this.trustedSwaggerUrl = sanitizer.bypassSecurityTrustResourceUrl(this._swaggerUrl);
    }
    EtranComponent.prototype.ngOnInit = function () {
    };
    EtranComponent.prototype.onSubmit = function (form) {
        console.log('Получаем данные из этрана!');
        var invoiceRequest = new _model_etran_invoice_request_model__WEBPACK_IMPORTED_MODULE_4__["InvoiceRequest"]();
        if (form.value && form.value.invoiceId !== undefined) {
            var property = { value: form.value.invoiceId, valueAttribute: "" };
            console.log(form.value.invoiceId);
            invoiceRequest.invoiceID = property;
            // invoiceRequest.invoiceID.value = form.value.invoiceId;
        }
        if (form.value.invoiceNumber !== undefined) {
            var property = { value: form.value.invoiceId, valueAttribute: "" };
            console.log(form.value.invoiceNumber);
            invoiceRequest.invNumber = property;
            invoiceRequest.invNumber.value = form.value.invoiceNumber;
        }
        this.getInvoice(invoiceRequest);
    };
    EtranComponent.prototype.getInvoice = function (invoiceRequest) {
        // this.invoice =
        //     {"invoiceID":{"value":"877523492"},"invUNP":{"value":"877523492"},"invDateCreate":{"value":"19.03.2019 06:46:14"},"invoiceStateID":{"value":"44"},"invoiceState":{"value":"Работа с документом окончена"},"invLastOper":{"value":"20.03.2019 19:52:54"},"invNeedForECP":{"value":"0"},"invECPSign":{"value":"1"},"invTypeID":{"value":"3"},"invTypeName":{"value":"Досылочная накладная"},"invBlankTypeID":{"value":"94"},"invBlankType":{"value":"94"},"invBlankTypeName":{"value":"Универсальный перевозочный документ на все виды отправок"},"invSenderID":{"value":"2518"},"invSenderName":{"value":"ЛОСИНООСТРОВСКАЯ"},"invSenderAddressID":{"value":"0"},"invSenderAddress":{"value":"ДС"},"invSenderTGNL":{"value":"6302"},"invFromCountryCode":{"value":"643"},"invFromCountryName":{"value":"РОССИЯ"},"invFromStationCode":{"value":"19540"},"invFromStationName":{"value":"ЛОСИНООСТРОВСКАЯ"},"invRecipID":{"value":"571174"},"invRecipOKPO":{"value":"47859907"},"invRecipName":{"value":"Акционерное общество \"Газпромнефть-Терминал\""},"invRecipAddressID":{"value":"0"},"invRecipAddress":{"value":"630099, НОВОСИБИРСКАЯ ОБЛАСТЬ, г.Новосибирск, ул.М.Горького - д.80, тел: (383) 218-94-85,(383) 218-96-18"},"invRecipTGNL":{"value":"6302"},"invToCountryCode":{"value":"643"},"invToCountryName":{"value":"РОССИЯ"},"invToStationCode":{"value":"23790"},"invToStationName":{"value":"ЛОБНЯ"},"invToLoadWay":{"value":"п/п Павельцевская нефтебаза  АО /Газпром"},"invSendSpeedID":{"value":"2"},"invSendSpeedName":{"value":"Грузовая"},"invSendKindID":{"value":"1"},"invSendKindName":{"value":"Повагонная"},"invPayPlaceID":{"value":"9"},"invPayPlaceName":{"value":"бесплатная перевозка"},"invPayFormID":{"value":"60"},"invPayFormName":{"value":"Досылка"},"invPlanCarTypeID":{"value":"70"},"invPlanCarTypeCode":{"value":"70"},"invPlanCarTypeName":{"value":"цистерны"},"invPlanCarCount":{"value":"1"},"invPlanCarOwnerTypeID":{"value":"1"},"invPlanCarOwnerTypeName":{"value":"Собственный"},"invLoadTypeID":{"value":"11"},"invLoadTypeName":{"value":"БЕЗ ЗАЧЕТА В ПОГРУЗКУ"},"invAnnounceValue":{"value":"0"},"invAVCurrencyID":{"value":"1"},"invDispKindID":{"value":"10"},"invRespPerson":{"value":"ЕГОРОВА"},"invDateExpire":{"value":"20.03.2019 00:00:00"},"invScaleTypeID":{"value":"14"},"invScaleTypeName":{"value":"Вагон. весы, в движении, Мах 150 т"},"invScalePersonID":{"value":"2"},"invScalePersonName":{"value":"Отправителем"},"invScalePrecision":{"value":".5"},"invCheckDepID":{"value":"0"},"invCheckDepName":{"value":"смешанная"},"invDeplPerson":{"value":"Оператор товарный Сидоров В.С."},"invDepNormDocID":{"value":"1"},"invDepNormDocName":{"value":"Правилам перевозок"},"invDateLoad":{"value":"14.03.2019 00:00:00"},"invFactDateToLoad":{"value":"19.03.2019 07:03:52"},"invFactDateToLoadLocal":{"value":"19.03.2019 07:03:52"},"invFactDateAccept":{"value":"19.03.2019 07:04:14"},"invFIOAccept":{"value":"ЕГОРОВА"},
        //  "invFreight":[{"freightCode":{"value":"21105"},"freightName":{"value":"БЕНЗИН МОТОРНЫЙ (АВТОМОБИЛЬНЫЙ) НЕЭТИЛИРОВАННЫЙ"},"freightExactName":{"value":"Бензин АИ-92-К5"},"freightAdditional":{"value":"Бензин моторный автомобильны й н/эт /Бензин неэтилированн ый марки АИ-92-К5 по ГОСТ325 13-2013/Автомобильный бензин экологического класса К5 ма рки АИ-92-К5/"},"freightPackTypeID":{"value":"24"},"freightPackTypeName":{"value":"Налив"},"freightWeight":{"value":"48700"},"freightRealWeight":{"value":"48700"},"freightDangerSignID":{"value":"2"},"freightDangerSignName":{"value":"Опасный груз"},"freightDangerID":{"value":"15077"},"freightDangerName":{"value":"БЕНЗИН МОТОРНЫЙ"},"freightAccidentCardID":{"value":"98"},"freightAccidentCard":{"value":"305"}}],"invDistance":[{"distCountryCode":{"value":"643"},"distCountryName":{"value":"РОССИЯ"},"distStationCountryId":{"value":"178"},"distStationCode":{"value":"19540"},"distStationName":{"value":"ЛОСИНООСТРОВСКАЯ"},"distTrackTypeID":{"value":"1"},"distTrackTypeName":{"value":"Широкая"},"distTranspTypeID":{"value":"1"},"distTranspTypeName":{"value":"Ж.Д."},"distMinWay":{"value":"167"},"distRecipID":{"value":"2518"},"distRecipName":{"value":"ЛОСИНООСТРОВСКАЯ"},"distRecipAddressID":{"value":"0"},"distRecipAddress":{"value":"ДС"},"distRecipTGNL":{"value":"6302"},"distSign":{"value":"10101"},"distCarrierID":{"value":"1"},"distPowerKind":{"value":"3"}},{"distCountryCode":{"value":"643"},"distCountryName":{"value":"РОССИЯ"},"distStationCountryId":{"value":"178"},"distStationCode":{"value":"23790"},"distStationName":{"value":"ЛОБНЯ"},"distLoadWay":{"value":"п/п Павельцевская нефтебаза  АО /Газпром"},"distTrackTypeID":{"value":"1"},"distTrackTypeName":{"value":"Широкая"},"distTranspTypeID":{"value":"1"},"distTranspTypeName":{"value":"Ж.Д."},"distMinWay":{"value":"0"},"distRecipID":{"value":"571174"},"distRecipOKPO":{"value":"47859907"},"distRecipName":{"value":"Акционерное общество \"Газпромнефть-Терминал\""},"distRecipAddressID":{"value":"0"},"distRecipAddress":{"value":"630099, НОВОСИБИРСКАЯ ОБЛАСТЬ, г.Новосибирск, ул.М.Горького - д.80, тел: (383) 218-94-85,(383) 218-96-18"},"distRecipTGNL":{"value":"6302"},"distSign":{"value":"101010"},"distCarrierID":{"value":"1"},"distPowerKind":{"value":"3"}}],"invSPC":[{"spcTranspClauseID":{"value":"1"},"spcTranspClauseDesc":{"value":"Другие отметки отправителя Росс., СМГС, Росс-Фин. г.4 и ЦИМ г. 7 отметка 16"},"spcCustomText":{"value":"Для АО /ГАЛА-ФОРМ/  /Ресурс ПА О /НК/Роснефть/. тел. /383/218 -94-84 03.04.2019"}},{"spcTranspClauseID":{"value":"9"},"spcTranspClauseDesc":{"value":"Легко воспламеняется"}},{"spcTranspClauseID":{"value":"1326"},"spcTranspClauseDesc":{"value":"Прикрытие 0-0-1"}},{"spcTranspClauseID":{"value":"1184"},"spcTranspClauseDesc":{"value":"Вагон (котел) и арматура исправны и соответствуют установленным требованиям"}},{"spcTranspClauseID":{"value":"882"},"spcTranspClauseDesc":{"value":"Другие в верхней части накладной"},"spcCustomText":{"value":"Возврат порожнего вагона по указанию собственника подвижного состава"}},{"spcTranspClauseID":{"value":"398"},"spcTranspClauseDesc":{"value":"Охрана"},"spcCustomText":{"value":"9"}}],"invDOC":[{"docTypeID":{"value":"853"},"docNumber":{"value":"№9/НОР-3/1107/ЮТС/819/2012.от.10,01,2012.г,"}},{"docTypeID":{"value":"794"},"docNumber":{"value":"07936 от 11.03.2019"}},{"docTypeID":{"value":"872"}}],"invCar":[{"carTypeID":{"value":"70"},"carTypeCode":{"value":"70"},"carTypeName":{"value":"цистерны"},"carNumber":{"value":"51734341"},"carOrder":{"value":"1"},"carOwnerCountryCode":{"value":"643"},"carOwnerCountryName":{"value":"РОССИЯ"},"carOwnerTypeID":{"value":"1"},"carOwnerTypeName":{"value":"Собственный"},"carOwnerID":{"value":"68140"},"carOwnerOKPO":{"value":"52142548"},"carOwnerName":{"value":"ООО \"БалтТрансСервис\""},"carTonnage":{"value":"66"},"carAxles":{"value":"4"},"carVolume":{"value":"74"},"carWeightDep":{"value":"270"},"carWeightDepReal":{"value":"266"},"carWeightGross":{"value":"75300"},"carWeightNet":{"value":"48700"},"carGuideCount":{"value":"0"},"carLiquidTemperature":{"value":"0"},"carLiquidHeight":{"value":"0"},"carLiquidDensity":{"value":"0"},"carLiquidVolume":{"value":"74"},"carTankType":{"value":"85"},"carRolls":{"value":"1"},"carConnectCode":{"value":"0"},"carIsCover":{"value":"0"},"carLength":{"value":"12.02"},"carCSL":{"sealTypeID":{"value":"109"},"sealTypeName":{"value":"ТП50"},"sealMarks":{"value":"РЖДА2487714"},"sealOwnerTypeID":{"value":"2"},"sealOwnerTypeName":{"value":"отправитель"},"sealRailwayID":{"value":"7"},"sealRailwayCode":{"value":"17"},"sealRailwayName":{"value":"МОСКОВСКАЯ"}}}],"wayCLS":[{"wayTranspClauseID":{"value":"1320"},"wayTranspClauseName":{"value":"Расстояние перевозки с учетом п. 20 приказа № 245"},"wayCustomText":{"value":"70"},"wayRwPanish":{"value":"0"}}],"invNumber":{"value":"ЭЖ683862"},"invUniqueNumber":{"value":"877523492"},"invSignRouteNumCirc":{"value":"0"},"invGoodsCashier":{"value":"Глубокова Олеся Николаевна"},"invGoodsCashierPost":{"value":"Агент сист фирм тр об (СФТО) I категории"},"invDateReady":{"value":"19.03.2019 07:04:14"},"invDateReadyLocal":{"value":"19.03.2019 07:04:14"},"invDateDeparture":{"value":"19.03.2019 07:05:09"},"invDateArrive":{"value":"20.03.2019 05:58:00"},"invDateArriveLocal":{"value":"20.03.2019 05:58:00"},"invDateDelivery":{"value":"20.03.2019 09:02:02"},"invDateDeliveryLocal":{"value":"20.03.2019 09:02:02"},"invDateRaskrEl":{"value":"20.03.2019 09:13:23"},"invDateNotification":{"value":"20.03.2019 06:39:43"},"invNotification":{"value":"Информирование по e-mail gpnl-dc@gazprom-neft.ru"},"invNum410":{"value":"82622"},"invKPZ":{"value":"195406"},"invParentID":{"value":"876185371"},"invParentNumber":{"value":"ЭЖ454359"}};
        var _this = this;
        this.etranService.getInvoice(invoiceRequest).subscribe(function (reportResponse) {
            _this.invoice = reportResponse;
        }, function (error) {
            _this.showError(error);
            console.log(error.toString());
        }, function () {
            console.log('Данные получены');
            _this.showSuccess('Данные из Этран получены');
        });
    };
    EtranComponent.prototype.showError = function (serviceMessage) {
        this.messageService.add({ severity: 'error', summary: 'Ошибка', detail: 'Ошибка обращения к сервису: ' +
                serviceMessage });
    };
    EtranComponent.prototype.showSuccess = function (serviceMessage) {
        this.messageService.add({ severity: 'success', summary: 'Получение', detail: serviceMessage });
    };
    EtranComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-etran',
            template: __webpack_require__(/*! ./etran.component.html */ "./src/app/pages/etran/etran.component.html"),
            providers: [primeng_api__WEBPACK_IMPORTED_MODULE_2__["MessageService"]],
            styles: [__webpack_require__(/*! ./etran.component.css */ "./src/app/pages/etran/etran.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [primeng_api__WEBPACK_IMPORTED_MODULE_2__["MessageService"], _services_etran_service__WEBPACK_IMPORTED_MODULE_3__["EtranService"],
            _angular_platform_browser__WEBPACK_IMPORTED_MODULE_6__["DomSanitizer"]])
    ], EtranComponent);
    return EtranComponent;
}());



/***/ }),

/***/ "./src/app/pages/mailer/mailer.component.css":
/*!***************************************************!*\
  !*** ./src/app/pages/mailer/mailer.component.css ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL21haWxlci9tYWlsZXIuY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/pages/mailer/mailer.component.html":
/*!****************************************************!*\
  !*** ./src/app/pages/mailer/mailer.component.html ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p>\n  mailer works!\n</p>\n"

/***/ }),

/***/ "./src/app/pages/mailer/mailer.component.ts":
/*!**************************************************!*\
  !*** ./src/app/pages/mailer/mailer.component.ts ***!
  \**************************************************/
/*! exports provided: MailerComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MailerComponent", function() { return MailerComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var MailerComponent = /** @class */ (function () {
    function MailerComponent() {
    }
    MailerComponent.prototype.ngOnInit = function () {
    };
    MailerComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-mailer',
            template: __webpack_require__(/*! ./mailer.component.html */ "./src/app/pages/mailer/mailer.component.html"),
            styles: [__webpack_require__(/*! ./mailer.component.css */ "./src/app/pages/mailer/mailer.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], MailerComponent);
    return MailerComponent;
}());



/***/ }),

/***/ "./src/app/pages/mc/mc.component.css":
/*!*******************************************!*\
  !*** ./src/app/pages/mc/mc.component.css ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".ui-table .ui-table-frozen-view .ui-table-tbody > tr > td,\r\n.ui-table .ui-table-unfrozen-view .ui-table-tbody > tr > td {\r\n    height: 160px !important;\r\n}\r\n\r\n.header-height {\r\n    height: 167px;\r\n}\r\n\r\n.mc-btn-padding {\r\n    padding-left: 20px;\r\n}\r\n\r\n.ng-invalid.ng-touched  {\r\n    border-left: 5px solid #a94442; /* red */\r\n}\r\n\r\n.red-mes {\r\n    color: #cc0000;\r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvbWMvbWMuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTs7SUFFSSx3QkFBd0I7QUFDNUI7O0FBRUE7SUFDSSxhQUFhO0FBQ2pCOztBQUVBO0lBQ0ksa0JBQWtCO0FBQ3RCOztBQUVBO0lBQ0ksOEJBQThCLEVBQUUsUUFBUTtBQUM1Qzs7QUFFQTtJQUNJLGNBQWM7QUFDbEIiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy9tYy9tYy5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLnVpLXRhYmxlIC51aS10YWJsZS1mcm96ZW4tdmlldyAudWktdGFibGUtdGJvZHkgPiB0ciA+IHRkLFxyXG4udWktdGFibGUgLnVpLXRhYmxlLXVuZnJvemVuLXZpZXcgLnVpLXRhYmxlLXRib2R5ID4gdHIgPiB0ZCB7XHJcbiAgICBoZWlnaHQ6IDE2MHB4ICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbi5oZWFkZXItaGVpZ2h0IHtcclxuICAgIGhlaWdodDogMTY3cHg7XHJcbn1cclxuXHJcbi5tYy1idG4tcGFkZGluZyB7XHJcbiAgICBwYWRkaW5nLWxlZnQ6IDIwcHg7XHJcbn1cclxuXHJcbi5uZy1pbnZhbGlkLm5nLXRvdWNoZWQgIHtcclxuICAgIGJvcmRlci1sZWZ0OiA1cHggc29saWQgI2E5NDQ0MjsgLyogcmVkICovXHJcbn1cclxuXHJcbi5yZWQtbWVzIHtcclxuICAgIGNvbG9yOiAjY2MwMDAwO1xyXG59Il19 */"

/***/ }),

/***/ "./src/app/pages/mc/mc.component.html":
/*!********************************************!*\
  !*** ./src/app/pages/mc/mc.component.html ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p-toast [style]=\"{marginTop: '80px'}\"></p-toast>\r\n<div class=\"container-fluid align-self-end\">\r\n    <p-panel header=\"Получение данных из Мобильной карты\">\r\n        <p-tabView>\r\n            <p-tabPanel header=\"Сменный отчет\">\r\n                <div class=\"row\">\r\n                    <div class=\"col-sm-2\"></div>\r\n\r\n                </div>\r\n                <form (ngSubmit)=\"onSubmit(f)\" #f=\"ngForm\">\r\n                    <p-table (onRowSelect)=\"validate(f)\"\r\n                             id=\"azs_table\"\r\n                             selectionMode=\"single\"\r\n                             [(selection)]=\"selectedAzs\"\r\n                             [paginator]=\"true\"\r\n                             [responsive]=\"true\"\r\n                             [rowsPerPageOptions]=\"[5,10,20,50]\"\r\n                             [rows]=\"5\"\r\n                             [value]=\"azsList\"\r\n                             *ngIf=\"azsList\"\r\n                             dataKey=\"number\"\r\n                    >\r\n                        <ng-template pTemplate=\"header\">\r\n                            <tr>\r\n                                <th scope=\"col-sm\">ID</th>\r\n                                <th scope=\"col-sm\">Номер АЗС</th>\r\n                            </tr>\r\n                        </ng-template>\r\n                        <ng-template let-azs let-rowIndex=\"ID\" pTemplate=\"body\">\r\n                            <tr [pSelectableRow]=\"azs\">\r\n                                <td>{{azs.id}}</td>\r\n                                <td>{{azs.number}}</td>\r\n                            </tr>\r\n                        </ng-template>\r\n                    </p-table>\r\n                    <span class=\"red-mes\" *ngIf=\"azsList && !selectedAzs\">Выберите АЗС!</span>\r\n\r\n                    <div class=\"row\">\r\n\r\n                        <label class=\"col-sm-12 col-md-6 text-center\">Начало периода: </label>\r\n                        <div class=\"form-group\">\r\n                            <p-calendar [(ngModel)]=\"startDate\"\r\n                                        name=\"startDate\"\r\n                                        class=\"col-sm-12 col-md-6 mc-elements\"\r\n                                        showIcon=\"true\"\r\n                                        required=\"true\"\r\n                                        dateFormat=\"dd.mm.yy\"></p-calendar>\r\n                        </div>\r\n                        <label class=\"col-sm-12 col-md-6 text-center\">Окончание периода: </label>\r\n                        <div class=\"form-group\">\r\n                            <p-calendar [(ngModel)]=\"endDate\"\r\n                                        name=\"endDate\"\r\n                                        class=\"col-sm-12 col-md-6 mc-elements\"\r\n                                        showIcon=\"true\"\r\n                                        required=\"true\"\r\n                                        dateFormat=\"dd.mm.yy\"></p-calendar>\r\n                        </div>\r\n                    </div>\r\n                    <div class=\"row\">\r\n                        <span   class=\"col-sm-6 col-md-6 col-lg-6\"></span>\r\n                        <span   class=\"red-mes\"\r\n                                *ngIf=\"!startDate && !endDate\">Выберите период</span>\r\n                    </div>\r\n\r\n                    <div class=\"row\">\r\n                        <span class=\"col-sm-6 col-md-6 col-lg-6\"></span>\r\n                        <button name=\"Submit\"\r\n                                type=\"submit\"\r\n                                [disabled]=\"!f.valid\"\r\n                                class=\"col-sm-12 col-md-6  col-lg-2 btn btn-outline-success\"> Получить смены\r\n                        </button>\r\n\r\n                        <!--<p-button (onClick)=\"getAzsList($event)\"-->\r\n                        <!--class=\"col-sm-12 col-md-6\"-->\r\n                        <!--label=\"Обновить список АЗС\"-->\r\n                        <!--styleClass=\"ui-button-info\"></p-button>-->\r\n                    </div>\r\n\r\n                </form>\r\n\r\n\r\n                <p-table (onRowSelect)=\"onShiftSelect($event)\"\r\n                         selectionMode=\"single\"\r\n                         [(selection)]=\"selectedShift\"\r\n                         [paginator]=\"true\"\r\n                         [responsive]=\"true\"\r\n                         [rowsPerPageOptions]=\"[5,10,20,50]\"\r\n                         [rows]=\"5\"\r\n                         [value]=\"shiftList\"\r\n                         *ngIf=\"shiftList\"\r\n                         dataKey=\"id\"\r\n                >\r\n                    <ng-template pTemplate=\"header\">\r\n                        <tr>\r\n                            <th scope=\"col-sm\">ID</th>\r\n                            <th scope=\"col-sm\">Пользователь</th>\r\n                            <th scope=\"col-sm\">Начало</th>\r\n                            <th scope=\"col-sm\">Окончание</th>\r\n                        </tr>\r\n                    </ng-template>\r\n                    <ng-template let-shift let-rowIndex=\"ID\" pTemplate=\"body\">\r\n                        <tr [pSelectableRow]=\"shift\">\r\n                            <td>{{shift.id}}</td>\r\n                            <td>{{shift.userName}}</td>\r\n                            <td>{{shift.dateBeg  | date}}</td>\r\n                            <td>{{shift.dateEnd  | date}}</td>\r\n                        </tr>\r\n                    </ng-template>\r\n                </p-table>\r\n                <p-dialog [(visible)]=\"reportDialogVisible\"\r\n                          [width]=\"1200\"\r\n                          header=\"Отчет по смене  {{selectedShift?.id}}\"\r\n                          positionLeft=\"100\"\r\n                          positionTop=\"50\"\r\n                >\r\n                    <div class=\"row\">\r\n                        <label class=\"control-label col-sm-2\">Операторы: {{report?.username}}</label>\r\n                    </div>\r\n\r\n                    <p-accordion>\r\n                        <p-accordionTab [selected]=\"true\" header=\"Отделы\">\r\n                            <p-table *ngIf=\"report?.dept\"\r\n                                     [responsive]=\"true\"\r\n                                     [value]=\"report?.dept\"\r\n                            >\r\n                                <ng-template pTemplate=\"header\">\r\n                                    <tr>\r\n                                        <th scope=\"col-sm\">ID</th>\r\n                                        <th scope=\"col-sm\">Сумма за наличный расчет</th>\r\n                                        <th scope=\"col-sm\">Сумма кредит</th>\r\n                                        <th scope=\"col-sm\">Наименование отдела</th>\r\n                                        <th scope=\"col-sm\">Сумма за безналичный расчет</th>\r\n                                        <th scope=\"col-sm\">Порядок в метагруппе отчета</th>\r\n                                        <th scope=\"col-sm\">Номер метагруппы показателей в отчете</th>\r\n                                    </tr>\r\n                                </ng-template>\r\n                                <ng-template let-dept pTemplate=\"body\">\r\n                                    <tr>\r\n                                        <td>{{dept.id}}</td>\r\n                                        <td>{{dept.cashPay}}</td>\r\n                                        <td>{{dept.creditPay}}</td>\r\n                                        <td>{{dept.deptName}}</td>\r\n                                        <td>{{dept.nonCashPay}}</td>\r\n                                        <td>{{dept.orderNo}}</td>\r\n                                        <td>{{dept.mGroupNo}}</td>\r\n                                    </tr>\r\n                                </ng-template>\r\n                            </p-table>\r\n                        </p-accordionTab>\r\n\r\n                        <p-accordionTab header=\"Z отчеты по ФР\">\r\n                            <p-table *ngIf=\"report?.ecrZnums\"\r\n                                     [responsive]=\"true\"\r\n                                     [value]=\"report?.ecrZnums\"\r\n                            >\r\n                                <ng-template pTemplate=\"header\">\r\n                                    <tr>\r\n                                        <th scope=\"col-sm\">ID</th>\r\n                                        <th scope=\"col-sm\">Сумма в кассе</th>\r\n                                        <th scope=\"col-sm\">Сумма возвратов денежных средств покупателям</th>\r\n                                        <th scope=\"col-sm\">Сумма инкассаций</th>\r\n                                        <th scope=\"col-sm\">Дата и время получения Z-отчета</th>\r\n                                        <th scope=\"col-sm\">Сумма внесений в кассу</th>\r\n                                        <th scope=\"col-sm\">Сумма продаж</th>\r\n                                        <th scope=\"col-sm\">Серийный номер ФР</th>\r\n                                        <th scope=\"col-sm\">Номер Z-отчета</th>\r\n                                    </tr>\r\n                                </ng-template>\r\n                                <ng-template let-znum pTemplate=\"body\">\r\n                                    <tr>\r\n                                        <td>{{znum.id}}</td>\r\n                                        <td>{{znum.amountinCash}}</td>\r\n                                        <td>{{znum.cashBackSum}}</td>\r\n                                        <td>{{znum.collectionSum}}</td>\r\n                                        <td>{{znum.dateTime | date}}</td>\r\n                                        <td>{{znum.deposition}}</td>\r\n                                        <td>{{znum.saleSum}}</td>\r\n                                        <td>{{znum.serialNumber}}</td>\r\n                                        <td>{{znum.nZrep}}</td>\r\n                                    </tr>\r\n                                </ng-template>\r\n                            </p-table>\r\n                        </p-accordionTab>\r\n\r\n                        <p-accordionTab header=\"Расход\">\r\n                            <p-table *ngIf=\"report?.expense\"\r\n                                     [responsive]=\"true\"\r\n                                     [value]=\"report?.expense\"\r\n                            >\r\n                                <ng-template pTemplate=\"header\">\r\n                                    <tr>\r\n                                        <th scope=\"col-sm\">ID</th>\r\n                                        <th scope=\"col-sm\">Наименование Товара</th>\r\n                                        <th scope=\"col-sm\">Остаток (на конец смены) в миллилитрах</th>\r\n                                        <th scope=\"col-sm\">Идентификатор товара</th>\r\n                                        <th scope=\"col-sm\">Порядок в отчете</th>\r\n                                        <th scope=\"col-sm\">Тип расчета</th>\r\n                                        <th scope=\"col-sm\">Наименование типа оплаты</th>\r\n                                        <th scope=\"col-sm\">Код типа оплаты</th>\r\n                                        <th scope=\"col-sm\">Реализация в разменной денежной единице</th>\r\n                                        <th scope=\"col-sm\">Реализация в миллилитрах</th>\r\n                                    </tr>\r\n                                </ng-template>\r\n                                <ng-template let-expense pTemplate=\"body\">\r\n                                    <tr>\r\n                                        <td>{{expense.id}}</td>\r\n                                        <td>{{expense.articleName}}</td>\r\n                                        <td>{{expense.endRestVol}}</td>\r\n                                        <td>{{expense.externalStrID}}</td>\r\n                                        <td>{{expense.orderNo}}</td>\r\n                                        <td>{{expense.payType}}</td>\r\n                                        <td>{{expense.payTypeName}}</td>\r\n                                        <td>{{expense.paytypeID}}</td>\r\n                                        <td>{{expense.saleMon}}</td>\r\n                                        <td>{{expense.saleVol}}</td>\r\n                                    </tr>\r\n                                </ng-template>\r\n                            </p-table>\r\n                        </p-accordionTab>\r\n\r\n                        <p-accordionTab header=\"Сальдо\">\r\n                            <p-table *ngIf=\"report?.saldo\"\r\n                                     [responsive]=\"true\"\r\n                                     [value]=\"report?.saldo\"\r\n                            >\r\n                                <ng-template pTemplate=\"header\">\r\n                                    <tr>\r\n                                        <th scope=\"col-sm\">ID</th>\r\n                                        <th scope=\"col-sm\">Наименование типа сальдо</th>\r\n                                        <th scope=\"col-sm\">Сумма в разменной денежной единице</th>\r\n                                        <th scope=\"col-sm\">Маркер назначения записи</th>\r\n                                    </tr>\r\n                                </ng-template>\r\n                                <ng-template let-saldo pTemplate=\"body\">\r\n                                    <tr>\r\n                                        <td>{{saldo.id}}</td>\r\n                                        <td>{{saldo.nameSaldo}}</td>\r\n                                        <td>{{saldo.saldoMon}}</td>\r\n                                        <td>{{saldo.st5Key}}</td>\r\n                                    </tr>\r\n                                </ng-template>\r\n                            </p-table>\r\n                        </p-accordionTab>\r\n\r\n                        <p-accordionTab header=\"Остатки\">\r\n                            <p-table *ngIf=\"report?.trk\"\r\n                                     [value]=\"report?.trk\"\r\n                                     [scrollable]=\"true\"\r\n                                     [style]=\"{width:'4200px'}\"\r\n                                     scrollHeight=\"200px\"\r\n\r\n                            >\r\n\r\n\r\n                                <ng-template pTemplate=\"header\">\r\n                                    <tr class=\"header-height\">\r\n                                        <th>ID</th>\r\n                                        <th>Наименование Товара</th>\r\n                                        <th>Книжный остаток на начало смены в литрах</th>\r\n                                        <th>Книжный остаток на начало смены в килограммах</th>\r\n                                        <th>Значение счетчика на начало смены</th>\r\n                                        <th>Фактический остаток на начало смены в литрах</th>\r\n                                        <th>Фактический остаток на начало смены в килограммах</th>\r\n                                        <th scope=\"col-sm\">Книжный расход в литрах</th>\r\n                                        <th scope=\"col-sm\">Общий уровень в миллиметрах</th>\r\n                                        <th scope=\"col-sm\">Недостача в килограммах</th>\r\n                                        <th scope=\"col-sm\">Недостача в литрах</th>\r\n                                        <th scope=\"col-sm\">Книжный остаток на конец смены в литрах</th>\r\n                                        <th scope=\"col-sm\">Книжный остаток на конец смены в килограммах</th>\r\n                                        <th scope=\"col-sm\">Значение счетчика на конец смены</th>\r\n                                        <th scope=\"col-sm\">Фактический остаток на конец смены в литрах</th>\r\n                                        <th scope=\"col-sm\">Фактический остаток на конец смены в килограммах</th>\r\n                                        <th scope=\"col-sm\">Погрешность носа в миллилитрах</th>\r\n                                        <th scope=\"col-sm\">Относительная погрешность носа в процентах</th>\r\n                                        <th scope=\"col-sm\">Расход в килограммах</th>\r\n                                        <th scope=\"col-sm\">Расход в литрах</th>\r\n                                        <th scope=\"col-sm\">Внешний код Товара</th>\r\n                                        <th scope=\"col-sm\">Комментарии к не валидным фактическим показателям</th>\r\n                                        <th scope=\"col-sm\">Объем мерника в литрах</th>\r\n                                        <th scope=\"col-sm\">Номер РК</th>\r\n                                        <th scope=\"col-sm\">Масса НП в трубопроводе в граммах</th>\r\n                                        <th scope=\"col-sm\">Объем НП в трубопроводе в литрах</th>\r\n                                        <th scope=\"col-sm\">Поступление НП за смену в граммах</th>\r\n                                        <th scope=\"col-sm\">Поступление НП за смену в миллилитрах</th>\r\n                                        <th scope=\"col-sm\">Перемещение в граммах</th>\r\n                                        <th scope=\"col-sm\">Перемещение в миллилитрах</th>\r\n                                        <th scope=\"col-sm\">Излишки в граммах</th>\r\n                                        <th scope=\"col-sm\">Излишки в миллилитрах</th>\r\n                                        <th scope=\"col-sm\">Плотность НП в резервуаре на конец периода в граммах на\r\n                                            сантиметр кубический\r\n                                        </th>\r\n                                        <th scope=\"col-sm\">Идентификатор резервуара, не является наименованием (номером)\r\n                                            резервуара на АЗК\r\n                                        </th>\r\n                                        <th scope=\"col-sm\">Наименование резервуара на АЗК</th>\r\n                                        <th scope=\"col-sm\">Номер ТРК</th>\r\n                                        <th scope=\"col-sm\">Уровень подтоварной воды в миллиметрах</th>\r\n                                    </tr>\r\n                                </ng-template>\r\n                                <ng-template let-trk pTemplate=\"body\">\r\n                                    <tr>\r\n                                        <td>{{trk.id}}</td>\r\n                                        <td>{{trk.articleName}}</td>\r\n                                        <td>{{trk.beginBookRest}}</td>\r\n                                        <td>{{trk.beginBookRestMass}}</td>\r\n                                        <td>{{trk.beginCounter}}</td>\r\n                                        <td>{{trk.beginFactRest}}</td>\r\n                                        <td>{{trk.beginFactRestMass}}</td>\r\n                                        <td>{{trk.bookExpenseVol}}</td>\r\n                                        <td>{{trk.commonLev}}</td>\r\n                                        <td>{{trk.deficitMass}}</td>\r\n                                        <td>{{trk.deficitVol}}</td>\r\n                                        <td>{{trk.endBookRest}}</td>\r\n                                        <td>{{trk.endBookRestMass}}</td>\r\n                                        <td>{{trk.endCounter}}</td>\r\n                                        <td>{{trk.endFactRest}}</td>\r\n                                        <td>{{trk.endFactRestMass}}</td>\r\n                                        <td>{{trk.errorNozzle}}</td>\r\n                                        <td>{{trk.errorNozzleRel}}</td>\r\n                                        <td>{{trk.expenseMass}}</td>\r\n                                        <td>{{trk.expenseVol}}</td>\r\n                                        <td>{{trk.externalStrID}}</td>\r\n                                        <td>{{trk.measurementMsg}}</td>\r\n                                        <td>{{trk.mernikVol}}</td>\r\n                                        <td>{{trk.nozzle}}</td>\r\n                                        <td>{{trk.pipeMass}}</td>\r\n                                        <td>{{trk.pipeVol}}</td>\r\n                                        <td>{{trk.receipMmass}}</td>\r\n                                        <td>{{trk.receipt}}</td>\r\n                                        <td>{{trk.relocMass}}</td>\r\n                                        <td>{{trk.relocVol}}</td>\r\n                                        <td>{{trk.surplusMass}}</td>\r\n                                        <td>{{trk.surplusVol}}</td>\r\n                                        <td>{{trk.tankDensity}}</td>\r\n                                        <td>{{trk.tankID}}</td>\r\n                                        <td>{{trk.tankName}}</td>\r\n                                        <td>{{trk.trkNo}}</td>\r\n                                        <td>{{trk.waterLev}}</td>\r\n                                    </tr>\r\n                                </ng-template>\r\n                            </p-table>\r\n                        </p-accordionTab>\r\n\r\n                    </p-accordion>\r\n                </p-dialog>\r\n            </p-tabPanel>\r\n            <p-tabPanel header=\"Схема взаимодействия\">\r\n                <img class=\"img-fluid\" id=\"scheme-mc-azsReport\" src=\"assets/images/scheme-mc.png\">\r\n            </p-tabPanel>\r\n            <p-tabPanel header=\"Инфраструктура\">\r\n                <img class=\"img-fluid\" id=\"scheme-infr\" src=\"assets/images/infr-mc.png\">\r\n            </p-tabPanel>\r\n            <p-tabPanel header=\"Swagger\">\r\n                <div class=\"embed-responsive embed-responsive-16by9 e2e-trusted-url\">\r\n                    <iframe class=\"embed-responsive-item\"\r\n                            [src]=\"trustedSwaggerUrl\"></iframe>\r\n                </div>\r\n            </p-tabPanel>\r\n        </p-tabView>\r\n    </p-panel>\r\n</div>"

/***/ }),

/***/ "./src/app/pages/mc/mc.component.ts":
/*!******************************************!*\
  !*** ./src/app/pages/mc/mc.component.ts ***!
  \******************************************/
/*! exports provided: McComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "McComponent", function() { return McComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_mc_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/mc.service */ "./src/app/services/mc.service.ts");
/* harmony import */ var primeng_api__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! primeng/api */ "./node_modules/primeng/api.js");
/* harmony import */ var primeng_api__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(primeng_api__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");






var McComponent = /** @class */ (function () {
    function McComponent(mcService, messageService, sanitizer) {
        this.mcService = mcService;
        this.messageService = messageService;
        this.sanitizer = sanitizer;
        this.reportDialogVisible = false;
        this.submitAvailable = false;
        this._swaggerUrl = _environments_environment__WEBPACK_IMPORTED_MODULE_4__["environment"].mcUrl + '/webjars/swagger-ui/index.html?url='
            + _environments_environment__WEBPACK_IMPORTED_MODULE_4__["environment"].mcUrl + '/mc/api-doc/swagger.json';
        this.trustedSwaggerUrl = sanitizer.bypassSecurityTrustResourceUrl(this._swaggerUrl);
    }
    McComponent.prototype.ngOnInit = function () {
        this.getAzsList();
    };
    McComponent.prototype.onSubmit = function () {
        if (this.selectedAzs === undefined) {
            console.log('Не выбрана АЗС!');
        }
        console.log('startDate: ' + this.startDate);
        console.log('endDate: ' + this.endDate);
        console.log('selectedAzsId: ' + this.selectedAzs.id);
        this.getShiftList(this.selectedAzs.id, this.startDate, this.endDate);
    };
    McComponent.prototype.validate = function (event) {
    };
    McComponent.prototype.onShiftSelect = function (event) {
        console.log('Получение отчета');
        this.getShiftReport(this.selectedAzs.id, this.selectedShift.id);
    };
    McComponent.prototype.getAzsList = function () {
        var _this = this;
        console.log('Получение списка АЗС из МК');
        this.mcService.getAzsList().subscribe(function (azsListResponse) {
            _this.azsList = azsListResponse;
            _this.showSuccess('Данные по АЗС получены.');
        }, function (error) {
            _this.showError(error);
            console.log(error);
        });
    };
    McComponent.prototype.getShiftList = function (azsId, startDate, endDate) {
        var _this = this;
        console.log('Получение списка смен по АЗС id= ' + azsId);
        this.mcService.getShiftsByDateRange(azsId, startDate.toISOString(), endDate.toISOString()).subscribe(function (azsListResponse) {
            _this.shiftList = azsListResponse;
            _this.showSuccess('Данные по сменам получены.');
        }, function (error) {
            _this.showError(error);
            console.log(error);
        });
    };
    McComponent.prototype.getShiftReport = function (azsId, shiftId) {
        var _this = this;
        console.log('Получение отчета по АЗС id= ' + azsId + ' и смене id= ' + shiftId);
        this.mcService.getShiftReport(azsId, shiftId, shiftId).subscribe(function (reportResponse) {
            _this.report = reportResponse;
            _this.showSuccess('Сменный отчет загружен');
            _this.reportDialogVisible = true;
        }, function (error) {
            _this.showError(error);
            console.log(error);
        });
    };
    McComponent.prototype.showError = function (serviceMessage) {
        this.messageService.add({ severity: 'error', summary: 'Ошибка', detail: 'Ошибка обращения к сервису: ' +
                serviceMessage });
    };
    McComponent.prototype.showSuccess = function (serviceMessage) {
        this.messageService.add({ severity: 'success', summary: 'Получение', detail: serviceMessage });
    };
    McComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-mc',
            template: __webpack_require__(/*! ./mc.component.html */ "./src/app/pages/mc/mc.component.html"),
            providers: [primeng_api__WEBPACK_IMPORTED_MODULE_3__["MessageService"]],
            styles: [__webpack_require__(/*! ./mc.component.css */ "./src/app/pages/mc/mc.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_mc_service__WEBPACK_IMPORTED_MODULE_2__["McService"], primeng_api__WEBPACK_IMPORTED_MODULE_3__["MessageService"],
            _angular_platform_browser__WEBPACK_IMPORTED_MODULE_5__["DomSanitizer"]])
    ], McComponent);
    return McComponent;
}());



/***/ }),

/***/ "./src/app/pages/monitoring/monitoring.component.css":
/*!***********************************************************!*\
  !*** ./src/app/pages/monitoring/monitoring.component.css ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL21vbml0b3JpbmcvbW9uaXRvcmluZy5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/pages/monitoring/monitoring.component.html":
/*!************************************************************!*\
  !*** ./src/app/pages/monitoring/monitoring.component.html ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container-fluid align-self-end\">\r\n  <p-panel header=\"Система мониторинга\">\r\n    <p-tabView>\r\n      <p-tabPanel header=\"Интеграционные тесты\">\r\n        <h6>Сдесь должны онлайн отображаться результаты выполнения интеграционных тестов и health запросов</h6>\r\n      </p-tabPanel>\r\n      <p-tabPanel header=\"Grafana DMZ\">\r\n        <iframe class=\"embed-responsive-item\"\r\n                src=\"https://grafana-openshift-grafana.dmz-apps.demo.rht.ru/?orgId=1\"></iframe>\r\n      </p-tabPanel>\r\n      <p-tabPanel header=\"Grafana KSPD\">\r\n        <iframe class=\"embed-responsive-item\"\r\n                src=\"https://grafana-openshift-grafana.kspd-apps.demo.rht.ru/?orgId=1\"></iframe>\r\n      </p-tabPanel>\r\n    </p-tabView>\r\n  </p-panel>\r\n</div>"

/***/ }),

/***/ "./src/app/pages/monitoring/monitoring.component.ts":
/*!**********************************************************!*\
  !*** ./src/app/pages/monitoring/monitoring.component.ts ***!
  \**********************************************************/
/*! exports provided: MonitoringComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MonitoringComponent", function() { return MonitoringComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var MonitoringComponent = /** @class */ (function () {
    function MonitoringComponent() {
    }
    MonitoringComponent.prototype.ngOnInit = function () {
    };
    MonitoringComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-monitoring',
            template: __webpack_require__(/*! ./monitoring.component.html */ "./src/app/pages/monitoring/monitoring.component.html"),
            styles: [__webpack_require__(/*! ./monitoring.component.css */ "./src/app/pages/monitoring/monitoring.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], MonitoringComponent);
    return MonitoringComponent;
}());



/***/ }),

/***/ "./src/app/pages/ofd/ofd.component.css":
/*!*********************************************!*\
  !*** ./src/app/pages/ofd/ofd.component.css ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "#ofd-widget-val {\r\n    text-align: right;\r\n    \r\n    color: #0079c1;\r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvb2ZkL29mZC5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0lBQ0ksaUJBQWlCOztJQUVqQixjQUFjO0FBQ2xCIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvb2ZkL29mZC5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiI29mZC13aWRnZXQtdmFsIHtcclxuICAgIHRleHQtYWxpZ246IHJpZ2h0O1xyXG4gICAgXHJcbiAgICBjb2xvcjogIzAwNzljMTtcclxufSJdfQ== */"

/***/ }),

/***/ "./src/app/pages/ofd/ofd.component.html":
/*!**********************************************!*\
  !*** ./src/app/pages/ofd/ofd.component.html ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p-toast [style]=\"{marginTop: '80px'}\"></p-toast>\n<p-panel header=\"Получение данных из ОФД\">\n    <p-tabView>\n        <p-tabPanel header=\"Статистика ККТС\">\n            <div class=\"row container-fluid\">\n                <div class=\"col-12 col-sm-10 col-md-6 col-lg-6 offset-0 offset-md-1 offset-sm-0\">\n                    <form (ngSubmit)=\"onSubmit(f)\" #f=\"ngForm\">\n                        <div class=\"form-group\">\n                            <label class=\"offset-sm-0\" for=\"inn\">ИНН:</label>\n                            <input type=\"text\" id=\"inn\" name=\"inn\" [(ngModel)]=\"inn\" class=\"form-control\">\n                        </div>\n\n                        <div class=\"form-group\">\n                            <label class=\"offset-sm-0\" for=\"kkt\">ККТ:</label>\n                            <input type=\"text\" id=\"kkt\" name=\"kkt\" [(ngModel)]=\"kkt\" class=\"form-control\">\n                        </div>\n\n                        <div class=\"form-group\">\n                            <label for=\"dateFrom\" class=\"col-12 col-sm-12 col-md-6\">Дата начала</label>\n                            <p-calendar id=\"dateFrom\"\n                                        ngModel\n                                        class=\"col-12 col-sm-12 col-md-6\"\n                                        name=\"dateFrom\"\n                                        showIcon=\"true\"\n                                        showTime=\"true\"\n                                        required=\"true\"\n                                        dateFormat=\"yy-mm-dd\"></p-calendar>\n                        </div>\n\n                        <div class=\"form-group\">\n                            <label for=\"dateFrom\" class=\"col-12 col-sm-12 col-md-6\">Дата окончания</label>\n                            <p-calendar id=\"dateTo\"\n                                        ngModel\n                                        name=\"dateTo\"\n                                        class=\"col-12 col-sm-12 col-md-6\"\n                                        showIcon=\"true\"\n                                        showTime=\"true\"\n                                        required=\"true\"\n                                        dateFormat=\"yy-mm-dd\"></p-calendar>\n                        </div>\n                        <button class=\"btn btn-outline-secondary ml-auto\" type=\"button\" (click)=\"onAuth()\"\n                                *ngIf=\"!ofdService.isAuthenticated()\">\n                            Авторизоваться\n                        </button>\n\n                        <button class=\"btn btn-outline-warning ml-auto\"\n                                type=\"button\"\n                                (click)=\"onLogout()\"\n                                *ngIf=\"ofdService.isAuthenticated()\">\n                            Разлогиниться\n                        </button>\n\n                        <button class=\"btn btn-primary\" type=\"submit\" [disabled]=\"!f.valid\">\n                            Получить статистику\n                        </button>\n                        <!--<button class=\"btn btn-outline-secondary ml-auto\" type=\"button\" (click)=\"test()\">-->\n                        <!--тест-->\n                        <!--</button>-->\n                    </form>\n\n                    <p-panel *ngIf=\"statistics && cashStat\" header=\"ОФД виджет\">\n\n                        <table class=\"table-responsive-lg table-bordered\">\n                            <tr pTooltip=\"Лучшая АЗС\" tooltipPosition=\"right\">\n                                <td><img src=\"assets/images/ofd/azs-50.svg\" style=\"height: 50px\"></td>\n                                <td><h6>{{azsStat?.name}}</h6></td>\n                                <td><h6>{{azsStat?.value}}</h6></td>\n                            </tr>\n                            <tr pTooltip=\"Лучшый оператор\" tooltipPosition=\"right\">\n                                <td><img src=\"assets/images/ofd/operator-50.svg\" style=\"height: 50px\"></td>\n                                <td><h6>{{operatorStat?.name}}</h6></td>\n                                <td><h6>{{operatorStat?.value}}</h6></td>\n                            </tr>\n                            <tr pTooltip=\"Самый продаваемый товар\" tooltipPosition=\"right\">\n                                <td><img src=\"assets/images/ofd/barrel-50.svg\" style=\"height: 50px\"></td>\n                                <td><h6>{{itemStat?.name}}</h6></td>\n                                <td><h6>{{itemStat?.value}}</h6></td>\n                            </tr>\n                            <tr pTooltip=\"Наибольшая выручка\" tooltipPosition=\"right\">\n                                <td rowspan=\"2\"><img src=\"assets/images/ofd/cash-50.svg\" style=\"height: 50px\"></td>\n                                <td><h6>{{cashStat?.name}}</h6></td>\n                                <td><h6>{{cashStat?.value}}</h6></td>\n                            </tr>\n                            <tr>\n                                <td><h6>{{ecashStat?.name}}</h6></td>\n                                <td><h6>{{ecashStat?.value}}</h6></td>\n                            </tr>\n                        </table>\n                    </p-panel>\n\n\n                </div>\n                <p-accordion *ngIf=\"statistics\">\n                    <p-accordionTab [selected]=\"true\" header=\"Детально\">\n\n\n                        <p-table class=\"table-striped\"\n                                [(selection)]=\"selectedData\" [paginator]=\"true\"\n                                [responsive]=\"true\"\n                                [rowsPerPageOptions]=\"[5,10,20,50]\"\n                                [rows]=\"10\"\n                                [value]=\"statistics?.Data\"\n\n                                selectionMode=\"single\"\n                        >\n                            <ng-template pTemplate=\"header\">\n                                <tr>\n                                    <th>Тэг</th>\n                                    <th>Пользователь</th>\n                                    <th>ИНН</th>\n                                    <th>Номер</th>\n                                    <th>Дата</th>\n                                    <th>Смена</th>\n                                    <th>Операция</th>\n                                    <th>Тариф</th>\n                                    <th>Оператор</th>\n                                    <th>Номер ККТ</th>\n                                    <th>Зав. №</th>\n                                </tr>\n                            </ng-template>\n                            <ng-template let-dt let-rowIndex=\"Tag\" pTemplate=\"body\">\n                                <tr [pSelectableRow]=\"dt\">\n                                    <td>{{dt.Tag}} </td>\n                                    <td>{{dt.User}}</td>\n                                    <td>{{dt.UserInn}}</td>\n                                    <td>{{dt.Number}}</td>\n                                    <td>{{dt.DateTime}}</td>\n                                    <td>{{dt.ShiftNumber}}</td>\n                                    <td>{{dt.OperationType}}</td>\n                                    <td>{{dt.TaxationType}}</td>\n                                    <td>{{dt.Operator}}</td>\n                                    <td>{{dt.KktRegNumber}}</td>\n                                    <td>{{dt.FnFactoryNumber}}</td>\n                                    <td>\n                                        <div class=\"row\">\n                                            <p-button (onClick)=\"onDataItems(dt.Items)\" icon=\"pi pi-info-circle\"\n                                                      style=\"padding-left: 10px\" pTooltip=\"Детально по товарам\"\n                                                      tooltipPosition=\"left\"></p-button>\n\n                                            <p-button (onClick)=\"onData(dt)\" icon=\"pi pi-info\"\n                                                      style=\"padding-left: 10px\" pTooltip=\"Детально по чеку\"\n                                                      tooltipPosition=\"left\"></p-button>\n                                        </div>\n                                    </td>\n                                </tr>\n                            </ng-template>\n                        </p-table>\n\n                    </p-accordionTab>\n                </p-accordion>\n\n\n                <!--<h6 *ngIf=\"statistics\">{{statistics?.Data | json}}</h6>-->\n            </div>\n\n            <p-dialog [(visible)]=\"itemViewEnabled\"\n                      class=\"container-fluid\"\n                      header=\"Список товаров\"\n                      positionLeft=\"100\"\n                      positionTop=\"50\"\n            >\n                <table *ngIf=\"itemsList\" class=\"table table-bordered table-responsive\">\n                    <thead>\n                    <tr>\n                        <th>Товар</th>\n                        <th>Цена</th>\n                        <th>Количество</th>\n                        <th>Сумма без НДС</th>\n                        <th>Сумма</th>\n                        <th>Метод расчета</th>\n                        <th>Ставка НДС</th>\n                        <th>Сумма НДС</th>\n                    </tr>\n                    </thead>\n                    <tbody *ngFor=\"let item of itemsList\">\n                    <tr>\n                        <td>{{item.Name}}</td>\n                        <td>{{item.Price}}</td>\n                        <td>{{item.Quantity}}</td>\n                        <td>{{item.Nds00_TotalSumm}}</td>\n                        <td>{{item.Total}}</td>\n                        <td>{{item.CalculationMethod}}</td>\n                        <td>{{item.NdsRate}}</td>\n                        <td>{{item.NdsSumm}}</td>\n                    </tr>\n                    </tbody>\n                </table>\n            </p-dialog>\n\n\n            <p-dialog [(visible)]=\"detailedDataViewEnabled\"\n                      class=\"container-fluid\"\n                      header=\"Детально по чеку\"\n                      positionLeft=\"200\"\n                      positionTop=\"100\"\n            >\n                <table *ngIf=\"dataDetails\" class=\"table table-bordered table-responsive\">\n                    <tbody>\n                    <tr>\n                        <td>Тэг</td>\n                        <td>{{dataDetails.Tag}}</td>\n                    </tr>\n                    <tr>\n                        <td>Пользователь</td>\n                        <td>{{dataDetails.User}}</td>\n                    </tr>\n                    <tr>\n                        <td>ИНН</td>\n                        <td>{{dataDetails.UserInn}}</td>\n                    </tr>\n                    <tr>\n                        <td>Номер</td>\n                        <td>{{dataDetails.Number}}</td>\n                    </tr>\n                    <tr>\n                        <td>Дата</td>\n                        <td>{{dataDetails.DateTime}}</td>\n                    </tr>\n                    <tr>\n                        <td>Номер смены</td>\n                        <td>{{dataDetails.ShiftNumber}}</td>\n                    </tr>\n                    <tr>\n                        <td>Тип операции</td>\n                        <td>{{dataDetails.OperationType}}</td>\n                    </tr>\n                    <tr>\n                        <td>Тип тарифа</td>\n                        <td>{{dataDetails.TaxationType}}</td>\n                    </tr>\n                    <tr>\n                        <td>Оператор</td>\n                        <td>{{dataDetails.Operator}}</td>\n                    </tr>\n                    <tr>\n                        <td>Регистрационный номер ККТ</td>\n                        <td>{{dataDetails.KktRegNumber}}</td>\n                    </tr>\n                    <tr>\n                        <td>Номер фискального номер</td>\n                        <td>{{dataDetails.FnFactoryNumber}}</td>\n                    </tr>\n                    <tr>\n                        <td>Розничный адрес</td>\n                        <td>{{dataDetails.RetailPlaceAddress}}</td>\n                    </tr>\n                    <tr>\n                        <td>Адрес отправителя</td>\n                        <td>{{dataDetails.SenderAddress}}</td>\n                    </tr>\n                    <tr>\n                        <td>Адрес покупателя</td>\n                        <td>{{dataDetails.BuyerAddress}}</td>\n                    </tr>\n                    <tr>\n                        <td>Сумма НДС 18%</td>\n                        <td>{{dataDetails.Nds18TotalSumm}}</td>\n                    </tr>\n                    <tr>\n                        <td>Сумма НДС 10%</td>\n                        <td>{{dataDetails.Nds10TotalSumm}}</td>\n                    </tr>\n                    <tr>\n                        <td>Сумма НДС</td>\n                        <td>{{dataDetails.NdsNATotalSumm}}</td>\n                    </tr>\n                    <tr>\n                        <td>Расчитанная сумма НДС 18%</td>\n                        <td>{{dataDetails.Nds18CalculatedTotalSumm}}</td>\n                    </tr>\n                    <tr>\n                        <td>Расчитанная сумма НДС 10%</td>\n                        <td>{{dataDetails.Nds10CalculatedTotalSumm}}</td>\n                    </tr>\n                    <tr>\n                        <td>Сумма без НДС</td>\n                        <td>{{dataDetails.Nds00TotalSumm}}</td>\n                    </tr>\n                    <tr>\n                        <td>Сумма всего</td>\n                        <td>{{dataDetails.AmountTotal}}</td>\n                    </tr>\n                    <tr>\n                        <td>Сумма наличными</td>\n                        <td>{{dataDetails.AmountCash}}</td>\n                    </tr>\n                    <tr>\n                        <td>Сумма электронными</td>\n                        <td>{{dataDetails.AmountECash}}</td>\n                    </tr>\n                    <tr>\n                        <td>Номер документа</td>\n                        <td>{{dataDetails.DocumentNumber}}</td>\n                    </tr>\n                    <tr>\n                        <td>Фискальный признак документа</td>\n                        <td>{{dataDetails.FiscalSign}}</td>\n                    </tr>\n                    <tr>\n                        <td>Десятичный фискальный признак документа</td>\n                        <td>{{dataDetails.DecimalFiscalSign}}</td>\n                    </tr>\n                    <tr>\n                        <td>Вервия форматирования</td>\n                        <td>{{dataDetails.FormatVersion}}</td>\n                    </tr>\n                    <tr>\n                        <td>Общее количество</td>\n                        <td>{{dataDetails.AmountAdvance}}</td>\n                    </tr>\n                    <tr>\n                        <td>Сумма кредитная</td>\n                        <td>{{dataDetails.AmountLoan}}</td>\n                    </tr>\n                    <tr>\n                        <td>Предоставление суммы</td>\n                        <td>{{dataDetails.AmountGranting}}</td>\n                    </tr>\n                    <tr>\n                        <td>Сайт налоговой</td>\n                        <td>{{dataDetails.TaxAuthoritySite}}</td>\n                    </tr>\n                    <tr>\n                        <td>Место расчета</td>\n                        <td>{{dataDetails.CalculationPlace}}</td>\n                    </tr>\n                    <tr>\n                        <td>Инн оператора</td>\n                        <td>{{dataDetails.OperatorINN}}</td>\n                    </tr>\n                    </tbody>\n                    <tfoot *ngFor=\"let prop of dataDetails.ExtraProperty\">\n                    <tr>\n                        <td>prop.ExtraPropertyName</td>\n                        <td>prop.ExtraPropertyValue</td>\n                    </tr>\n                    </tfoot>\n                </table>\n            </p-dialog>\n\n        </p-tabPanel>\n        <p-tabPanel header=\"Схема взаимодействия\">\n            <img class=\"img-fluid\" id=\"scheme-mc-azsReport\" src=\"assets/images/ofd/ofd-scheme.png\">\n        </p-tabPanel>\n        <p-tabPanel header=\"Инфраструктура\">\n            <img class=\"img-fluid\" id=\"scheme-infr\" src=\"assets/images/ofd/ofd-infr.png\">\n        </p-tabPanel>\n        <p-tabPanel header=\"Swagger\">\n            <div class=\"embed-responsive embed-responsive-16by9 e2e-trusted-url\">\n                <iframe class=\"embed-responsive-item\"\n                        [src]=\"trustedSwaggerUrl\"></iframe>\n            </div>\n        </p-tabPanel>\n\n\n    </p-tabView>\n</p-panel>"

/***/ }),

/***/ "./src/app/pages/ofd/ofd.component.ts":
/*!********************************************!*\
  !*** ./src/app/pages/ofd/ofd.component.ts ***!
  \********************************************/
/*! exports provided: OfdComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OfdComponent", function() { return OfdComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var primeng_api__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! primeng/api */ "./node_modules/primeng/api.js");
/* harmony import */ var primeng_api__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(primeng_api__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _services_ofd_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/ofd.service */ "./src/app/services/ofd.service.ts");
/* harmony import */ var _model_ofd_WidgetStatInfo__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../model/ofd/WidgetStatInfo */ "./src/app/model/ofd/WidgetStatInfo.ts");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../environments/environment */ "./src/environments/environment.ts");







var OfdComponent = /** @class */ (function () {
    function OfdComponent(messageService, ofdService, sanitizer) {
        this.messageService = messageService;
        this.ofdService = ofdService;
        this.sanitizer = sanitizer;
        this.itemViewEnabled = false;
        this.detailedDataViewEnabled = false;
        var swaggerUrl = _environments_environment__WEBPACK_IMPORTED_MODULE_6__["environment"].ofdUrl + '/webjars/swagger-ui/index.html?url='
            + _environments_environment__WEBPACK_IMPORTED_MODULE_6__["environment"].ofdUrl + '/ofd/api-doc';
        this.trustedSwaggerUrl = sanitizer.bypassSecurityTrustResourceUrl(swaggerUrl);
    }
    OfdComponent.prototype.ngOnInit = function () {
        // this.inn = 1646014874;
        this.inn = 7709359770;
        this.kkt = '0000498268002903';
        // this.inn = 7709359770;
        this.example = { "Status": "Success", "Elapsed": "00:00:06.644", "Statistics": [{ "StatisticsMarker": "CASH", "Name": "Total cash", "Value": "278900" }, { "StatisticsMarker": "ECASH", "Name": "Total eCash", "Value": "0" }, { "StatisticsMarker": "AZS", "Name": "г. Елабуга, ул. Окружное Шоссе, д. 37", "Value": "278900" }, { "StatisticsMarker": "ITEM", "Name": "Молоко \"Домик в деревне\" ультрапастеризо", "Value": "259150" }, { "StatisticsMarker": "OPERATOR", "Name": "Михайлов Александр", "Value": "278900" }], "Data": [{ "Tag": 3, "User": "ООО \"Оптовик\"", "UserInn": "1646014874", "Number": 1, "DateTime": "2019-02-05T08:56:00", "ShiftNumber": 35, "OperationType": "1", "TaxationType": 1, "Operator": "Михайлов Александр", "KKT_RegNumber": "0000000002008480    ", "FN_FactoryNumber": "9999078900011482", "Items": [{ "Name": "Молоко \"Домик в деревне\" ультрапастеризо", "Price": 4300, "Quantity": "1.000", "Total": 4300, "CalculationMethod": 4, "NDS_Rate": 2, "NDS_Summ": 391 }], "RetailPlaceAddress": "г. Елабуга, ул. Окружное Шоссе, д. 37", "Sender_Address": "sender@mail.ru", "Nds18_TotalSumm": 0, "Nds10_TotalSumm": 391, "NdsNA_TotalSumm": 0, "Nds18_CalculatedTotalSumm": 0, "Nds10_CalculatedTotalSumm": 0, "Nds00_TotalSumm": 0, "Amount_Total": 4300, "Amount_Cash": 4300, "Amount_ECash": 0, "Document_Number": 247, "FiscalSign": "MQR1/KG+", "DecimalFiscalSign": "1979490750", "Format_Version": 2, "Amount_Advance": 0, "Amount_Loan": 0, "Amount_Granting": 0, "TaxAuthority_Site": "nalog.ru", "Calculation_Place": "Магазин \"ЭССЕН\"", "Operator_INN": "" }, { "Tag": 3, "User": "ООО \"Оптовик\"", "UserInn": "1646014874", "Number": 2, "DateTime": "2019-02-05T09:01:00", "ShiftNumber": 35, "OperationType": "1", "TaxationType": 1, "Operator": "Михайлов Александр", "KKT_RegNumber": "0000000002008480    ", "FN_FactoryNumber": "9999078900011482", "Items": [{ "Name": "Молоко \"Домик в деревне\" ультрапастеризо", "Price": 4300, "Quantity": "1.000", "Total": 4300, "CalculationMethod": 4, "NDS_Rate": 2, "NDS_Summ": 391 }], "RetailPlaceAddress": "г. Елабуга, ул. Окружное Шоссе, д. 37", "Sender_Address": "sender@mail.ru", "Nds18_TotalSumm": 0, "Nds10_TotalSumm": 391, "NdsNA_TotalSumm": 0, "Nds18_CalculatedTotalSumm": 0, "Nds10_CalculatedTotalSumm": 0, "Nds00_TotalSumm": 0, "Amount_Total": 4300, "Amount_Cash": 4300, "Amount_ECash": 0, "Document_Number": 248, "FiscalSign": "MQRiavrl", "DecimalFiscalSign": "1651178213", "Format_Version": 2, "Amount_Advance": 0, "Amount_Loan": 0, "Amount_Granting": 0, "TaxAuthority_Site": "nalog.ru", "Calculation_Place": "Магазин \"ЭССЕН\"", "Operator_INN": "" }, { "Tag": 3, "User": "ООО \"Оптовик\"", "UserInn": "1646014874", "Number": 3, "DateTime": "2019-02-05T09:36:00", "ShiftNumber": 35, "OperationType": "1", "TaxationType": 1, "Operator": "Михайлов Александр", "KKT_RegNumber": "0000000002008480    ", "FN_FactoryNumber": "9999078900011482", "Items": [{ "Name": "Молоко \"Домик в деревне\" ультрапастеризо", "Price": 2550, "Quantity": "1.000", "Total": 2550, "CalculationMethod": 4, "NDS_Rate": 2, "NDS_Summ": 232 }], "RetailPlaceAddress": "г. Елабуга, ул. Окружное Шоссе, д. 37", "Sender_Address": "sender@mail.ru", "Nds18_TotalSumm": 0, "Nds10_TotalSumm": 232, "NdsNA_TotalSumm": 0, "Nds18_CalculatedTotalSumm": 0, "Nds10_CalculatedTotalSumm": 0, "Nds00_TotalSumm": 0, "Amount_Total": 2550, "Amount_Cash": 2550, "Amount_ECash": 0, "Document_Number": 249, "FiscalSign": "MQSp2Wfh", "DecimalFiscalSign": "2849597409", "Format_Version": 2, "Amount_Advance": 0, "Amount_Loan": 0, "Amount_Granting": 0, "TaxAuthority_Site": "nalog.ru", "Calculation_Place": "Магазин \"ЭССЕН\"", "Operator_INN": "" }, { "Tag": 3, "User": "ООО \"Оптовик\"", "UserInn": "1646014874", "Number": 4, "DateTime": "2019-02-05T09:40:00", "ShiftNumber": 35, "OperationType": "1", "TaxationType": 1, "Operator": "Михайлов Александр", "KKT_RegNumber": "0000000002008480    ", "FN_FactoryNumber": "9999078900011482", "Items": [{ "Name": "Кофе \"Якобс\" монарх растворимый сублимир", "Price": 14750, "Quantity": "1.000", "Total": 14750, "CalculationMethod": 4, "NDS_Rate": 1, "NDS_Summ": 2458 }], "RetailPlaceAddress": "г. Елабуга, ул. Окружное Шоссе, д. 37", "Sender_Address": "sender@mail.ru", "Nds18_TotalSumm": 2458, "Nds10_TotalSumm": 0, "NdsNA_TotalSumm": 0, "Nds18_CalculatedTotalSumm": 0, "Nds10_CalculatedTotalSumm": 0, "Nds00_TotalSumm": 0, "Amount_Total": 14750, "Amount_Cash": 14750, "Amount_ECash": 0, "Document_Number": 250, "FiscalSign": "MQQx4QCV", "DecimalFiscalSign": "836829333", "Format_Version": 2, "Amount_Advance": 0, "Amount_Loan": 0, "Amount_Granting": 0, "TaxAuthority_Site": "nalog.ru", "Calculation_Place": "Магазин \"ЭССЕН\"", "Operator_INN": "" }, { "Tag": 3, "User": "ООО \"Оптовик\"", "UserInn": "1646014874", "Number": 5, "DateTime": "2019-02-05T09:41:00", "ShiftNumber": 35, "OperationType": "1", "TaxationType": 1, "Operator": "Михайлов Александр", "KKT_RegNumber": "0000000002008480    ", "FN_FactoryNumber": "9999078900011482", "Items": [{ "Name": "Молоко \"Домик в деревне\" ультрапастеризо", "Price": 6850, "Quantity": "1.000", "Total": 6850, "CalculationMethod": 4, "NDS_Rate": 2, "NDS_Summ": 623 }], "RetailPlaceAddress": "г. Елабуга, ул. Окружное Шоссе, д. 37", "Sender_Address": "sender@mail.ru", "Nds18_TotalSumm": 0, "Nds10_TotalSumm": 623, "NdsNA_TotalSumm": 0, "Nds18_CalculatedTotalSumm": 0, "Nds10_CalculatedTotalSumm": 0, "Nds00_TotalSumm": 0, "Amount_Total": 6850, "Amount_Cash": 6850, "Amount_ECash": 0, "Document_Number": 251, "FiscalSign": "MQSvxqV1", "DecimalFiscalSign": "2949031285", "Format_Version": 2, "Amount_Advance": 0, "Amount_Loan": 0, "Amount_Granting": 0, "TaxAuthority_Site": "nalog.ru", "Calculation_Place": "Магазин \"ЭССЕН\"", "Operator_INN": "" }, { "Tag": 3, "User": "ООО \"Оптовик\"", "UserInn": "1646014874", "Number": 6, "DateTime": "2019-02-05T14:39:00", "ShiftNumber": 35, "OperationType": "1", "TaxationType": 1, "Operator": "Михайлов Александр", "KKT_RegNumber": "0000000002008480    ", "FN_FactoryNumber": "9999078900011482", "Items": [{ "Name": "Молоко \"Домик в деревне\" ультрапастеризо", "Price": 6890, "Quantity": "10.000", "Total": 68900, "CalculationMethod": 4, "NDS_Rate": 2, "NDS_Summ": 6264 }, { "Name": "Набор для выращивания растений \"Чудо Ого", "Price": 5000, "Quantity": "1.000", "Total": 5000, "CalculationMethod": 4, "NDS_Rate": 1, "NDS_Summ": 833 }], "RetailPlaceAddress": "г. Елабуга, ул. Окружное Шоссе, д. 37", "Sender_Address": "sender@mail.ru", "Nds18_TotalSumm": 833, "Nds10_TotalSumm": 6264, "NdsNA_TotalSumm": 0, "Nds18_CalculatedTotalSumm": 0, "Nds10_CalculatedTotalSumm": 0, "Nds00_TotalSumm": 0, "Amount_Total": 73900, "Amount_Cash": 73900, "Amount_ECash": 0, "Document_Number": 252, "FiscalSign": "MQTSsqWa", "DecimalFiscalSign": "3534923162", "Format_Version": 2, "Amount_Advance": 0, "Amount_Loan": 0, "Amount_Granting": 0, "TaxAuthority_Site": "nalog.ru", "Calculation_Place": "Магазин \"ЭССЕН\"", "Operator_INN": "" }, { "Tag": 3, "User": "ООО \"Оптовик\"", "UserInn": "1646014874", "Number": 7, "DateTime": "2019-02-05T15:15:00", "ShiftNumber": 35, "OperationType": "1", "TaxationType": 1, "Operator": "Михайлов Александр", "KKT_RegNumber": "0000000002008480    ", "FN_FactoryNumber": "9999078900011482", "Items": [{ "Name": "Молоко \"Домик в деревне\" ультрапастеризо", "Price": 6890, "Quantity": "25.000", "Total": 172250, "CalculationMethod": 4, "NDS_Rate": 2, "NDS_Summ": 15659 }], "RetailPlaceAddress": "г. Елабуга, ул. Окружное Шоссе, д. 37", "Sender_Address": "sender@mail.ru", "Nds18_TotalSumm": 0, "Nds10_TotalSumm": 15659, "NdsNA_TotalSumm": 0, "Nds18_CalculatedTotalSumm": 0, "Nds10_CalculatedTotalSumm": 0, "Nds00_TotalSumm": 0, "Amount_Total": 172250, "Amount_Cash": 172250, "Amount_ECash": 0, "Document_Number": 253, "FiscalSign": "MQR3J7vJ", "DecimalFiscalSign": "1999092681", "Format_Version": 2, "Amount_Advance": 0, "Amount_Loan": 0, "Amount_Granting": 0, "TaxAuthority_Site": "nalog.ru", "Calculation_Place": "Магазин \"ЭССЕН\"", "Operator_INN": "" }] };
    };
    OfdComponent.prototype.onData = function (data) {
        console.log(data.User);
        this.dataDetails = data;
        this.detailedDataViewEnabled = true;
    };
    OfdComponent.prototype.onDataItems = function (items) {
        this.itemsList = items;
        this.itemViewEnabled = true;
    };
    OfdComponent.prototype.onSubmit = function (form) {
        console.log('Получаем данные из ОФД!');
        this.getStatistic(form.value.inn, form.value.kkt, 'all', form.value.dateFrom, form.value.dateTo);
    };
    OfdComponent.prototype.onAuth = function () {
        if (!this.ofdService.isAuthenticated()) {
            this.ofdService.authorise();
            console.log('Авторизовались!');
        }
        ;
    };
    OfdComponent.prototype.onLogout = function () {
        this.ofdService.logout();
    };
    /**
     * Получение списка ККТС
     * @param inn - ИНН.
     */
    OfdComponent.prototype.getKktsList = function (inn) {
        var _this = this;
        if (this.ofdService.isAuthenticated()) {
            this.ofdService.getKkts(inn).subscribe(function (response) {
                _this.kktsList = response;
                _this.showSuccess();
            }, function (error) {
                console.log('Ошибка обращения к сервису авторизации!');
                console.log(error);
                _this.showError(error.valueOf());
            });
        }
    };
    /**
     * Получение статистики.
     * @param inn - ИНН
     * @param marker - маркер для фильтра
     * @param dateFrom дата начала
     * @param dateTo - дата окончания
     */
    OfdComponent.prototype.getStatistic = function (inn, kkt, marker, dateFrom, dateTo) {
        var _this = this;
        if (this.ofdService.isAuthenticated()) {
            this.ofdService.getReceiptsStatistic(inn, kkt, marker, dateFrom, dateTo).subscribe(function (response) {
                if (response) {
                    _this.processStatResponse(response);
                    _this.statistics = response;
                }
            }, function (error) {
                console.log('Ошибка обращения к сервису авторизации!');
                console.log(error);
                _this.showError(error.valueOf());
            }, function () {
                _this.showSuccess();
            });
        }
    };
    OfdComponent.prototype.test = function () {
        this.statistics = this.example;
        this.processStatResponse(this.example);
    };
    OfdComponent.prototype.processStatResponse = function (response) {
        var stats = response.Statistics;
        var azs = stats.find(function (stats) { return stats.StatisticsMarker === 'AZS'; });
        this.azsStat = new _model_ofd_WidgetStatInfo__WEBPACK_IMPORTED_MODULE_4__["WidgetStatInfo"]();
        this.azsStat.name = azs.Name;
        this.azsStat.value = azs.Value / 100 + ' руб. ';
        var cash = stats.find(function (stats) { return stats.StatisticsMarker == 'CASH'; });
        this.cashStat = new _model_ofd_WidgetStatInfo__WEBPACK_IMPORTED_MODULE_4__["WidgetStatInfo"]();
        this.cashStat.name = 'Оплата наличными';
        this.cashStat.value = cash.Value / 100 + ' руб. ';
        var ecash = stats.find(function (stats) { return stats.StatisticsMarker == 'ECASH'; });
        this.ecashStat = new _model_ofd_WidgetStatInfo__WEBPACK_IMPORTED_MODULE_4__["WidgetStatInfo"]();
        this.ecashStat.name = 'Оплата картой';
        this.ecashStat.value = ecash.Value / 100 + ' руб. ';
        var operator = stats.find(function (stats) { return stats.StatisticsMarker == 'OPERATOR'; });
        this.operatorStat = new _model_ofd_WidgetStatInfo__WEBPACK_IMPORTED_MODULE_4__["WidgetStatInfo"]();
        this.operatorStat.name = operator.Name;
        this.operatorStat.value = operator.Value / 100 + ' руб. ';
        var item = stats.find(function (stats) { return stats.StatisticsMarker == 'ITEM'; });
        this.itemStat = new _model_ofd_WidgetStatInfo__WEBPACK_IMPORTED_MODULE_4__["WidgetStatInfo"]();
        this.itemStat.name = item.Name;
        this.itemStat.value = item.Value / 100 + ' руб. ';
    };
    //TODO: make shared super component
    OfdComponent.prototype.showWarn = function (serviceMessage) {
        this.messageService.add({ severity: 'warn', summary: 'Ошибка получения данных', detail: 'Данные ещё не обработаны: ' +
                serviceMessage });
    };
    OfdComponent.prototype.showSuccess = function () {
        this.messageService.add({ severity: 'success', summary: 'Получение', detail: 'Данные получены!' });
    };
    OfdComponent.prototype.showError = function (serviceMessage) {
        this.messageService.add({ severity: 'error', summary: 'Ошибка', detail: 'Ошибка обращения к сервису: ' +
                serviceMessage });
    };
    OfdComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-ofd',
            template: __webpack_require__(/*! ./ofd.component.html */ "./src/app/pages/ofd/ofd.component.html"),
            providers: [primeng_api__WEBPACK_IMPORTED_MODULE_2__["MessageService"]],
            styles: [__webpack_require__(/*! ./ofd.component.css */ "./src/app/pages/ofd/ofd.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [primeng_api__WEBPACK_IMPORTED_MODULE_2__["MessageService"], _services_ofd_service__WEBPACK_IMPORTED_MODULE_3__["OfdService"], _angular_platform_browser__WEBPACK_IMPORTED_MODULE_5__["DomSanitizer"]])
    ], OfdComponent);
    return OfdComponent;
}());



/***/ }),

/***/ "./src/app/pages/preloader/preloader.component.css":
/*!*********************************************************!*\
  !*** ./src/app/pages/preloader/preloader.component.css ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".peeek-loading {\r\n    background-color: #38d368;\r\n    overflow: hidden;\r\n    position: absolute;\r\n    top: 0;\r\n    bottom: 0;\r\n    left: 0;\r\n    right: 0;\r\n    height: 100%;\r\n    width: 100%;\r\n}\r\n\r\n.peeek-loading ul {\r\n    position: absolute;\r\n    left: calc(50% - 0.7em);\r\n    top: calc(50% - 4.2em);\r\n    display: inline-block;\r\n    text-indent: 2.8em;\r\n}\r\n\r\n.peeek-loading ul li:after,\r\n.peeek-loading ul:after {\r\n    width: 1.4em;\r\n    height: 1.4em;\r\n    background-color: #fff;\r\n    border-radius: 100%;\r\n}\r\n\r\n.peeek-loading ul li:after,\r\n.peeek-loading ul:after {\r\n    content: \"\";\r\n    display: block;\r\n}\r\n\r\n.peeek-loading ul:after {\r\n    position: absolute;\r\n    top: 2.8em;\r\n}\r\n\r\n.peeek-loading li {\r\n    position: absolute;\r\n    padding-bottom: 5.6em;\r\n    top: 0;\r\n    left: 0;\r\n}\r\n\r\n.peeek-loading li:nth-child(1) {\r\n    -webkit-transform: rotate(0deg);\r\n            transform: rotate(0deg);\r\n    -webkit-animation-delay: 0.125s;\r\n            animation-delay: 0.125s;\r\n}\r\n\r\n.peeek-loading li:nth-child(1):after {\r\n    -webkit-animation-delay: 0.125s;\r\n            animation-delay: 0.125s;\r\n}\r\n\r\n.peeek-loading li:nth-child(2) {\r\n    -webkit-transform: rotate(36deg);\r\n            transform: rotate(36deg);\r\n    -webkit-animation-delay: 0.25s;\r\n            animation-delay: 0.25s;\r\n}\r\n\r\n.peeek-loading li:nth-child(2):after {\r\n    -webkit-animation-delay: 0.25s;\r\n            animation-delay: 0.25s;\r\n}\r\n\r\n.peeek-loading li:nth-child(3) {\r\n    -webkit-transform: rotate(72deg);\r\n            transform: rotate(72deg);\r\n    -webkit-animation-delay: 0.375s;\r\n            animation-delay: 0.375s;\r\n}\r\n\r\n.peeek-loading li:nth-child(3):after {\r\n    -webkit-animation-delay: 0.375s;\r\n            animation-delay: 0.375s;\r\n}\r\n\r\n.peeek-loading li:nth-child(4) {\r\n    -webkit-transform: rotate(108deg);\r\n            transform: rotate(108deg);\r\n    -webkit-animation-delay: 0.5s;\r\n            animation-delay: 0.5s;\r\n}\r\n\r\n.peeek-loading li:nth-child(4):after {\r\n    -webkit-animation-delay: 0.5s;\r\n            animation-delay: 0.5s;\r\n}\r\n\r\n.peeek-loading li:nth-child(5) {\r\n    -webkit-transform: rotate(144deg);\r\n            transform: rotate(144deg);\r\n    -webkit-animation-delay: 0.625s;\r\n            animation-delay: 0.625s;\r\n}\r\n\r\n.peeek-loading li:nth-child(5):after {\r\n    -webkit-animation-delay: 0.625s;\r\n            animation-delay: 0.625s;\r\n}\r\n\r\n.peeek-loading li:nth-child(6) {\r\n    -webkit-transform: rotate(180deg);\r\n            transform: rotate(180deg);\r\n    -webkit-animation-delay: 0.75s;\r\n            animation-delay: 0.75s;\r\n}\r\n\r\n.peeek-loading li:nth-child(6):after {\r\n    -webkit-animation-delay: 0.75s;\r\n            animation-delay: 0.75s;\r\n}\r\n\r\n.peeek-loading li:nth-child(7) {\r\n    -webkit-transform: rotate(216deg);\r\n            transform: rotate(216deg);\r\n    -webkit-animation-delay: 0.875s;\r\n            animation-delay: 0.875s;\r\n}\r\n\r\n.peeek-loading li:nth-child(7):after {\r\n    -webkit-animation-delay: 0.875s;\r\n            animation-delay: 0.875s;\r\n}\r\n\r\n.peeek-loading li:nth-child(8) {\r\n    -webkit-transform: rotate(252deg);\r\n            transform: rotate(252deg);\r\n    -webkit-animation-delay: 1s;\r\n            animation-delay: 1s;\r\n}\r\n\r\n.peeek-loading li:nth-child(8):after {\r\n    -webkit-animation-delay: 1s;\r\n            animation-delay: 1s;\r\n}\r\n\r\n.peeek-loading li:nth-child(9) {\r\n    -webkit-transform: rotate(288deg);\r\n            transform: rotate(288deg);\r\n    -webkit-animation-delay: 1.125s;\r\n            animation-delay: 1.125s;\r\n}\r\n\r\n.peeek-loading li:nth-child(9):after {\r\n    -webkit-animation-delay: 1.125s;\r\n            animation-delay: 1.125s;\r\n}\r\n\r\n.peeek-loading li:nth-child(10) {\r\n    -webkit-transform: rotate(324deg);\r\n            transform: rotate(324deg);\r\n    -webkit-animation-delay: 1.25s;\r\n            animation-delay: 1.25s;\r\n}\r\n\r\n.peeek-loading li:nth-child(10):after {\r\n    -webkit-animation-delay: 1.25s;\r\n            animation-delay: 1.25s;\r\n}\r\n\r\n.peeek-loading li {\r\n    -webkit-animation: dotAnimation 2.5s infinite;\r\n            animation: dotAnimation 2.5s infinite;\r\n}\r\n\r\n@-webkit-keyframes dotAnimation {\r\n    0%, 55%, 100% {\r\n        padding: 0 0 5.6em 0;\r\n    }\r\n    5%,50% {\r\n        padding: 2.8em 0;\r\n    }\r\n}\r\n\r\n@keyframes dotAnimation {\r\n    0%, 55%, 100% {\r\n        padding: 0 0 5.6em 0;\r\n    }\r\n    5%,50% {\r\n        padding: 2.8em 0;\r\n    }\r\n}\r\n\r\n.peeek-loading li:after {\r\n    -webkit-animation: dotAnimationTwo 2.5s infinite;\r\n            animation: dotAnimationTwo 2.5s infinite;\r\n}\r\n\r\n@-webkit-keyframes dotAnimationTwo {\r\n    0%, 55%, 100% {\r\n        opacity: 1;\r\n        -webkit-transform: scale(1);\r\n                transform: scale(1);\r\n    }\r\n    5%,50% {\r\n        opacity: .5;\r\n        -webkit-transform: scale(0.5);\r\n                transform: scale(0.5);\r\n    }\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvcHJlbG9hZGVyL3ByZWxvYWRlci5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0lBQ0kseUJBQXlCO0lBQ3pCLGdCQUFnQjtJQUNoQixrQkFBa0I7SUFDbEIsTUFBTTtJQUNOLFNBQVM7SUFDVCxPQUFPO0lBQ1AsUUFBUTtJQUNSLFlBQVk7SUFDWixXQUFXO0FBQ2Y7O0FBRUE7SUFDSSxrQkFBa0I7SUFDbEIsdUJBQXVCO0lBQ3ZCLHNCQUFzQjtJQUN0QixxQkFBcUI7SUFDckIsa0JBQWtCO0FBQ3RCOztBQUVBOztJQUVJLFlBQVk7SUFDWixhQUFhO0lBQ2Isc0JBQXNCO0lBQ3RCLG1CQUFtQjtBQUN2Qjs7QUFFQTs7SUFFSSxXQUFXO0lBQ1gsY0FBYztBQUNsQjs7QUFFQTtJQUNJLGtCQUFrQjtJQUNsQixVQUFVO0FBQ2Q7O0FBRUE7SUFDSSxrQkFBa0I7SUFDbEIscUJBQXFCO0lBQ3JCLE1BQU07SUFDTixPQUFPO0FBQ1g7O0FBRUE7SUFDSSwrQkFBdUI7WUFBdkIsdUJBQXVCO0lBQ3ZCLCtCQUF1QjtZQUF2Qix1QkFBdUI7QUFDM0I7O0FBRUE7SUFDSSwrQkFBdUI7WUFBdkIsdUJBQXVCO0FBQzNCOztBQUVBO0lBQ0ksZ0NBQXdCO1lBQXhCLHdCQUF3QjtJQUN4Qiw4QkFBc0I7WUFBdEIsc0JBQXNCO0FBQzFCOztBQUVBO0lBQ0ksOEJBQXNCO1lBQXRCLHNCQUFzQjtBQUMxQjs7QUFFQTtJQUNJLGdDQUF3QjtZQUF4Qix3QkFBd0I7SUFDeEIsK0JBQXVCO1lBQXZCLHVCQUF1QjtBQUMzQjs7QUFFQTtJQUNJLCtCQUF1QjtZQUF2Qix1QkFBdUI7QUFDM0I7O0FBRUE7SUFDSSxpQ0FBeUI7WUFBekIseUJBQXlCO0lBQ3pCLDZCQUFxQjtZQUFyQixxQkFBcUI7QUFDekI7O0FBRUE7SUFDSSw2QkFBcUI7WUFBckIscUJBQXFCO0FBQ3pCOztBQUVBO0lBQ0ksaUNBQXlCO1lBQXpCLHlCQUF5QjtJQUN6QiwrQkFBdUI7WUFBdkIsdUJBQXVCO0FBQzNCOztBQUVBO0lBQ0ksK0JBQXVCO1lBQXZCLHVCQUF1QjtBQUMzQjs7QUFFQTtJQUNJLGlDQUF5QjtZQUF6Qix5QkFBeUI7SUFDekIsOEJBQXNCO1lBQXRCLHNCQUFzQjtBQUMxQjs7QUFFQTtJQUNJLDhCQUFzQjtZQUF0QixzQkFBc0I7QUFDMUI7O0FBRUE7SUFDSSxpQ0FBeUI7WUFBekIseUJBQXlCO0lBQ3pCLCtCQUF1QjtZQUF2Qix1QkFBdUI7QUFDM0I7O0FBRUE7SUFDSSwrQkFBdUI7WUFBdkIsdUJBQXVCO0FBQzNCOztBQUVBO0lBQ0ksaUNBQXlCO1lBQXpCLHlCQUF5QjtJQUN6QiwyQkFBbUI7WUFBbkIsbUJBQW1CO0FBQ3ZCOztBQUVBO0lBQ0ksMkJBQW1CO1lBQW5CLG1CQUFtQjtBQUN2Qjs7QUFFQTtJQUNJLGlDQUF5QjtZQUF6Qix5QkFBeUI7SUFDekIsK0JBQXVCO1lBQXZCLHVCQUF1QjtBQUMzQjs7QUFFQTtJQUNJLCtCQUF1QjtZQUF2Qix1QkFBdUI7QUFDM0I7O0FBRUE7SUFDSSxpQ0FBeUI7WUFBekIseUJBQXlCO0lBQ3pCLDhCQUFzQjtZQUF0QixzQkFBc0I7QUFDMUI7O0FBRUE7SUFDSSw4QkFBc0I7WUFBdEIsc0JBQXNCO0FBQzFCOztBQUVBO0lBQ0ksNkNBQXFDO1lBQXJDLHFDQUFxQztBQUN6Qzs7QUFFQTtJQUNJO1FBQ0ksb0JBQW9CO0lBQ3hCO0lBQ0E7UUFDSSxnQkFBZ0I7SUFDcEI7QUFDSjs7QUFQQTtJQUNJO1FBQ0ksb0JBQW9CO0lBQ3hCO0lBQ0E7UUFDSSxnQkFBZ0I7SUFDcEI7QUFDSjs7QUFDQTtJQUNJLGdEQUF3QztZQUF4Qyx3Q0FBd0M7QUFDNUM7O0FBRUE7SUFDSTtRQUNJLFVBQVU7UUFDViwyQkFBbUI7Z0JBQW5CLG1CQUFtQjtJQUN2QjtJQUNBO1FBQ0ksV0FBVztRQUNYLDZCQUFxQjtnQkFBckIscUJBQXFCO0lBQ3pCO0FBQ0oiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy9wcmVsb2FkZXIvcHJlbG9hZGVyLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIucGVlZWstbG9hZGluZyB7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjMzhkMzY4O1xyXG4gICAgb3ZlcmZsb3c6IGhpZGRlbjtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIHRvcDogMDtcclxuICAgIGJvdHRvbTogMDtcclxuICAgIGxlZnQ6IDA7XHJcbiAgICByaWdodDogMDtcclxuICAgIGhlaWdodDogMTAwJTtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG59XHJcblxyXG4ucGVlZWstbG9hZGluZyB1bCB7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICBsZWZ0OiBjYWxjKDUwJSAtIDAuN2VtKTtcclxuICAgIHRvcDogY2FsYyg1MCUgLSA0LjJlbSk7XHJcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgICB0ZXh0LWluZGVudDogMi44ZW07XHJcbn1cclxuXHJcbi5wZWVlay1sb2FkaW5nIHVsIGxpOmFmdGVyLFxyXG4ucGVlZWstbG9hZGluZyB1bDphZnRlciB7XHJcbiAgICB3aWR0aDogMS40ZW07XHJcbiAgICBoZWlnaHQ6IDEuNGVtO1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZjtcclxuICAgIGJvcmRlci1yYWRpdXM6IDEwMCU7XHJcbn1cclxuXHJcbi5wZWVlay1sb2FkaW5nIHVsIGxpOmFmdGVyLFxyXG4ucGVlZWstbG9hZGluZyB1bDphZnRlciB7XHJcbiAgICBjb250ZW50OiBcIlwiO1xyXG4gICAgZGlzcGxheTogYmxvY2s7XHJcbn1cclxuXHJcbi5wZWVlay1sb2FkaW5nIHVsOmFmdGVyIHtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIHRvcDogMi44ZW07XHJcbn1cclxuXHJcbi5wZWVlay1sb2FkaW5nIGxpIHtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIHBhZGRpbmctYm90dG9tOiA1LjZlbTtcclxuICAgIHRvcDogMDtcclxuICAgIGxlZnQ6IDA7XHJcbn1cclxuXHJcbi5wZWVlay1sb2FkaW5nIGxpOm50aC1jaGlsZCgxKSB7XHJcbiAgICB0cmFuc2Zvcm06IHJvdGF0ZSgwZGVnKTtcclxuICAgIGFuaW1hdGlvbi1kZWxheTogMC4xMjVzO1xyXG59XHJcblxyXG4ucGVlZWstbG9hZGluZyBsaTpudGgtY2hpbGQoMSk6YWZ0ZXIge1xyXG4gICAgYW5pbWF0aW9uLWRlbGF5OiAwLjEyNXM7XHJcbn1cclxuXHJcbi5wZWVlay1sb2FkaW5nIGxpOm50aC1jaGlsZCgyKSB7XHJcbiAgICB0cmFuc2Zvcm06IHJvdGF0ZSgzNmRlZyk7XHJcbiAgICBhbmltYXRpb24tZGVsYXk6IDAuMjVzO1xyXG59XHJcblxyXG4ucGVlZWstbG9hZGluZyBsaTpudGgtY2hpbGQoMik6YWZ0ZXIge1xyXG4gICAgYW5pbWF0aW9uLWRlbGF5OiAwLjI1cztcclxufVxyXG5cclxuLnBlZWVrLWxvYWRpbmcgbGk6bnRoLWNoaWxkKDMpIHtcclxuICAgIHRyYW5zZm9ybTogcm90YXRlKDcyZGVnKTtcclxuICAgIGFuaW1hdGlvbi1kZWxheTogMC4zNzVzO1xyXG59XHJcblxyXG4ucGVlZWstbG9hZGluZyBsaTpudGgtY2hpbGQoMyk6YWZ0ZXIge1xyXG4gICAgYW5pbWF0aW9uLWRlbGF5OiAwLjM3NXM7XHJcbn1cclxuXHJcbi5wZWVlay1sb2FkaW5nIGxpOm50aC1jaGlsZCg0KSB7XHJcbiAgICB0cmFuc2Zvcm06IHJvdGF0ZSgxMDhkZWcpO1xyXG4gICAgYW5pbWF0aW9uLWRlbGF5OiAwLjVzO1xyXG59XHJcblxyXG4ucGVlZWstbG9hZGluZyBsaTpudGgtY2hpbGQoNCk6YWZ0ZXIge1xyXG4gICAgYW5pbWF0aW9uLWRlbGF5OiAwLjVzO1xyXG59XHJcblxyXG4ucGVlZWstbG9hZGluZyBsaTpudGgtY2hpbGQoNSkge1xyXG4gICAgdHJhbnNmb3JtOiByb3RhdGUoMTQ0ZGVnKTtcclxuICAgIGFuaW1hdGlvbi1kZWxheTogMC42MjVzO1xyXG59XHJcblxyXG4ucGVlZWstbG9hZGluZyBsaTpudGgtY2hpbGQoNSk6YWZ0ZXIge1xyXG4gICAgYW5pbWF0aW9uLWRlbGF5OiAwLjYyNXM7XHJcbn1cclxuXHJcbi5wZWVlay1sb2FkaW5nIGxpOm50aC1jaGlsZCg2KSB7XHJcbiAgICB0cmFuc2Zvcm06IHJvdGF0ZSgxODBkZWcpO1xyXG4gICAgYW5pbWF0aW9uLWRlbGF5OiAwLjc1cztcclxufVxyXG5cclxuLnBlZWVrLWxvYWRpbmcgbGk6bnRoLWNoaWxkKDYpOmFmdGVyIHtcclxuICAgIGFuaW1hdGlvbi1kZWxheTogMC43NXM7XHJcbn1cclxuXHJcbi5wZWVlay1sb2FkaW5nIGxpOm50aC1jaGlsZCg3KSB7XHJcbiAgICB0cmFuc2Zvcm06IHJvdGF0ZSgyMTZkZWcpO1xyXG4gICAgYW5pbWF0aW9uLWRlbGF5OiAwLjg3NXM7XHJcbn1cclxuXHJcbi5wZWVlay1sb2FkaW5nIGxpOm50aC1jaGlsZCg3KTphZnRlciB7XHJcbiAgICBhbmltYXRpb24tZGVsYXk6IDAuODc1cztcclxufVxyXG5cclxuLnBlZWVrLWxvYWRpbmcgbGk6bnRoLWNoaWxkKDgpIHtcclxuICAgIHRyYW5zZm9ybTogcm90YXRlKDI1MmRlZyk7XHJcbiAgICBhbmltYXRpb24tZGVsYXk6IDFzO1xyXG59XHJcblxyXG4ucGVlZWstbG9hZGluZyBsaTpudGgtY2hpbGQoOCk6YWZ0ZXIge1xyXG4gICAgYW5pbWF0aW9uLWRlbGF5OiAxcztcclxufVxyXG5cclxuLnBlZWVrLWxvYWRpbmcgbGk6bnRoLWNoaWxkKDkpIHtcclxuICAgIHRyYW5zZm9ybTogcm90YXRlKDI4OGRlZyk7XHJcbiAgICBhbmltYXRpb24tZGVsYXk6IDEuMTI1cztcclxufVxyXG5cclxuLnBlZWVrLWxvYWRpbmcgbGk6bnRoLWNoaWxkKDkpOmFmdGVyIHtcclxuICAgIGFuaW1hdGlvbi1kZWxheTogMS4xMjVzO1xyXG59XHJcblxyXG4ucGVlZWstbG9hZGluZyBsaTpudGgtY2hpbGQoMTApIHtcclxuICAgIHRyYW5zZm9ybTogcm90YXRlKDMyNGRlZyk7XHJcbiAgICBhbmltYXRpb24tZGVsYXk6IDEuMjVzO1xyXG59XHJcblxyXG4ucGVlZWstbG9hZGluZyBsaTpudGgtY2hpbGQoMTApOmFmdGVyIHtcclxuICAgIGFuaW1hdGlvbi1kZWxheTogMS4yNXM7XHJcbn1cclxuXHJcbi5wZWVlay1sb2FkaW5nIGxpIHtcclxuICAgIGFuaW1hdGlvbjogZG90QW5pbWF0aW9uIDIuNXMgaW5maW5pdGU7XHJcbn1cclxuXHJcbkBrZXlmcmFtZXMgZG90QW5pbWF0aW9uIHtcclxuICAgIDAlLCA1NSUsIDEwMCUge1xyXG4gICAgICAgIHBhZGRpbmc6IDAgMCA1LjZlbSAwO1xyXG4gICAgfVxyXG4gICAgNSUsNTAlIHtcclxuICAgICAgICBwYWRkaW5nOiAyLjhlbSAwO1xyXG4gICAgfVxyXG59XHJcbi5wZWVlay1sb2FkaW5nIGxpOmFmdGVyIHtcclxuICAgIGFuaW1hdGlvbjogZG90QW5pbWF0aW9uVHdvIDIuNXMgaW5maW5pdGU7XHJcbn1cclxuXHJcbkAtd2Via2l0LWtleWZyYW1lcyBkb3RBbmltYXRpb25Ud28ge1xyXG4gICAgMCUsIDU1JSwgMTAwJSB7XHJcbiAgICAgICAgb3BhY2l0eTogMTtcclxuICAgICAgICB0cmFuc2Zvcm06IHNjYWxlKDEpO1xyXG4gICAgfVxyXG4gICAgNSUsNTAlIHtcclxuICAgICAgICBvcGFjaXR5OiAuNTtcclxuICAgICAgICB0cmFuc2Zvcm06IHNjYWxlKDAuNSk7XHJcbiAgICB9XHJcbn1cclxuIl19 */"

/***/ }),

/***/ "./src/app/pages/preloader/preloader.component.html":
/*!**********************************************************!*\
  !*** ./src/app/pages/preloader/preloader.component.html ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h5>GazProm...</h5>\r\n<div class='peeek-loading'>\r\n    <ul>\r\n        <li></li>\r\n        <li></li>\r\n        <li></li>\r\n        <li></li>\r\n        <li></li>\r\n        <li></li>\r\n        <li></li>\r\n        <li></li>\r\n        <li></li>\r\n        <li></li>\r\n    </ul>\r\n</div>"

/***/ }),

/***/ "./src/app/pages/preloader/preloader.component.ts":
/*!********************************************************!*\
  !*** ./src/app/pages/preloader/preloader.component.ts ***!
  \********************************************************/
/*! exports provided: PreloaderComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PreloaderComponent", function() { return PreloaderComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var PreloaderComponent = /** @class */ (function () {
    function PreloaderComponent() {
    }
    PreloaderComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-preloader',
            template: __webpack_require__(/*! ./preloader.component.html */ "./src/app/pages/preloader/preloader.component.html"),
            styles: [__webpack_require__(/*! ./preloader.component.css */ "./src/app/pages/preloader/preloader.component.css")]
        })
    ], PreloaderComponent);
    return PreloaderComponent;
}());



/***/ }),

/***/ "./src/app/pages/scout/scout.component.css":
/*!*************************************************!*\
  !*** ./src/app/pages/scout/scout.component.css ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3Njb3V0L3Njb3V0LmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/pages/scout/scout.component.html":
/*!**************************************************!*\
  !*** ./src/app/pages/scout/scout.component.html ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container-fluid align-self-end\">\r\n    <p-panel header=\"Получение статистики из системы СКАУТ\">\r\n        <p-tabView>\r\n            <p-tabPanel header=\"топливо\">\r\n\r\n                <app-unit-picker (selChanged)=\"selectorChanged($event)\"></app-unit-picker>\r\n                <div class=\"form-group container\">\r\n                    <div class=\"row\">\r\n                        <label class=\"control-label col-sm-2\">По дням</label>\r\n                        <p-inputSwitch (onChange)=\"handleFuelDaylyChange($event)\"\r\n                                       [(ngModel)]=\"daylyFuelChecked\"\r\n                                       class=\"col-sm-2\"\r\n                                       styleClass=\"padding-left: 20px\">По дням\r\n                        </p-inputSwitch>\r\n                    </div>\r\n\r\n                    <div class=\"row\">\r\n                        <div class=\"col-sm-2\"></div>\r\n                        <p-button (onClick)=\"fuel($event)\"\r\n                                  class=\"col-sm-2\"\r\n                                  label=\"Топливо\"\r\n                                  style=\"padding-left: 20px\"\r\n                                  styleClass=\"ui-button-success\"></p-button>\r\n                    </div>\r\n                </div>\r\n\r\n                <h5 *ngIf=\"fuelStatistic\">Ответ по топливу:</h5>\r\n                <br>\r\n\r\n\r\n                <table *ngIf=\"fuelStatistic\" class=\"table table-bordered\">\r\n                    <thead class=\"thead-light\">\r\n                    <tr>\r\n                        <th scope=\"col-sm-4\">Дата</th>\r\n                        <th scope=\"col-sm-4\">Количество</th>\r\n                        <th scope=\"col-sm-4\">Примечание</th>\r\n                    </tr>\r\n                    </thead>\r\n                    <tbody *ngFor=\"let interval of fuelStatistic?.intervals\">\r\n                    <tr>\r\n                        <td>{{interval.begin | date:'dd/MM/yy'}} - {{interval.end | date:'dd/MM/yy'}} </td>\r\n                        <td>{{interval.beginFuel.value}}</td>\r\n                        <td>{{interval.beginFuel.noValueReason}}</td>\r\n                    </tr>\r\n                    </tbody>\r\n                </table>\r\n                <p-accordion>\r\n                    <p-accordionTab header=\"Диаграмма последовательности для отчета по топливу\">\r\n                        <img class=\"img-fluid\" id=\"scheme_infr\" src=\"assets/images/scout/fuel_scheme.png\">\r\n                    </p-accordionTab>\r\n                </p-accordion>\r\n\r\n            </p-tabPanel>\r\n            <p-tabPanel header=\"одометр\">\r\n                <app-unit-picker (selChanged)=\"selectorChanged($event)\"></app-unit-picker>\r\n\r\n                <div class=\"form-group container\">\r\n                    <label class=\"control-label col-sm-2\">По дням</label>\r\n                    <p-inputSwitch\r\n                            (onChange)=\"handleOdoDaylyChange($event)\"\r\n                            [(ngModel)]=\"daylyOdoChecked\"\r\n                            class=\"col-sm-2\"\r\n                            styleClass=\"padding-left: 20px\">По дням\r\n                    </p-inputSwitch>\r\n\r\n                    <div class=\"row\">\r\n                        <div class=\"col-sm-2\"></div>\r\n                        <p-button (onClick)=\"odometer($event)\"\r\n                                  class=\"col-sm-2\"\r\n                                  label=\"Одометр\"\r\n                                  style=\"padding-left: 20px\"\r\n                                  styleClass=\"ui-button-success\"></p-button>\r\n                    </div>\r\n                </div>\r\n\r\n                <h5 *ngIf=\"odoStatistic\">Показания по одометру:</h5>\r\n                <br>\r\n\r\n\r\n                <table *ngIf=\"odoStatistic\" class=\"table table-bordered\">\r\n                    <thead class=\"thead-light\">\r\n                    <tr>\r\n                        <th scope=\"col-sm-4\">Дата</th>\r\n                        <th scope=\"col-sm-4\">Показанания начало</th>\r\n                        <th scope=\"col-sm-4\">Показанания конец</th>\r\n                        <th scope=\"col-sm-4\">Итоговое показание</th>\r\n                        <th scope=\"col-sm-4\">Примечание</th>\r\n                    </tr>\r\n                    </thead>\r\n                    <tbody *ngFor=\"let odointerv of odoStatistic?.intervals\">\r\n                    <tr>\r\n                        <td>{{odointerv.begin | date:'dd/MM/yy'}} - {{odointerv.end | date:'dd/MM/yy'}} </td>\r\n                        <td>{{odointerv.beginMileageKm.value}}</td>\r\n                        <td>{{odointerv.endMileageKm.value}}</td>\r\n                        <td>{{odointerv.totalMileageKm.value}}</td>\r\n                        <td>{{odointerv.totalMileageKm.noValueReason}} {{odointerv.beginMileageKm.noValueReason}}\r\n                            {{odointerv.totalMileageKm.noValueReason}}</td>\r\n                        <!--<td>{{odointerv.}}</td>-->\r\n                    </tr>\r\n                    </tbody>\r\n                </table>\r\n                <p-accordion>\r\n                    <p-accordionTab header=\"Диаграмма последовательности для отчета по топливу\">\r\n                        <img class=\"img-fluid\" id=\"scheme2\" src=\"assets/images/scout/odo_scheme.png\">\r\n                    </p-accordionTab>\r\n                </p-accordion>\r\n            </p-tabPanel>\r\n\r\n            <p-tabPanel header=\"местоположение\">\r\n                <app-unit-picker (selChanged)=\"selectorChanged($event)\"></app-unit-picker>\r\n\r\n                <div class=\"form-group container\">\r\n                    <label class=\"control-label col-sm-2\">По дням</label>\r\n                    <p-inputSwitch\r\n                            [(ngModel)]=\"daylyTracksChecked\"\r\n                            class=\"col-sm-2\"\r\n                            styleClass=\"padding-left: 20px\">По дням\r\n                    </p-inputSwitch>\r\n                    <div class=\"row\">\r\n                        <div class=\"col-sm-2\"></div>\r\n                        <p-button (onClick)=\"tracks($event)\"\r\n                                  class=\"col-sm-2\"\r\n                                  label=\"Местоположение\"\r\n                                  style=\"padding-left: 20px\"\r\n                                  styleClass=\"ui-button-success\"></p-button>\r\n                    </div>\r\n                </div>\r\n\r\n                <h5 *ngIf=\"tracksStatistic\">Данные о местоположении:</h5>\r\n                <br>\r\n\r\n                <p-table id=\"scout_geo_table\"\r\n                         class=\"table-striped\"\r\n                         [paginator]=\"true\"\r\n                         [responsive]=\"true\"\r\n                         [rowsPerPageOptions]=\"[5,10,20,50]\"\r\n                         [rows]=\"10\"\r\n                         [value]=\"tracksStatistic?.trackPoints\"\r\n                         *ngIf=\"tracksStatistic\"\r\n                >\r\n                    <ng-template pTemplate=\"header\">\r\n                        <tr>\r\n                            <th>Время</th>\r\n                            <th>Широта</th>\r\n                            <th>Долгота</th>\r\n                            <th>Пробег</th>\r\n                            <th>Длительность</th>\r\n                            <th>Скорость</th>\r\n                        </tr>\r\n                    </ng-template>\r\n                    <ng-template let-track let-rowIndex=\"id\" pTemplate=\"body\">\r\n                        <tr [pSelectableRow]=\"track\">\r\n                            <td>{{track.time | date:'dd/MM/yy'}}</td>\r\n                            <td>{{track.lat}}</td>\r\n                            <td>{{track.lon}}</td>\r\n                            <td>{{track.mileageM}}</td>\r\n                            <td>{{track.duration}}</td>\r\n                            <td>{{track.speedKmh}}</td>\r\n                        </tr>\r\n                    </ng-template>\r\n                </p-table>\r\n\r\n\r\n\r\n                <p-accordion>\r\n                    <p-accordionTab header=\"Диаграмма последовательности для отчета по местоположеню\">\r\n                        <img class=\"img-fluid\" id=\"scheme2\" src=\"assets/images/scout/location_scheme.png\">\r\n                    </p-accordionTab>\r\n                </p-accordion>\r\n            </p-tabPanel>\r\n\r\n            <p-tabPanel header=\"Инфраструктура\">\r\n                <img class=\"img-fluid\" id=\"scheme1\" src=\"assets/images/scout/infr_scheme.png\">\r\n            </p-tabPanel>\r\n            <p-tabPanel header=\"Swagger\">\r\n                <div class=\"embed-responsive embed-responsive-16by9\">\r\n                    <iframe [src]=\"trustedSwaggerUrl\"\r\n                            class=\"embed-responsive-item\"></iframe>\r\n                </div>\r\n            </p-tabPanel>\r\n        </p-tabView>\r\n    </p-panel>\r\n</div>\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n"

/***/ }),

/***/ "./src/app/pages/scout/scout.component.ts":
/*!************************************************!*\
  !*** ./src/app/pages/scout/scout.component.ts ***!
  \************************************************/
/*! exports provided: ScoutComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ScoutComponent", function() { return ScoutComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_scout_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/scout.service */ "./src/app/services/scout.service.ts");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var primeng_api__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! primeng/api */ "./node_modules/primeng/api.js");
/* harmony import */ var primeng_api__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(primeng_api__WEBPACK_IMPORTED_MODULE_5__);






var ScoutComponent = /** @class */ (function () {
    function ScoutComponent(scoutService, sanitizer, messageService) {
        this.scoutService = scoutService;
        this.sanitizer = sanitizer;
        this.messageService = messageService;
        this.title = 'front';
        this.units = [{ id: 51005 }, { id: 51006 }, { id: 51007 }, { id: 51008 }, { id: 51010 }, { id: 51011 }, { id: 51164 }];
        this._swaggerUrl = _environments_environment__WEBPACK_IMPORTED_MODULE_4__["environment"].scoutUrl + '/webjars/swagger-ui/index.html?url='
            + _environments_environment__WEBPACK_IMPORTED_MODULE_4__["environment"].scoutUrl + '/scout/api-doc';
        this.trustedSwaggerUrl = sanitizer.bypassSecurityTrustResourceUrl(this._swaggerUrl);
    }
    ScoutComponent.prototype.ngOnInit = function () {
    };
    ScoutComponent.prototype.tracks = function () {
        var _this = this;
        console.log('Получение даннных о местоположении');
        this.scoutService.getTracks(this.selectedUnit, this.startDate, this.endDate, this.daylyTracksChecked).subscribe(function (response) {
            _this.tracksStatistic = response;
        }, function (error) {
            console.log('Ошибка обращения к сервису!');
            console.log(error);
        }, function () {
            _this.showSuccess();
        });
    };
    ScoutComponent.prototype.fuel = function () {
        var _this = this;
        console.log('Получение топлива');
        this.scoutService.getFuel(this.selectedUnit, this.startDate, this.endDate, this.daylyFuelChecked).subscribe(function (fuelStatistic) {
            _this.fuelStatistic = fuelStatistic;
        }, function (error) {
            console.log('факеншит!');
            console.log(error);
        }, function () {
            _this.showSuccess();
        });
    };
    ScoutComponent.prototype.odometer = function () {
        var _this = this;
        console.log('Статистика по одометру');
        this.scoutService.getOdometer(this.selectedUnit, this.startDate, this.endDate, this.daylyOdoChecked).subscribe(function (odoStatistic) {
            _this.odoStatistic = odoStatistic;
        }, function (error) { return console.log(error); }), function () {
            _this.showSuccess();
        };
    };
    ScoutComponent.prototype.handleFuelDaylyChange = function (e) {
        this.fuel();
    };
    ScoutComponent.prototype.handleOdoDaylyChange = function (e) {
        this.odometer();
    };
    ScoutComponent.prototype.selectorChanged = function (selector) {
        console.log('Готово!');
        this.startDate = selector.startDate;
        this.endDate = selector.endDate;
        this.selectedUnit = selector.selectedUnit;
    };
    //TODO: make shared super component
    ScoutComponent.prototype.showWarn = function (serviceMessage) {
        this.messageService.add({ severity: 'warn', summary: 'Ошибка получения данных', detail: 'Данные ещё не обработаны: ' +
                serviceMessage });
    };
    ScoutComponent.prototype.showSuccess = function () {
        this.messageService.add({ severity: 'success', summary: 'Получение', detail: 'Данные получены!' });
    };
    ScoutComponent.prototype.showError = function (serviceMessage) {
        this.messageService.add({ severity: 'error', summary: 'Ошибка', detail: 'Ошибка обращения к сервису: ' +
                serviceMessage });
    };
    ScoutComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-scout',
            template: __webpack_require__(/*! ./scout.component.html */ "./src/app/pages/scout/scout.component.html"),
            providers: [primeng_api__WEBPACK_IMPORTED_MODULE_5__["MessageService"]],
            styles: [__webpack_require__(/*! ./scout.component.css */ "./src/app/pages/scout/scout.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_scout_service__WEBPACK_IMPORTED_MODULE_2__["ScoutService"], _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__["DomSanitizer"], primeng_api__WEBPACK_IMPORTED_MODULE_5__["MessageService"]])
    ], ScoutComponent);
    return ScoutComponent;
}());



/***/ }),

/***/ "./src/app/pages/scout/unit-picker.component.css":
/*!*******************************************************!*\
  !*** ./src/app/pages/scout/unit-picker.component.css ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3Njb3V0L3VuaXQtcGlja2VyLmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/pages/scout/unit-picker.component.html":
/*!********************************************************!*\
  !*** ./src/app/pages/scout/unit-picker.component.html ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"form-group container\">\r\n  <div class=\"row\">\r\n    <label class=\"control-label col-sm-2\">Ид бензовоза: </label>\r\n    <p-dropdown\r\n      [(ngModel)]=\"statusSelect.selectedUnit\"\r\n      [options]=\"units\"\r\n      [style]=\"{'width':100}\"\r\n      class=\"col-sm-2\"\r\n      editable=\"true\"\r\n      name=\"Юнит\"\r\n      optionLabel=\"id\"\r\n      required=\"true\"\r\n      (ngModelChange)=\"selectorUnitChanged()\">\r\n    </p-dropdown>\r\n    <div class=\"col-sm-10\"></div>\r\n  </div>\r\n\r\n  <div class=\"row\">\r\n    <label class=\"control-label col-sm-2\">Период от</label>\r\n    <div class=\"col-sm-2\">\r\n      <p-calendar [(ngModel)]=\"statusSelect.startDate\"\r\n                  (ngModelChange)=\"selectorUnitChanged()\"\r\n                  required=\"true\" dateFormat=\"dd.mm.yy\"></p-calendar>\r\n    </div>\r\n  </div>\r\n\r\n  <div class=\"row\">\r\n    <label class=\"control-label col-sm-2\">Период по</label>\r\n    <div class=\"col-sm-2\">\r\n      <p-calendar [(ngModel)]=\"statusSelect.endDate\"\r\n                  (ngModelChange)=\"selectorUnitChanged()\"\r\n                  required=\"true\"\r\n                  dateFormat=\"dd.mm.yy\"></p-calendar>\r\n    </div>\r\n  </div>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/pages/scout/unit-picker.component.ts":
/*!******************************************************!*\
  !*** ./src/app/pages/scout/unit-picker.component.ts ***!
  \******************************************************/
/*! exports provided: UnitPickerComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UnitPickerComponent", function() { return UnitPickerComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _model_unit_pick_model__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../model/unit-pick.model */ "./src/app/model/unit-pick.model.ts");



var UnitPickerComponent = /** @class */ (function () {
    // constructor (private scoutService: ScoutService) { }
    function UnitPickerComponent() {
        this.selChanged = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.statusSelect = new _model_unit_pick_model__WEBPACK_IMPORTED_MODULE_2__["UnitPick"]();
        this.units = [{ id: 51005 }, { id: 51006 }, { id: 51007 }, { id: 51008 }, { id: 51010 }, { id: 51011 }, { id: 51164 }];
    }
    UnitPickerComponent.prototype.ngOnInit = function () {
    };
    UnitPickerComponent.prototype.selectorUnitChanged = function () {
        console.log('ВЖУХ!');
        this.selChanged.emit(this.statusSelect);
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"])
    ], UnitPickerComponent.prototype, "selChanged", void 0);
    UnitPickerComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-unit-picker',
            template: __webpack_require__(/*! ./unit-picker.component.html */ "./src/app/pages/scout/unit-picker.component.html"),
            styles: [__webpack_require__(/*! ./unit-picker.component.css */ "./src/app/pages/scout/unit-picker.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], UnitPickerComponent);
    return UnitPickerComponent;
}());



/***/ }),

/***/ "./src/app/pages/wiki/model/microservice.model.ts":
/*!********************************************************!*\
  !*** ./src/app/pages/wiki/model/microservice.model.ts ***!
  \********************************************************/
/*! exports provided: Microservice */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Microservice", function() { return Microservice; });
var Microservice = /** @class */ (function () {
    function Microservice() {
    }
    return Microservice;
}());



/***/ }),

/***/ "./src/app/pages/wiki/wiki-crt/wiki.crt.component.css":
/*!************************************************************!*\
  !*** ./src/app/pages/wiki/wiki-crt/wiki.crt.component.css ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "li {\r\n    color: #0079c1;\r\n}\r\n\r\n\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvd2lraS93aWtpLWNydC93aWtpLmNydC5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0lBQ0ksY0FBYztBQUNsQiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3dpa2kvd2lraS1jcnQvd2lraS5jcnQuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbImxpIHtcclxuICAgIGNvbG9yOiAjMDA3OWMxO1xyXG59XHJcblxyXG5cclxuIl19 */"

/***/ }),

/***/ "./src/app/pages/wiki/wiki-crt/wiki.crt.component.html":
/*!*************************************************************!*\
  !*** ./src/app/pages/wiki/wiki-crt/wiki.crt.component.html ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h1>Руководство по выживанию</h1>\r\n\r\n<br>\r\n\r\n<h6>Как мы работаем с самовыпущенными сертификатами</h6>\r\n\r\n<!--<p>-->\r\n    <!--Создаем пару ключ + сертификат:-->\r\n    <!--<app-cp [(inputText)]=\"generateKeyPairCommand\"></app-cp>-->\r\n\r\n<!--</p>-->\r\n\r\n<ol>\r\n    <li><b>Создаем корневой ключ</b>></li>\r\n    <app-cp [(inputText)]=\"generateCaKey\"></app-cp>\r\n    <li><b>Создаем корневой сертификат</b>>с его помощью будем подписывать наши самоподписанные сертификаты</li>\r\n    <app-cp [(inputText)]=\"generateCaCert\"></app-cp>\r\n    <h6>Country Name (2 letter code) [RU]:</h6>\r\n    <h6>State or Province Name (full name) [SPb]: </h6>\r\n    <h6>Locality Name (eg, city) [SPb]:</h6>\r\n    <h6>Organization Name (eg, company) [GPN]:</h6>\r\n    <h6>Organizational Unit Name (eg, section) [GPN-AT]:</h6>\r\n    <h6>Common Name (e.g. server FQDN or YOUR name) [*]:</h6>\r\n    <h6>Email Address [esb-drcp@gazprom-neft.ru]:</h6>\r\n    <li><b>Генерируем ключ нашего приложения</b>> Нужно указать имя файла ключа (amq.key)</li>\r\n    <app-cp [(inputText)]=\"generateAppKey\"></app-cp>\r\n    <li><b>Создаем запрос на подписание открытого ключа (сертификата)</b> Важно правильно указать правильное доменное имя сервиса!</li>\r\n    <app-cp [(inputText)]=\"signRequest\"></app-cp>\r\n    <li><b>Подписываем наш запрос при помощи самосгенеренного CA</b></li>\r\n    <app-cp [(inputText)]=\"signCommand\"></app-cp>\r\n    <li><b>Создаем хранилище сертификатов для приложения</b> Оно должно содержать в себе закрытый ключ, чтобы шифровать исхощий трафик</li>\r\n    <app-cp [(inputText)]=\"generateP12keystore\"></app-cp>\r\n    <li><b>Переделываем p12 хранилище в JKS</b> В хранилище пока будет только ключ, который нужно хранить в секрете. Не забываем пароли от хранилищ.</li>\r\n    <app-cp [(inputText)]=\"importP12\"></app-cp>\r\n    <li><b>Добавляем открытые сертификаты в хранилище</b></li>\r\n    <app-cp [(inputText)]=\"importCaCert\"></app-cp>\r\n    <app-cp [(inputText)]=\"importAppCert\"></app-cp>\r\n    <li><b>Создаем секрет с хранилищем</b></li>\r\n    <app-cp [(inputText)]=\"generateAppSecret\"></app-cp>\r\n    <li><b>Создаем доверенное хранилище сертификатов для клиентских приложений</b></li>\r\n    <app-cp [(inputText)]=\"clientCertAdd\"></app-cp>\r\n    <app-cp [(inputText)]=\"clientCertAdd2\"></app-cp>\r\n    <li><b>Создаем секрет для доверенного хранилища</b> Не забываем переключиться на проект/кластер клиента</li>\r\n    <app-cp [(inputText)]=\"generateClientSecret\"></app-cp>\r\n    <li><b>Маунтим секреты в приложениях</b> Деплой конфиги должны содержать следующие разделы:</li>\r\n\r\n    <pre>\r\n       <code class=\"language-groovy\" pCode>\r\n      volumeMounts:\r\n        - mountPath: /var/run/secrets/amq/serverkeystores\r\n          name: amq-server\r\n\r\n      volumes:\r\n        - name: amq-server\r\n          secret:\r\n            defaultMode: 420\r\n            secretName: amq-server-secret\r\n\r\n        </code>\r\n    </pre>\r\n    <li><b>Настраиваем TLS termination</b> Редактируем роутер приложений. Если приложение умеет работать с сертификатами (например, наш AMQ) ставим passthrough</li>\r\n    <h6>Если не умеет - выбираем re-encrypt, а в Destination CA подсовываем наш rootCA.crt</h6>\r\n\r\n\r\n</ol>"

/***/ }),

/***/ "./src/app/pages/wiki/wiki-crt/wiki.crt.component.ts":
/*!***********************************************************!*\
  !*** ./src/app/pages/wiki/wiki-crt/wiki.crt.component.ts ***!
  \***********************************************************/
/*! exports provided: WikiCrtComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WikiCrtComponent", function() { return WikiCrtComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var primeng_api__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! primeng/api */ "./node_modules/primeng/api.js");
/* harmony import */ var primeng_api__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(primeng_api__WEBPACK_IMPORTED_MODULE_2__);



var WikiCrtComponent = /** @class */ (function () {
    function WikiCrtComponent() {
        this.generateKeyPairCommand = 'keytool -genkeypair -keyalg RSA -alias "mcrest" -dname "CN=fuse-mc-rest.mobilcard.svc, OU=Architect, O=EmDev, L=SPB, C=RU" -ext SAN=dns:fuse-mc-rest-mobilcard.kspd-apps.demo.rht.ru,dns:kspd-apps.demo.rht.ru -keystore "mcks.jks" -storepass "changeit" -keypass "changeit" ';
        this.generateCaKey = 'openssl genrsa -out rootCA.key 2048';
        this.generateCaCert = 'openssl req -x509 -new -key rootCA.key -days 10000 -out rootCA.crt';
        this.generateAppKey = 'openssl genrsa -out amq.key 2048';
        this.signRequest = 'openssl req -new -key amq.key -out amq.csr';
        this.signCommand = 'openssl x509 -req -in amq.csr -CA rootCA.crt -CAkey rootCA.key -CAcreateserial -out amq.crt -days 5000';
        this.generateP12keystore = 'openssl pkcs12 -export -in aqm.crt -inkey amq.key -name "amqkey" -out keystore.p12';
        this.importP12 = 'keytool -importkeystore -srckeystore keystore.p12 -srcstoretype pkcs12 -destkeystore keystore.jks deststoretype JKS';
        this.importCaCert = 'keytool -import -alias "root" -keystore "keystore.jks" -file "rootCA.crt" -storepass "changeit" -trustcacerts -noprompt';
        this.importAppCert = 'keytool -import -alias "amq" -keystore "keystore.jks" -file "amq.crt" -storepass "changeit" -trustcacerts -noprompt';
        this.generateAppSecret = 'oc create secret generic amq-server-jks --from-file="/tmp/aaa/keystore.jks"';
        this.clientCertAdd = 'keytool -import -alias "root" -keystore "client.ts" -file "rootCA.crt" -storepass "changeit" -trustcacerts -noprompt';
        this.clientCertAdd2 = 'keytool -import -alias "amq" -keystore "client.ts" -file "amq.crt" -storepass "changeit" -trustcacerts -noprompt';
        this.generateClientSecret = 'oc create secret generic amq-client --from-file="/tmp/aaa/client.ts"';
        this.mounting = 'volumeMounts:\n' +
            '            - mountPath: /var/run/secrets/amq/serverkeystores\n' +
            '              name: amq-server\n' +
            '\n' +
            '      volumes:\n' +
            '        - name: amq-server\n' +
            '          secret:\n' +
            '            defaultMode: 420\n' +
            '            secretName: amq-server-secret\n';
    }
    WikiCrtComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-wiki-crt',
            template: __webpack_require__(/*! ./wiki.crt.component.html */ "./src/app/pages/wiki/wiki-crt/wiki.crt.component.html"),
            providers: [primeng_api__WEBPACK_IMPORTED_MODULE_2__["MessageService"]],
            styles: [__webpack_require__(/*! ./wiki.crt.component.css */ "./src/app/pages/wiki/wiki-crt/wiki.crt.component.css")]
        })
    ], WikiCrtComponent);
    return WikiCrtComponent;
}());



/***/ }),

/***/ "./src/app/pages/wiki/wiki-esb/wiki-esb.component.css":
/*!************************************************************!*\
  !*** ./src/app/pages/wiki/wiki-esb/wiki-esb.component.css ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3dpa2kvd2lraS1lc2Ivd2lraS1lc2IuY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/pages/wiki/wiki-esb/wiki-esb.component.html":
/*!*************************************************************!*\
  !*** ./src/app/pages/wiki/wiki-esb/wiki-esb.component.html ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p-fieldset legend=\"Что это\" [toggleable]=\"true\">\r\n\r\n  <h6>Enterprise Service Bus (сервисная шина данных) - связующее программное обеспечение, обеспечивающее централизованный и унифицированный событийно-ориентированный обмен сообщениями между различными информационными системами на принципах сервис-ориентированной архитектуры. </h6>\r\n\r\n</p-fieldset>\r\n\r\n\r\n<p-fieldset legend=\"Миссия\" [toggleable]=\"true\">\r\n\r\n  <h2>Основные задачи, решаемые шиной</h2>\r\n  <ul style=\"list-style-type:square;\">\r\n    <li>поддержка синхронного и асинхронного способа вызова служб;</li>\r\n    <li>использование защищённого транспорта, с гарантированной доставкой сообщений, поддерживающего транзакционную модель;</li>\r\n    <li>статическая и алгоритмическая маршрутизация сообщений;</li>\r\n    <li>оступ к данным из сторонних информационных систем с помощью готовых или специально разработанных адаптеров;</li>\r\n    <li>обработка и преобразование сообщений;</li>\r\n    <li>оркестровка и хореография служб</li>\r\n    <li>разнообразные механизмы контроля и управления (аудиты, протоколирование).</li>\r\n  </ul>\r\n</p-fieldset>\r\n\r\n<p-fieldset legend=\"Команда\" [toggleable]=\"true\">\r\n  <ul style=\"list-style-type:square;\">\r\n    <li><b>Гриненко Илья</b> - руководитель проекта</li>\r\n    <li><b>Соколов Александр</b> - ведущий разработчик</li>\r\n    <li><b>Телицын Сергей</b> -  Java разработчик</li>\r\n    <li><b>Шерин Анатолий</b> -  бизнес аналитик</li>\r\n    <li><b>Удалов Виталий</b> -  архитектор</li>\r\n    <li><b>Бугаев Александр</b> - специалист службы ИБ </li>\r\n    <li><b>Малахова Дарья</b> - инженер по автоматизированному тестированию</li>\r\n    <li><b>Семенов Сергей</b> - администратор Openshift</li>\r\n\r\n  </ul>\r\n\r\n\r\n</p-fieldset>"

/***/ }),

/***/ "./src/app/pages/wiki/wiki-esb/wiki-esb.component.ts":
/*!***********************************************************!*\
  !*** ./src/app/pages/wiki/wiki-esb/wiki-esb.component.ts ***!
  \***********************************************************/
/*! exports provided: WikiEsbComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WikiEsbComponent", function() { return WikiEsbComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");



var WikiEsbComponent = /** @class */ (function () {
    function WikiEsbComponent(route, router) {
        this.route = route;
        this.router = router;
    }
    WikiEsbComponent.prototype.ngOnInit = function () {
    };
    WikiEsbComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-wiki-esb',
            template: __webpack_require__(/*! ./wiki-esb.component.html */ "./src/app/pages/wiki/wiki-esb/wiki-esb.component.html"),
            styles: [__webpack_require__(/*! ./wiki-esb.component.css */ "./src/app/pages/wiki/wiki-esb/wiki-esb.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
    ], WikiEsbComponent);
    return WikiEsbComponent;
}());



/***/ }),

/***/ "./src/app/pages/wiki/wiki-infrastructure/wiki-infrastructure.component.css":
/*!**********************************************************************************!*\
  !*** ./src/app/pages/wiki/wiki-infrastructure/wiki-infrastructure.component.css ***!
  \**********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3dpa2kvd2lraS1pbmZyYXN0cnVjdHVyZS93aWtpLWluZnJhc3RydWN0dXJlLmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/pages/wiki/wiki-infrastructure/wiki-infrastructure.component.html":
/*!***********************************************************************************!*\
  !*** ./src/app/pages/wiki/wiki-infrastructure/wiki-infrastructure.component.html ***!
  \***********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n<h3 class=\"first\">Список серверов ESB</h3>\n<p-table #dt [columns]=\"cols\" [value]=\"servers\">\n  <ng-template pTemplate=\"header\" let-columns>\n    <tr>\n      <th *ngFor=\"let col of columns\" [pSortableColumn]=\"col.field\">\n        {{col.header}}\n        <p-sortIcon [field]=\"col.field\" ariaLabel=\"Activate to sort\" ariaLabelDesc=\"Activate to sort in descending order\" ariaLabelAsc=\"Activate to sort in ascending order\"></p-sortIcon>\n      </th>\n    </tr>\n    <tr>\n      <th *ngFor=\"let col of columns\" [ngSwitch]=\"col.field\">\n        <input *ngSwitchCase=\"'id'\" pInputText type=\"text\" (input)=\"dt.filter($event.target.value, col.field, 'equals')\">\n        <input *ngSwitchCase=\"'name'\" pInputText type=\"text\" (input)=\"dt.filter($event.target.value, col.field, 'contains')\">\n        <input *ngSwitchCase=\"'address'\" pInputText type=\"text\" (input)=\"dt.filter($event.target.value, col.field, 'contains')\">\n        <input *ngSwitchCase=\"'description'\" pInputText type=\"text\" (input)=\"dt.filter($event.target.value, col.field, 'contains')\">\n        <p-multiSelect *ngSwitchCase=\"'stage'\" [options]=\"stages\" defaultLabel=\"Все\"  (onChange)=\"dt.filter($event.value, col.field, 'in')\"></p-multiSelect>\n      </th>\n    </tr>\n  </ng-template>\n  <ng-template pTemplate=\"body\" let-rowData let-columns=\"columns\">\n    <tr>\n      <td *ngFor=\"let col of columns\">\n        {{rowData[col.field]}}\n      </td>\n    </tr>\n  </ng-template>\n</p-table>"

/***/ }),

/***/ "./src/app/pages/wiki/wiki-infrastructure/wiki-infrastructure.component.ts":
/*!*********************************************************************************!*\
  !*** ./src/app/pages/wiki/wiki-infrastructure/wiki-infrastructure.component.ts ***!
  \*********************************************************************************/
/*! exports provided: WikiInfrastructureComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WikiInfrastructureComponent", function() { return WikiInfrastructureComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var WikiInfrastructureComponent = /** @class */ (function () {
    function WikiInfrastructureComponent() {
    }
    WikiInfrastructureComponent.prototype.ngOnInit = function () {
        this.cols = [
            { field: 'id', header: 'ИД' },
            { field: 'name', header: 'Имя' },
            { field: 'address', header: 'Адрес' },
            { field: 'description', header: 'Описание' },
            { field: 'stage', header: 'Фильтр' }
        ];
        this.stages = [
            { label: 'КСПД', value: 'TEST-KSPD' },
            { label: 'ДМЗ', value: 'TEST-DMZ' },
        ];
        this.servers = [
            { id: 1, name: 'SPB26-ESB-M1D', address: '10.44.211.133', description: 'Мастер узел', stage: 'TEST-KSPD' },
            { id: 2, name: 'SPB26-ESB-M2D', address: '10.44.211.134', description: 'Мастер узел', stage: 'TEST-KSPD' },
            { id: 3, name: 'SPB26-ESB-M3D', address: '10.44.211.146', description: 'Мастер узел', stage: 'TEST-KSPD' },
            { id: 4, name: 'SPB26-ESDMZ-M1D', address: '10.44.211.170', description: 'Мастер узел', stage: 'TEST-DMZ' },
            { id: 5, name: 'SPB26-ESDMZ-M2D', address: '10.44.211.171', description: 'Мастер узел', stage: 'TEST-DMZ' },
            { id: 6, name: 'SPB26-ESDMZ-M3D', address: '10.44.211.174', description: 'Мастер узел', stage: 'TEST-DMZ' },
            { id: 7, name: 'SPB26-ESDMZ-W1D', address: '10.44.211.162', description: 'Рабочий узел', stage: 'TEST-DMZ' },
            { id: 8, name: 'SPB26-ESDMZ-W2D', address: '10.44.211.163', description: 'Рабочий узел', stage: 'TEST-DMZ' },
            { id: 9, name: 'SPB26-ESDMZ-W3D', address: '10.44.211.164', description: 'Рабочий узел', stage: 'TEST-DMZ' },
            { id: 10, name: 'SPB26-ESDMZ-W4D', address: '10.44.211.165', description: 'Рабочий узел', stage: 'TEST-DMZ' },
            { id: 11, name: 'SPB26-ESDMZ-W5D', address: '10.44.211.166', description: 'Рабочий узел', stage: 'TEST-DMZ' },
            { id: 12, name: 'SPB26-ESB-W1D', address: '10.44.211.135', description: 'Рабочий узел', stage: 'TEST-KSPD' },
            { id: 13, name: 'SPB26-ESB-W2D', address: '10.44.211.136', description: 'Рабочий узел', stage: 'TEST-KSPD' },
            { id: 14, name: 'SPB26-ESB-W3D', address: '10.44.211.137', description: 'Рабочий узел', stage: 'TEST-KSPD' },
            { id: 15, name: 'SPB26-ESB-W4D', address: '10.44.211.138', description: 'Рабочий узел', stage: 'TEST-KSPD' },
            { id: 16, name: 'SPB26-ESB-W5D', address: '10.44.211.139', description: 'Рабочий узел', stage: 'TEST-KSPD' },
            { id: 17, name: 'SPB26-ESB-W6D', address: '10.44.211.140', description: 'Рабочий узел', stage: 'TEST-KSPD' },
            { id: 18, name: 'SPB26-ESB-W7D', address: '10.44.211.141', description: 'Рабочий узел', stage: 'TEST-KSPD' },
            { id: 19, name: 'SPB26-ESB-W8D', address: '10.44.211.142', description: 'Рабочий узел', stage: 'TEST-KSPD' },
            { id: 20, name: 'SPB26-ESB-I1D', address: '10.44.211.130', description: 'Инфраструктурный узел', stage: 'TEST-KSPD' },
            { id: 21, name: 'SPB26-ESB-I2D', address: '10.44.211.131', description: 'Инфраструктурный узел', stage: 'TEST-KSPD' },
            { id: 22, name: 'SPB26-ESB-I3D', address: '10.44.211.132', description: 'Инфраструктурный узел', stage: 'TEST-KSPD' },
            { id: 23, name: 'SPB26-ESDMZ-I1D', address: '10.44.211.167', description: 'Инфраструктурный узел', stage: 'TEST-DMZ' },
            { id: 24, name: 'SPB26-ESDMZ-I2D', address: '10.44.211.168', description: 'Инфраструктурный узел', stage: 'TEST-DMZ' },
            { id: 25, name: 'SPB26-ESDMZ-I3D', address: '10.44.211.169', description: 'Инфраструктурный узел', stage: 'TEST-DMZ' },
            { id: 26, name: 'SPB26-ESDMZFS1D', address: '10.44.211.172', description: 'Файловый сервер', stage: 'TEST-DMZ' },
            { id: 27, name: 'SPB26-ESB-FS1D', address: '10.44.211.143', description: 'Файловый сервер', stage: 'TEST-KSPD' },
        ];
    };
    WikiInfrastructureComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-wiki-infrastructure',
            template: __webpack_require__(/*! ./wiki-infrastructure.component.html */ "./src/app/pages/wiki/wiki-infrastructure/wiki-infrastructure.component.html"),
            styles: [__webpack_require__(/*! ./wiki-infrastructure.component.css */ "./src/app/pages/wiki/wiki-infrastructure/wiki-infrastructure.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], WikiInfrastructureComponent);
    return WikiInfrastructureComponent;
}());



/***/ }),

/***/ "./src/app/pages/wiki/wiki-metrics/wiki-metrics.component.css":
/*!********************************************************************!*\
  !*** ./src/app/pages/wiki/wiki-metrics/wiki-metrics.component.css ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3dpa2kvd2lraS1tZXRyaWNzL3dpa2ktbWV0cmljcy5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/pages/wiki/wiki-metrics/wiki-metrics.component.html":
/*!*********************************************************************!*\
  !*** ./src/app/pages/wiki/wiki-metrics/wiki-metrics.component.html ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p>\r\n  <b>Метрики команды ESB</b>\r\n</p>\r\n\r\n\r\n<table class=\"table table-hover\">\r\n  <thead>\r\n  <tr>\r\n    <th>Метрика</th>\r\n    <th>Описание</th>\r\n    <th pTooltip=\"Business value - полезность для бизнеса\" tooltipPosition=\"left\">BV</th>\r\n    <th pTooltip=\"Technical value - полезность для команды разработки\" tooltipPosition=\"left\">TV</th>\r\n  </tr>\r\n  </thead>\r\n  <tbody>\r\n  <tr>\r\n    <td>Синхронные/асинхронные</td>\r\n    <td>Соотношение синхронных и асинхронных взаимодействия</td>\r\n    <td>5</td>\r\n    <td>5</td>\r\n  </tr>\r\n  <tr>\r\n    <td>Сервисов на точку интеграции</td>\r\n    <td>Показывает сколько раз переиспользуется один и тот же сервис разными системами</td>\r\n    <td>5</td>\r\n    <td>8</td>\r\n  </tr>\r\n  <tr>\r\n    <td>Количество точек интеграции</td>\r\n    <td>Общее количество сервисов</td>\r\n    <td>10</td>\r\n    <td>10</td>\r\n  </tr>\r\n  <tr>\r\n    <td>Время обработки запроса</td>\r\n    <td>Среднее время обработки одного запроса для каждого сервиса</td>\r\n    <td>8</td>\r\n    <td>5</td>\r\n  </tr>\r\n  <tr>\r\n    <td>Сложность</td>\r\n    <td>Коэффициент учитывающий сложность взаимодействия</td>\r\n    <td>2</td>\r\n    <td>10</td>\r\n  </tr>\r\n  <tr>\r\n    <td>Проверки жизнеспособности</td>\r\n    <td>Количество правил проверок работоспособности и доступности сервисов</td>\r\n    <td>8</td>\r\n    <td>8</td>\r\n  </tr>\r\n  <tr>\r\n    <td>Количество сокращенных серверов в DMZ сегменте</td>\r\n    <td>Количество серверов, замещенных механизмами шины</td>\r\n    <td>10</td>\r\n    <td>10</td>\r\n  </tr>\r\n\r\n  </tbody>\r\n</table>"

/***/ }),

/***/ "./src/app/pages/wiki/wiki-metrics/wiki-metrics.component.ts":
/*!*******************************************************************!*\
  !*** ./src/app/pages/wiki/wiki-metrics/wiki-metrics.component.ts ***!
  \*******************************************************************/
/*! exports provided: WikiMetricsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WikiMetricsComponent", function() { return WikiMetricsComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var WikiMetricsComponent = /** @class */ (function () {
    function WikiMetricsComponent() {
    }
    WikiMetricsComponent.prototype.ngOnInit = function () {
    };
    WikiMetricsComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-wiki-metrics',
            template: __webpack_require__(/*! ./wiki-metrics.component.html */ "./src/app/pages/wiki/wiki-metrics/wiki-metrics.component.html"),
            styles: [__webpack_require__(/*! ./wiki-metrics.component.css */ "./src/app/pages/wiki/wiki-metrics/wiki-metrics.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], WikiMetricsComponent);
    return WikiMetricsComponent;
}());



/***/ }),

/***/ "./src/app/pages/wiki/wiki-microservices/wiki-microservices.component.css":
/*!********************************************************************************!*\
  !*** ./src/app/pages/wiki/wiki-microservices/wiki-microservices.component.css ***!
  \********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3dpa2kvd2lraS1taWNyb3NlcnZpY2VzL3dpa2ktbWljcm9zZXJ2aWNlcy5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/pages/wiki/wiki-microservices/wiki-microservices.component.html":
/*!*********************************************************************************!*\
  !*** ./src/app/pages/wiki/wiki-microservices/wiki-microservices.component.html ***!
  \*********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p>\n    Список микросервисов\n</p>\n<p-dataView [value]=\"microservices\">\n    <ng-template let-service pTemplate=\"listItem\">\n        <div class=\"ui-g\" style=\"padding: 2em;border-bottom: 1px solid #d9d9d9\">\n            <div class=\"ui-g-2 ui-md-2\" style=\"text-align:center; width: 7%\">\n                <img src=\"assets/images/openshift/{{service.type}}.svg\">\n            </div>\n            <div class=\"ui-g-4 ui-sm-8 ui-g-offset-1\">\n                <div class=\"row\">\n                    Наименование:<b>{{service.name}}</b>\n                </div>\n                <div class=\"row\">\n                    Описание: <b>{{service.description}}</b>\n                </div>\n                <div class=\"row\">\n                    Тип: <b>{{service.type}}</b>\n                </div>\n            </div>\n            <div class=\"ui-g-4 ui-sm-6\">\n                <p-table [responsive]=\"true\"\n                         [value]=\"service?.resources\"\n                         class=\"table table-striped table-dark\"\n                >\n                    <ng-template pTemplate=\"header\">\n                        <tr>\n                            <th scope=\"col-sm\">Среда</th>\n                            <th scope=\"col-sm\">Cores</th>\n                            <th scope=\"col-sm\">Memory</th>\n                            <th scope=\"col-sm\">PV</th>\n                            <th scope=\"col-sm\">Cost</th>\n                        </tr>\n                    </ng-template>\n                    <ng-template let-res pTemplate=\"body\">\n                        <tr>\n                            <td>{{res.stage}}</td>\n                            <td>{{res.cores}}</td>\n                            <td>{{res.memory}}</td>\n                            <td>{{res.pv}}</td>\n                            <td>1 руб.</td>\n                        </tr>\n                    </ng-template>\n                </p-table>\n            </div>\n            <div class=\"ui-g-2 ui-sm-6\">\n                <div>\n                    <p-button (onClick)=\"show(service)\"\n                              icon=\"pi pi-eye\"\n                              pTooltip=\"Просмотр\" tooltipPosition=\"left\"></p-button>\n                </div>\n\n                <div>\n                    <p-button (onClick)=\"add(service.id)\"\n                              icon=\"pi pi-plus\"\n                              pTooltip=\"Добавить\" tooltipPosition=\"left\"></p-button>\n                </div>\n                <div>\n                    <p-button (onClick)=\"edit(service)\"\n                              icon=\"pi pi-pencil\"\n                              pTooltip=\"Редактировать\" tooltipPosition=\"left\"></p-button>\n                </div>\n                <div>\n                    <p-button (onClick)=\"share(service.id)\"\n                              icon=\"pi pi-share-alt\"\n                              pTooltip=\"Привязать к системе\" tooltipPosition=\"left\"></p-button>\n                </div>\n                <div>\n                    <p-button (onClick)=\"delete(service.id)\"\n                              icon=\"pi pi-times\"\n                              pTooltip=\"Удалить\" tooltipPosition=\"left\"></p-button>\n                </div>\n            </div>\n        </div>\n    </ng-template>\n</p-dataView>\n\n<p-dialog [(visible)]=\"showEditorForm\"\n          [width]=\"700\"\n          header=\"Микросервис\"\n          positionLeft=\"100\"\n          positionTop=\"50\"\n>\n    <table *ngIf=\"selectedService\" class=\"table\">\n        <tbody>\n        <tr >\n            <td>ID</td>\n            <td>{{selectedService.id}}</td>\n        </tr>\n        <tr>\n            <td>Наименование</td>\n            <td>\n                <input type=\"text\" size=\"50\" pInputText [(ngModel)]=\"selectedService.name\" [disabled]=\"blockEditorForm\"/>\n            </td>\n        </tr>\n        <tr>\n            <td>Описание</td>\n            <textarea pInputTextarea\n                      [(ngModel)]=\"selectedService.description\"\n                      [disabled]=\"blockEditorForm\"\n                      [rows]=\"3\" [cols]=\"50\">\n            </textarea>\n        </tr>\n        <tr>\n            <td>Тип</td>\n            <td>\n                <input type=\"text\" size=\"50\" pInputText [(ngModel)]=\"selectedService.type\" [disabled]=\"blockEditorForm\"/>\n            </td>\n        </tr>\n        <tr>\n            <td>Среда</td>\n            <td>\n                <input type=\"text\" size=\"50\" pInputText [(ngModel)]=\"selectedService.stage\" [disabled]=\"blockEditorForm\"/>\n            </td>\n        </tr>\n        <tr>\n            <td>Статус</td>\n            <td>\n                <input type=\"text\" size=\"50\" pInputText [(ngModel)]=\"selectedService.status\" [disabled]=\"blockEditorForm\"/>\n            </td>\n        </tr>\n        </tbody>\n    </table>\n\n    <table *ngIf=\"selectedService?.resources\" class=\"table\">\n    <thead>\n        <tr>\n            <td>Cores</td>\n            <td>Memory</td>\n            <td>PV</td>\n            <td>Cost</td>\n        </tr>\n    </thead>\n        <tbody *ngFor=\"let res of selectedService?.resources\">\n        <tr>\n            <td>{{res.cores}}</td>\n            <td>{{res.memory}}</td>\n            <td>{{res.pv}}</td>\n            <td></td>\n        </tr>\n        </tbody>\n    </table>\n\n    <button class=\"btn btn-outline-success ml-auto\" type=\"button\" (click)=\"saveSelected()\">Сохранить</button>\n</p-dialog>"

/***/ }),

/***/ "./src/app/pages/wiki/wiki-microservices/wiki-microservices.component.ts":
/*!*******************************************************************************!*\
  !*** ./src/app/pages/wiki/wiki-microservices/wiki-microservices.component.ts ***!
  \*******************************************************************************/
/*! exports provided: WikiMicroservicesComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WikiMicroservicesComponent", function() { return WikiMicroservicesComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _model_microservice_model__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../model/microservice.model */ "./src/app/pages/wiki/model/microservice.model.ts");



var WikiMicroservicesComponent = /** @class */ (function () {
    function WikiMicroservicesComponent() {
        this.showEditorForm = false;
        this.blockEditorForm = false;
    }
    WikiMicroservicesComponent.prototype.ngOnInit = function () {
        this.microservices = [
            { id: 1, name: "fuse-mc", description: "Прокси сервис маршрутизации из ESB в Мобильную карту", stage: 2,
                status: "В разработке", type: "fuse", urls: [{ id: 1, stage: "TEST", url: "http://www.fuse.spb.ru", port: 8080 },
                    { id: 1, stage: "PROD", url: "http://www.fuse.kspd.ru", port: 80 }],
                resources: [{ id: 1, stage: "TEST", cores: 2, memory: 4, pv: 1 },
                    { id: 2, stage: "PROD", cores: 4, memory: 8, pv: 1 }
                ] },
            { id: 2, name: "eap-mc", description: "Rest сервис для мобильной карты", stage: 2,
                status: "В разработке", type: "eap", urls: [{ id: 1, stage: "TEST", url: "http://www.eap.spb.ru", port: 8080 },
                    { id: 1, stage: "PROD", url: "http://www.eap.kspd.ru", port: 80 }],
                resources: [{ id: 1, stage: "TEST", cores: 2, memory: 4, pv: 1 },
                    { id: 2, stage: "PROD", cores: 2, memory: 4, pv: 1 }
                ] }
        ];
        this.items = [
            { label: 'Бэклог' },
            { label: 'Аналитика' },
            { label: 'В разработке' },
            { label: 'Тестирование' },
            { label: 'Разработана' },
            { label: 'Релиз' },
        ];
    };
    WikiMicroservicesComponent.prototype.show = function (service) {
        this.selectedService = service;
        console.log('Show id: ' + service.id);
        this.blockEditorForm = true;
        this.showEditorForm = true;
    };
    WikiMicroservicesComponent.prototype.add = function () {
        console.log('add new element');
        this.blockEditorForm = false;
        this.showEditorForm = true;
        this.selectedService = new _model_microservice_model__WEBPACK_IMPORTED_MODULE_2__["Microservice"]();
    };
    WikiMicroservicesComponent.prototype.edit = function (service) {
        this.selectedService = service;
        console.log('Edit id: ' + service.id);
        this.blockEditorForm = false;
        this.showEditorForm = true;
    };
    WikiMicroservicesComponent.prototype.saveSelected = function () {
    };
    WikiMicroservicesComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-wiki-microservices',
            template: __webpack_require__(/*! ./wiki-microservices.component.html */ "./src/app/pages/wiki/wiki-microservices/wiki-microservices.component.html"),
            styles: [__webpack_require__(/*! ./wiki-microservices.component.css */ "./src/app/pages/wiki/wiki-microservices/wiki-microservices.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], WikiMicroservicesComponent);
    return WikiMicroservicesComponent;
}());



/***/ }),

/***/ "./src/app/pages/wiki/wiki-requiments/wiki-requiments.component.css":
/*!**************************************************************************!*\
  !*** ./src/app/pages/wiki/wiki-requiments/wiki-requiments.component.css ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3dpa2kvd2lraS1yZXF1aW1lbnRzL3dpa2ktcmVxdWltZW50cy5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/pages/wiki/wiki-requiments/wiki-requiments.component.html":
/*!***************************************************************************!*\
  !*** ./src/app/pages/wiki/wiki-requiments/wiki-requiments.component.html ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h2>Требования к сторонним системам</h2>\n<div class=\"container-fluid\">\n<ul style=\"list-style-type:square;\">\n  <li><b>Схема взаимодействия </b> - Какие сегменты затрагивает интеграция, и куда идут информационные потоки (DMZ/КСПД) </li>\n  <li><b>Реквизиты серверов </b> - список IP/domain name :PORT для открытия доступа на межсетевых экранах </li>\n  <li><b>Тип взаимодействия </b> - SOAP/Rest/WebSocket/FTP, наличие описание WADL/WSDL/XSD/OPEN-API</li>\n  <li><b>Валидация </b> - Требования к валидации данных, в т.ч. бизнес-логики, жесткая привязка к схеме данных, версионность </li>\n  <li><b>Коды ошибок </b> - Описание как внешних кодов ошибок, так и внутренних возникающих на шине </li>\n  <li><b>Авторизация </b> -  доменная/локальная/по сертификату, храним ли служебные учетки в secrets</li>\n  <li><b>Мониторинг </b> - описаны требования к liveness/readyness проверкам </li>\n  <li><b>Паттерны EIP </b> - какие механизмы EIP нужны дополнительно (throttling, amq, deadletter, datagrid, transformer...) </li>\n  <li><b>Многопоточность </b> - нужна ли распределенная обработка в несколько потоков </li>\n  <li><b>Ресурсы </b> - Требования по необходимым ресурсам системы (ядра/память) и количество экземпляров </li>\n  <li><b>План релизов </b> - Предполагаемые сроки релизов для синхронизации шины и интегрируемых сервисов </li>\n  <li><b>Список изменений </b> - Примечания и изменения </li>\n</ul>\n\n\n\n  <h2>Правила формирования имен проектов и URL:</h2>\n\n  <p><b>Наименование проектов в Openshift’e: </b></p>\n\n  <ul style=\"list-style-type:square;\">\n    <li><b>Формат: [Организация или дирекция]-[Наименование системы]</b></li>\n    <li>  Где [Организация или дирекция]\n    <li>GPN – если система глобальная в ГПН. Пример: GPN-SAPPO</li>\n    <li>DRP – если система в ДРП. Пример: DRP-MobilCard\n    <li>ESB – если проект инфраструктурный. Пример: ESB-SSO</li>\n  </ul>\n\n  2)                  Наименование роутов:\n  Предложено два варианта, нужно выбрать какой лучше:\n  2.1) Формат: [Наименование системы источника]- [Наименование организации].apps.rhc.gazprom-neft.local/[сервис]/[протокол]/[версия]/\n  [Наименование организации] – аналогично пункту 1, кроме GPN. Если сервис глобальный на весь ГПН, то можно не указывать\n  Примеры:\n  mobilcard-drp.apps.rhc.gazprom-neft.local/reports/rest/v1/\n  mobilcard-drp.apps.rhc.gazprom-neft.local/edi/amq/v1/\n  etran-rzd.apps.rhc.gazprom-neft.local/invoice/rest/v1/\n  sappo.apps.rhc.gazprom-neft.local/monitoring-center/rest/v1/\n\n  2.2) Формат: esb.apps.rhc.gazprom-neft.local/[Наименование системы источника]- [Наименование организации]/[сервис]/[протокол]/[версия]/\n  Примеры\n  esb.apps.rhc.gazprom-neft.local/ mobilcard-drp/reports/rest/v1/\n  esb.apps.rhc.gazprom-neft.local/etran-rzd/invoice/rest/v1/\n  esb.apps.rhc.gazprom-neft.local/sappo/monitoring-center/rest/v1/\n\n  3)                  В description проекта в Openshift указывать:\n  •         Номер АЗ, по которой создавался\n  •         Контактное лицо / владелец системы\n\n\n\n\n</div>"

/***/ }),

/***/ "./src/app/pages/wiki/wiki-requiments/wiki-requiments.component.ts":
/*!*************************************************************************!*\
  !*** ./src/app/pages/wiki/wiki-requiments/wiki-requiments.component.ts ***!
  \*************************************************************************/
/*! exports provided: WikiRequimentsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WikiRequimentsComponent", function() { return WikiRequimentsComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var WikiRequimentsComponent = /** @class */ (function () {
    function WikiRequimentsComponent() {
    }
    WikiRequimentsComponent.prototype.ngOnInit = function () {
    };
    WikiRequimentsComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-wiki-requiments',
            template: __webpack_require__(/*! ./wiki-requiments.component.html */ "./src/app/pages/wiki/wiki-requiments/wiki-requiments.component.html"),
            styles: [__webpack_require__(/*! ./wiki-requiments.component.css */ "./src/app/pages/wiki/wiki-requiments/wiki-requiments.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], WikiRequimentsComponent);
    return WikiRequimentsComponent;
}());



/***/ }),

/***/ "./src/app/pages/wiki/wiki-routing.module.ts":
/*!***************************************************!*\
  !*** ./src/app/pages/wiki/wiki-routing.module.ts ***!
  \***************************************************/
/*! exports provided: WikiRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WikiRoutingModule", function() { return WikiRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _wiki_crt_wiki_crt_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./wiki-crt/wiki.crt.component */ "./src/app/pages/wiki/wiki-crt/wiki.crt.component.ts");
/* harmony import */ var _wiki_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./wiki.component */ "./src/app/pages/wiki/wiki.component.ts");
/* harmony import */ var _wiki_esb_wiki_esb_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./wiki-esb/wiki-esb.component */ "./src/app/pages/wiki/wiki-esb/wiki-esb.component.ts");
/* harmony import */ var _wiki_systems_wiki_systems_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./wiki-systems/wiki-systems.component */ "./src/app/pages/wiki/wiki-systems/wiki-systems.component.ts");
/* harmony import */ var _wiki_stack_wiki_stack_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./wiki-stack/wiki-stack.component */ "./src/app/pages/wiki/wiki-stack/wiki-stack.component.ts");
/* harmony import */ var _wiki_metrics_wiki_metrics_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./wiki-metrics/wiki-metrics.component */ "./src/app/pages/wiki/wiki-metrics/wiki-metrics.component.ts");
/* harmony import */ var _wiki_microservices_wiki_microservices_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./wiki-microservices/wiki-microservices.component */ "./src/app/pages/wiki/wiki-microservices/wiki-microservices.component.ts");
/* harmony import */ var _wiki_requiments_wiki_requiments_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./wiki-requiments/wiki-requiments.component */ "./src/app/pages/wiki/wiki-requiments/wiki-requiments.component.ts");
/* harmony import */ var _wiki_infrastructure_wiki_infrastructure_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./wiki-infrastructure/wiki-infrastructure.component */ "./src/app/pages/wiki/wiki-infrastructure/wiki-infrastructure.component.ts");












var wikiRoutes = [
    { path: '', redirectTo: 'wiki/esb', pathMatch: 'full' },
    {
        path: 'wiki', component: _wiki_component__WEBPACK_IMPORTED_MODULE_4__["WikiComponent"],
        children: [
            { path: 'esb', component: _wiki_esb_wiki_esb_component__WEBPACK_IMPORTED_MODULE_5__["WikiEsbComponent"] },
            { path: 'crt', component: _wiki_crt_wiki_crt_component__WEBPACK_IMPORTED_MODULE_3__["WikiCrtComponent"] },
            { path: 'infrastructure', component: _wiki_infrastructure_wiki_infrastructure_component__WEBPACK_IMPORTED_MODULE_11__["WikiInfrastructureComponent"] },
            { path: 'metrics', component: _wiki_metrics_wiki_metrics_component__WEBPACK_IMPORTED_MODULE_8__["WikiMetricsComponent"] },
            { path: 'microservices', component: _wiki_microservices_wiki_microservices_component__WEBPACK_IMPORTED_MODULE_9__["WikiMicroservicesComponent"] },
            { path: 'requiments', component: _wiki_requiments_wiki_requiments_component__WEBPACK_IMPORTED_MODULE_10__["WikiRequimentsComponent"] },
            { path: 'systems', component: _wiki_systems_wiki_systems_component__WEBPACK_IMPORTED_MODULE_6__["WikiSystemsComponent"] },
            { path: 'stack', component: _wiki_stack_wiki_stack_component__WEBPACK_IMPORTED_MODULE_7__["WikiStackComponent"] }
        ]
    }
];
var WikiRoutingModule = /** @class */ (function () {
    function WikiRoutingModule() {
    }
    WikiRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
            imports: [
                _angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(wikiRoutes)
            ],
            exports: [
                _angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]
            ]
        })
    ], WikiRoutingModule);
    return WikiRoutingModule;
}());



/***/ }),

/***/ "./src/app/pages/wiki/wiki-stack/wiki-stack.component.css":
/*!****************************************************************!*\
  !*** ./src/app/pages/wiki/wiki-stack/wiki-stack.component.css ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3dpa2kvd2lraS1zdGFjay93aWtpLXN0YWNrLmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/pages/wiki/wiki-stack/wiki-stack.component.html":
/*!*****************************************************************!*\
  !*** ./src/app/pages/wiki/wiki-stack/wiki-stack.component.html ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ul style=\"list-style-type:square;\">\r\n  <li><b>EIP (Enterprise Integration Patterns </b> - набор основных паттернов, используемых при проектировнии интеграций систем </li>\r\n  <li><b>Apache Camel </b> - основная библиотека, которая в себе содержит реализацию паттернов EIP</li>\r\n  <li><b>Java </b> - используется версия Java 8</li>\r\n  <li><b>Angular </b> - Java Script фреймворк, на котором сделан фронт для шины </li>\r\n  <li><b>EAP </b> - Red Hat Enterprise Application Platform - сервер приложений от компании Red Hat, в т.ч. для запуска Java EE приложений</li>\r\n  <li><b>Fuse </b> - Red Hat Fuse  - сервер приложений, адаптированный под работу с библиотекой apache camel</li>\r\n  <li><b>AMQ </b> - брокер сообщений, позволяющий обрабатывать очереди и подписки сообщений</li>\r\n  <li><b>DataGrid </b> - механизм кеширования сообщений</li>\r\n  <li><b>Openshift </b> - контейнерная платформа, на которой построена система</li>\r\n  <li><b>Angular </b> - фронт фреймворк от Google, на котором написан нащ фронт</li>\r\n</ul>"

/***/ }),

/***/ "./src/app/pages/wiki/wiki-stack/wiki-stack.component.ts":
/*!***************************************************************!*\
  !*** ./src/app/pages/wiki/wiki-stack/wiki-stack.component.ts ***!
  \***************************************************************/
/*! exports provided: WikiStackComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WikiStackComponent", function() { return WikiStackComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var WikiStackComponent = /** @class */ (function () {
    function WikiStackComponent() {
    }
    WikiStackComponent.prototype.ngOnInit = function () {
    };
    WikiStackComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-wiki-stack',
            template: __webpack_require__(/*! ./wiki-stack.component.html */ "./src/app/pages/wiki/wiki-stack/wiki-stack.component.html"),
            styles: [__webpack_require__(/*! ./wiki-stack.component.css */ "./src/app/pages/wiki/wiki-stack/wiki-stack.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], WikiStackComponent);
    return WikiStackComponent;
}());



/***/ }),

/***/ "./src/app/pages/wiki/wiki-systems/StatusPipe.ts":
/*!*******************************************************!*\
  !*** ./src/app/pages/wiki/wiki-systems/StatusPipe.ts ***!
  \*******************************************************/
/*! exports provided: StatusPipe */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StatusPipe", function() { return StatusPipe; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var StatusPipe = /** @class */ (function () {
    function StatusPipe() {
        this.items = [
            { label: 'Бэклог' },
            { label: 'Аналитика' },
            { label: 'В разработке' },
            { label: 'Тестирование' },
            { label: 'Разработана' },
        ];
    }
    StatusPipe.prototype.transform = function (value) {
        var args = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            args[_i - 1] = arguments[_i];
        }
        var result = 0;
        var iterator = 0;
        if (value != null) {
            this.items.forEach(function (val) {
                // console.log('value: ' + value + 'val: ' + val.label);
                iterator++;
                if (val.label === value) {
                    console.log('Сматчили: ' + val.label);
                    result = iterator;
                    return;
                    console.log('ВРОДЕ РЕТУРН НЕ РАБОТАЕТ В ДЖАВАСКРИПТ!');
                }
            });
        }
        console.log('RESULT:' + result);
        return result;
    };
    StatusPipe = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Pipe"])({
            name: 'statuspipe'
        })
    ], StatusPipe);
    return StatusPipe;
}());



/***/ }),

/***/ "./src/app/pages/wiki/wiki-systems/wiki-systems.component.css":
/*!********************************************************************!*\
  !*** ./src/app/pages/wiki/wiki-systems/wiki-systems.component.css ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3dpa2kvd2lraS1zeXN0ZW1zL3dpa2ktc3lzdGVtcy5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/pages/wiki/wiki-systems/wiki-systems.component.html":
/*!*********************************************************************!*\
  !*** ./src/app/pages/wiki/wiki-systems/wiki-systems.component.html ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p>\r\n    Дорожная карта интегрируемых систем\r\n</p>\r\n<p-dataView [value]=\"systems\">\r\n    <ng-template let-system pTemplate=\"listItem\">\r\n        <div class=\"ui-g\" style=\"padding: 2em;border-bottom: 1px solid #d9d9d9\">\r\n            <div class=\"ui-g-2 ui-sm-6\">Наименование: </div>\r\n            <div class=\"ui-g-10 ui-sm-6\"><b>{{system.name}}</b></div>\r\n\r\n            <div class=\"ui-g-2 ui-sm-6\">Описание: </div>\r\n            <div class=\"ui-g-10 ui-sm-6\"><b>{{system.description}}</b></div>\r\n\r\n            <div class=\"ui-g-2 ui-sm-6\">Ожидание: </div>\r\n            <div class=\"ui-g-10 ui-sm-6\"><b>{{system.expectedTime}}</b></div>\r\n\r\n            <div class=\"ui-g-2 ui-sm-6\">Микросервисы: </div>\r\n            <div class=\"ui-g-10 ui-sm-6\"><b>{{system.status | statuspipe}}</b></div>\r\n\r\n            <p-steps [model]=\"items\"  [(activeIndex)]=\"system.stage\" [readonly]=\"false\"></p-steps>\r\n        </div>\r\n    </ng-template>\r\n</p-dataView>"

/***/ }),

/***/ "./src/app/pages/wiki/wiki-systems/wiki-systems.component.ts":
/*!*******************************************************************!*\
  !*** ./src/app/pages/wiki/wiki-systems/wiki-systems.component.ts ***!
  \*******************************************************************/
/*! exports provided: WikiSystemsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WikiSystemsComponent", function() { return WikiSystemsComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var WikiSystemsComponent = /** @class */ (function () {
    function WikiSystemsComponent() {
        this.active = 2;
    }
    WikiSystemsComponent.prototype.ngOnInit = function () {
        this.systems = [
            { id: 1, name: "АСУ АЗС и DataLake (1 ОЦ)", description: "Интеграция между мобильной картой и озером данных", status: "Релиз", stage: 5, expectedTime: "1-квартал 2019" },
            { id: 2, name: "CRM и личный кабинет", description: "Интеграция между CRM Siebel и личным кабинетом (корпоративные продажи) в CRM систему", status: "В разработке", stage: 2, expectedTime: "2-квартал 2019" },
            { id: 3, name: "Скаут", description: "Интеграция с системой контроля транспорта (бензовозы)", status: "Разработана", stage: 3, expectedTime: "2-квартал 2019" },
            { id: 4, name: "Авторизация", description: "Механизм авторизации пользователей из DMZ  в КСПД", status: "В разработке", stage: 2, expectedTime: "3-квартал 2019" },
            { id: 5, name: "Контур.EDI", description: "добавить описание", status: "Разработана", stage: 3, expectedTime: "2-квартал 2019" },
            { id: 6, name: "Namos", description: "добавить описание", status: "Разработана", stage: 3, expectedTime: "2-квартал 2019" },
            { id: 7, name: "b2b КП", description: "добавить описание", status: "Бэклог", stage: 0, expectedTime: "2-квартал 2019" },
            { id: 8, name: "Этран", description: "добавить описание", status: "Разработана", stage: 3, expectedTime: "3-квартал 2019" },
            { id: 9, name: "МЦ ТЛК", description: "добавить описание", status: "Бэклог", stage: 0, expectedTime: "3-квартал 2019" },
            { id: 10, name: "SAP PI", description: "добавить описание", status: "Бэклог", stage: 0, expectedTime: "3-квартал 2019" },
            { id: 11, name: "Лк HR", description: "добавить описание", status: "Бэклог", stage: 0, expectedTime: "3-квартал 2019" },
            { id: 12, name: "G-Manager", description: "добавить описание", status: "Бэклог", stage: 0, expectedTime: "3-квартал 2019" },
            { id: 13, name: "Портал проектов ДРП", description: "", status: "В разработке", stage: 2, expectedTime: "3-квартал 2019" },
            { id: 14, name: "ЕАСКУ", description: "добавить описание", status: "Бэклог", stage: 0, expectedTime: "3-квартал 2019" },
            { id: 15, name: "Автозаказ", description: "добавить описание", status: "Бэклог", stage: 0, expectedTime: "4-квартал 2019" },
            { id: 16, name: "АСКУ НБ", description: "добавить описание", status: "Бэклог", stage: 0, expectedTime: "4-квартал 2019" },
            { id: 17, name: "Way4", description: "добавить описание", status: "Бэклог", stage: 0, expectedTime: "4-квартал 2019" },
            { id: 18, name: "Платформа ЭДО", description: "добавить описание", status: "В разработке", stage: 2, expectedTime: "4-квартал 2019" },
            { id: 19, name: "TBD", description: "добавить описание", status: "Бэклог", stage: 0, expectedTime: "4-квартал 2019" },
        ];
        this.items = [
            { label: 'Бэклог' },
            { label: 'Аналитика' },
            { label: 'В разработке' },
            { label: 'Тестирование' },
            { label: 'Разработана' },
            { label: 'Релиз' },
        ];
    };
    WikiSystemsComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-wiki-systems',
            template: __webpack_require__(/*! ./wiki-systems.component.html */ "./src/app/pages/wiki/wiki-systems/wiki-systems.component.html"),
            styles: [__webpack_require__(/*! ./wiki-systems.component.css */ "./src/app/pages/wiki/wiki-systems/wiki-systems.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], WikiSystemsComponent);
    return WikiSystemsComponent;
}());



/***/ }),

/***/ "./src/app/pages/wiki/wiki.component.css":
/*!***********************************************!*\
  !*** ./src/app/pages/wiki/wiki.component.css ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3dpa2kvd2lraS5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/pages/wiki/wiki.component.html":
/*!************************************************!*\
  !*** ./src/app/pages/wiki/wiki.component.html ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!--<div class=\"row\">-->\r\n    <!--<p-panel class=\"col\" header=\"WIKI ESB\" toggleable=\"true\">-->\r\n        <!--<p-tabView>-->\r\n            <!--<p-tabPanel header=\"Оглавление\">-->\r\n                <!--<p-panelMenu [model]=\"items\" [style]=\"{'width':'300px'}\"></p-panelMenu>-->\r\n            <!--</p-tabPanel>-->\r\n\r\n        <!--</p-tabView>-->\r\n\r\n        <!--Content-->\r\n    <!--</p-panel>-->\r\n    <!--<p-panel class=\"col-9\" header=\"Описание\">-->\r\n        <!--Сдесь будет описание-->\r\n    <!--</p-panel>-->\r\n<!--</div>-->\r\n<div class=\"container-fluid\">\r\n    <div class=\"row content\">\r\n        <div class=\"col-sm-2 sidenav\">\r\n            <h4>WIKI BUS</h4>\r\n            <ul class=\"nav nav-pills navbar-text\">\r\n                <li class=\"nav-item\">\r\n                    <a class=\"nav-link\" routerLink=\"/wiki/esb\" routerLinkActive=\"active\">ESB</a>\r\n                </li>\r\n                <li class=\"nav-item\">\r\n                    <a class=\"nav-link\" routerLink=\"/wiki/stack\" routerLinkActive=\"active\">Стэк технологий</a>\r\n                </li>\r\n                <li class=\"nav-item\">\r\n                    <a class=\"nav-link\" routerLink=\"/wiki/systems\" routerLinkActive=\"active\">Дорожная карта</a>\r\n                </li>\r\n                <li class=\"nav-item\">\r\n                    <a class=\"nav-link\" routerLink=\"/wiki/microservices\" routerLinkActive=\"active\">Микросервисы</a>\r\n                </li>\r\n                <li class=\"nav-item\">\r\n                    <a class=\"nav-link\" routerLink=\"/wiki/infrastructure\" routerLinkActive=\"active\">Инфраструктура</a>\r\n                </li>\r\n                <li class=\"nav-item\">\r\n                    <a class=\"nav-link\" routerLink=\"/wiki/crt\" routerLinkActive=\"active\">Сертификаты</a>\r\n                </li>\r\n                <li class=\"nav-item\">\r\n                    <a class=\"nav-link\" routerLink=\"/wiki/metrics\" routerLinkActive=\"active\">Метрики</a>\r\n                </li>\r\n                <li class=\"nav-item\">\r\n                    <a class=\"nav-link\" routerLink=\"/wiki/requiments\" routerLinkActive=\"active\">Требования к интеграции</a>\r\n                </li>\r\n            </ul><br>\r\n        </div>\r\n\r\n        <div class=\"col-sm-10\">\r\n            <router-outlet></router-outlet>\r\n        </div>\r\n    </div>\r\n</div>\r\n\r\n"

/***/ }),

/***/ "./src/app/pages/wiki/wiki.component.ts":
/*!**********************************************!*\
  !*** ./src/app/pages/wiki/wiki.component.ts ***!
  \**********************************************/
/*! exports provided: WikiComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WikiComponent", function() { return WikiComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var primeng_api__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! primeng/api */ "./node_modules/primeng/api.js");
/* harmony import */ var primeng_api__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(primeng_api__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");




var WikiComponent = /** @class */ (function () {
    function WikiComponent(messageService, sanitizer) {
        this.messageService = messageService;
        this.sanitizer = sanitizer;
    }
    WikiComponent.prototype.ngOnInit = function () {
    };
    WikiComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-wiki',
            template: __webpack_require__(/*! ./wiki.component.html */ "./src/app/pages/wiki/wiki.component.html"),
            providers: [primeng_api__WEBPACK_IMPORTED_MODULE_2__["MessageService"]],
            styles: [__webpack_require__(/*! ./wiki.component.css */ "./src/app/pages/wiki/wiki.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [primeng_api__WEBPACK_IMPORTED_MODULE_2__["MessageService"],
            _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__["DomSanitizer"]])
    ], WikiComponent);
    return WikiComponent;
}());



/***/ }),

/***/ "./src/app/pages/wiki/wiki.module.ts":
/*!*******************************************!*\
  !*** ./src/app/pages/wiki/wiki.module.ts ***!
  \*******************************************/
/*! exports provided: WikiModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WikiModule", function() { return WikiModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _wiki_routing_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./wiki-routing.module */ "./src/app/pages/wiki/wiki-routing.module.ts");
/* harmony import */ var _wiki_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./wiki.component */ "./src/app/pages/wiki/wiki.component.ts");
/* harmony import */ var _wiki_crt_wiki_crt_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./wiki-crt/wiki.crt.component */ "./src/app/pages/wiki/wiki-crt/wiki.crt.component.ts");
/* harmony import */ var _wiki_esb_wiki_esb_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./wiki-esb/wiki-esb.component */ "./src/app/pages/wiki/wiki-esb/wiki-esb.component.ts");
/* harmony import */ var _commons_copypaste_copy_paste_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../commons/copypaste/copy-paste.component */ "./src/app/commons/copypaste/copy-paste.component.ts");
/* harmony import */ var primeng_button__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! primeng/button */ "./node_modules/primeng/button.js");
/* harmony import */ var primeng_button__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(primeng_button__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var _wiki_systems_wiki_systems_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./wiki-systems/wiki-systems.component */ "./src/app/pages/wiki/wiki-systems/wiki-systems.component.ts");
/* harmony import */ var _wiki_stack_wiki_stack_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./wiki-stack/wiki-stack.component */ "./src/app/pages/wiki/wiki-stack/wiki-stack.component.ts");
/* harmony import */ var _wiki_metrics_wiki_metrics_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./wiki-metrics/wiki-metrics.component */ "./src/app/pages/wiki/wiki-metrics/wiki-metrics.component.ts");
/* harmony import */ var primeng_primeng__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! primeng/primeng */ "./node_modules/primeng/primeng.js");
/* harmony import */ var primeng_primeng__WEBPACK_IMPORTED_MODULE_13___default = /*#__PURE__*/__webpack_require__.n(primeng_primeng__WEBPACK_IMPORTED_MODULE_13__);
/* harmony import */ var primeng_dataview__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! primeng/dataview */ "./node_modules/primeng/dataview.js");
/* harmony import */ var primeng_dataview__WEBPACK_IMPORTED_MODULE_14___default = /*#__PURE__*/__webpack_require__.n(primeng_dataview__WEBPACK_IMPORTED_MODULE_14__);
/* harmony import */ var _wiki_microservices_wiki_microservices_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./wiki-microservices/wiki-microservices.component */ "./src/app/pages/wiki/wiki-microservices/wiki-microservices.component.ts");
/* harmony import */ var _wiki_systems_StatusPipe__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./wiki-systems/StatusPipe */ "./src/app/pages/wiki/wiki-systems/StatusPipe.ts");
/* harmony import */ var primeng_table__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! primeng/table */ "./node_modules/primeng/table.js");
/* harmony import */ var primeng_table__WEBPACK_IMPORTED_MODULE_17___default = /*#__PURE__*/__webpack_require__.n(primeng_table__WEBPACK_IMPORTED_MODULE_17__);
/* harmony import */ var _wiki_requiments_wiki_requiments_component__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./wiki-requiments/wiki-requiments.component */ "./src/app/pages/wiki/wiki-requiments/wiki-requiments.component.ts");
/* harmony import */ var _wiki_infrastructure_wiki_infrastructure_component__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ./wiki-infrastructure/wiki-infrastructure.component */ "./src/app/pages/wiki/wiki-infrastructure/wiki-infrastructure.component.ts");




















var WikiModule = /** @class */ (function () {
    function WikiModule() {
    }
    WikiModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                primeng_button__WEBPACK_IMPORTED_MODULE_9__["ButtonModule"],
                primeng_primeng__WEBPACK_IMPORTED_MODULE_13__["TooltipModule"],
                primeng_primeng__WEBPACK_IMPORTED_MODULE_13__["StepsModule"],
                _wiki_routing_module__WEBPACK_IMPORTED_MODULE_4__["WikiRoutingModule"],
                primeng_primeng__WEBPACK_IMPORTED_MODULE_13__["FieldsetModule"],
                primeng_dataview__WEBPACK_IMPORTED_MODULE_14__["DataViewModule"],
                primeng_table__WEBPACK_IMPORTED_MODULE_17__["TableModule"],
                primeng_primeng__WEBPACK_IMPORTED_MODULE_13__["TabViewModule"],
                primeng_primeng__WEBPACK_IMPORTED_MODULE_13__["DialogModule"],
                primeng_primeng__WEBPACK_IMPORTED_MODULE_13__["InputTextModule"],
                primeng_primeng__WEBPACK_IMPORTED_MODULE_13__["InputTextareaModule"],
                primeng_primeng__WEBPACK_IMPORTED_MODULE_13__["CodeHighlighterModule"]
            ],
            declarations: [
                primeng_primeng__WEBPACK_IMPORTED_MODULE_13__["MultiSelect"],
                _wiki_component__WEBPACK_IMPORTED_MODULE_5__["WikiComponent"],
                _wiki_crt_wiki_crt_component__WEBPACK_IMPORTED_MODULE_6__["WikiCrtComponent"],
                _wiki_esb_wiki_esb_component__WEBPACK_IMPORTED_MODULE_7__["WikiEsbComponent"],
                _commons_copypaste_copy_paste_component__WEBPACK_IMPORTED_MODULE_8__["CopyPasteComponent"],
                _wiki_systems_wiki_systems_component__WEBPACK_IMPORTED_MODULE_10__["WikiSystemsComponent"],
                _wiki_stack_wiki_stack_component__WEBPACK_IMPORTED_MODULE_11__["WikiStackComponent"],
                _wiki_metrics_wiki_metrics_component__WEBPACK_IMPORTED_MODULE_12__["WikiMetricsComponent"],
                _wiki_microservices_wiki_microservices_component__WEBPACK_IMPORTED_MODULE_15__["WikiMicroservicesComponent"],
                _wiki_systems_StatusPipe__WEBPACK_IMPORTED_MODULE_16__["StatusPipe"],
                _wiki_requiments_wiki_requiments_component__WEBPACK_IMPORTED_MODULE_18__["WikiRequimentsComponent"],
                _wiki_infrastructure_wiki_infrastructure_component__WEBPACK_IMPORTED_MODULE_19__["WikiInfrastructureComponent"],
            ]
        })
    ], WikiModule);
    return WikiModule;
}());



/***/ }),

/***/ "./src/app/services/auth.service.ts":
/*!******************************************!*\
  !*** ./src/app/services/auth.service.ts ***!
  \******************************************/
/*! exports provided: AuthService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthService", function() { return AuthService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var _model_auth_loginrequest_model__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../model/auth/loginrequest.model */ "./src/app/model/auth/loginrequest.model.ts");
/* harmony import */ var _model_auth_logout_request_model__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../model/auth/logout-request.model */ "./src/app/model/auth/logout-request.model.ts");







var AuthService = /** @class */ (function () {
    function AuthService(router, http) {
        this.router = router;
        this.http = http;
        this.codeMode = false;
        this.serviceURL = _environments_environment__WEBPACK_IMPORTED_MODULE_4__["environment"].authUrl;
        this.authURL = this.serviceURL + '/auth/auth';
        this.auth2fURL = this.serviceURL + '/auth/auth2fa';
        this.checkURL = this.serviceURL + '/auth/checkKey';
        this.rolesURL = this.serviceURL + '/auth/roles';
        this.getResponseURL = this.serviceURL + '/auth/getResponse';
        this.logoutUrl = this.serviceURL + '/auth/logout';
        this.kspdServiceUrl = _environments_environment__WEBPACK_IMPORTED_MODULE_4__["environment"].authKspdUrl;
        // this.token = 'token mocken!';
    }
    /**
     * Авторизация пользователя. Выполняется асинхронно в два шага.
     * @param email - он же username
     * @param password - пароль.
     */
    AuthService.prototype.signinUser = function (email, password) {
        var _this = this;
        this.token = null;
        this.authUser(email, password).subscribe(function (response) {
            var corId = response.correlationId;
            console.log('COR_ID: ' + corId);
            _this.asyncGetCircout(corId);
        }, function (error) {
            console.log('Ошибка обращения к сервису авторизации!');
            console.log(error);
        });
    };
    /**
     * Авторизация пользователя. Выполняется асинхронно в два шага.
     * @param email - он же username
     * @param password - пароль.
     */
    AuthService.prototype.signin2faUser = function (login, password) {
        var _this = this;
        this.token = null;
        this.authUser2fa(login, password).subscribe(function (response) {
            var corId = response.correlationId;
            console.log('COR_ID: ' + corId);
            if (response.correlationId != null) {
                _this.codeMode = true;
            }
        }, function (error) {
            console.log('Ошибка обращения к сервису авторизации!');
            console.log(error);
        });
    };
    AuthService.prototype.signInKspd = function (login, password) {
        var _this = this;
        this.token = null;
        this.authUser2fa(login, password).subscribe(function (response) {
            var corId = response.correlationId;
            console.log('COR_ID: ' + corId);
            if (response.correlationId != null) {
                _this.codeMode = true;
            }
        }, function (error) {
            console.log('Ошибка обращения к сервису авторизации!');
            console.log(error);
        });
    };
    AuthService.prototype.checkToken = function (secretCode) {
        var _this = this;
        this.getAuthTokenByCode(secretCode).subscribe(function (response) {
            console.log('Пришел ответ по secretCode: ' + secretCode);
            if (response.access_token != null && _this.token === null) {
                _this.token = response.access_token;
                _this.refreshToken = response.refresh_token;
                _this.getRoles();
                console.log('Авторизовались!');
            }
            if (response.code != null) {
                console.log('Код:' + response.code + '  ' + response.description);
            }
        }, function (error) {
            console.log('Ошибка обращения к сервису: ' + error);
        });
    };
    /**
     * Разлогинить пользователя. И на бэке, и на фронте.
     */
    AuthService.prototype.logout = function () {
        var _this = this;
        console.log('Выход из аккаунта');
        if (this.token != null && this.refreshToken != null) {
            this.logoutPost().subscribe(function () {
                console.log('Выполняется разлогирование!');
            }, function (error1) {
                console.log('Ошибка обращения к сервису авторизации!');
                console.log(error1);
            }, function () {
                _this.token = null;
                _this.refreshToken = null;
                _this.codeMode = false;
                _this.roles = null;
                _this.arrayRoles = null;
                console.log('Пользователь разлогинен!');
            });
        }
    };
    AuthService.prototype.getRoles = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                console.log('Получение ролей');
                this.roles = null;
                if (this.token != null) {
                    this.postRequestRoles().subscribe(function (response) {
                        var corId = response.correlationId;
                        // let corId = '123';
                        console.log('COR_ID: ' + corId);
                        _this.asyncGetRolesCircout(corId);
                    }, function (error1) {
                        console.log('Ошибка обращения к сервису авторизации!');
                        console.log(error1);
                    });
                }
                return [2 /*return*/];
            });
        });
    };
    //методы могут отличаться различными условиями выхода из цикла, количеством попыток и задержками по времени.
    AuthService.prototype.asyncGetCircout = function (correlationId) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var i;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        console.log('Получение токена. Пока токен:' + this.token);
                        i = 0;
                        _a.label = 1;
                    case 1:
                        if (!(i < 5)) return [3 /*break*/, 3];
                        i++;
                        if (this.token != null) {
                            this.getRoles();
                            return [2 /*return*/];
                        }
                        console.log('Запуск попытки: ' + i);
                        return [4 /*yield*/, this.delayedGetResponse(100, correlationId, i)];
                    case 2:
                        _a.sent();
                        console.log('Конец цикла');
                        return [3 /*break*/, 1];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    AuthService.prototype.asyncGetRolesCircout = function (correlationId) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var i;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        console.log('Получение ролей. Пока роли:' + this.roles);
                        i = 0;
                        _a.label = 1;
                    case 1:
                        if (!(i < 5)) return [3 /*break*/, 3];
                        i++;
                        if (this.roles != null) {
                            return [2 /*return*/];
                        }
                        console.log('Запуск попытки: ' + i);
                        return [4 /*yield*/, this.delayedGetResponse(100, correlationId, i)];
                    case 2:
                        _a.sent();
                        console.log('Конец цикла');
                        return [3 /*break*/, 1];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    AuthService.prototype.asyncGetMailSendResponse = function (correlationId) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var i;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        console.log('Получение статуса отправки кода');
                        i = 0;
                        _a.label = 1;
                    case 1:
                        if (!(i < 5)) return [3 /*break*/, 3];
                        i++;
                        if (this.codeMode != false) {
                            return [2 /*return*/];
                        }
                        console.log('Запуск попытки: ' + i);
                        return [4 /*yield*/, this.delayedGetResponse(100, correlationId, i)];
                    case 2:
                        _a.sent();
                        console.log('Конец цикла');
                        return [3 /*break*/, 1];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    AuthService.prototype.delayedGetResponse = function (ms, correlationId, attempt) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, new Promise(function (resolve) {
                            setTimeout(function () { return resolve(); }, ms * attempt);
                            console.log("Выждали " + ms * attempt + ' мс');
                            _this.getRequest(correlationId).subscribe(function (response) {
                                console.log('Пришел ответ по corId: ' + correlationId);
                                if (response.access_token != null && _this.token === null) {
                                    _this.token = response.access_token;
                                    _this.refreshToken = response.refresh_token;
                                    console.log('Токен получен с попытки: ' + attempt);
                                }
                                if (response.roles != null) {
                                    console.log('ROLES: ' + response.roles);
                                    console.log('ИМЯ: ' + response.preferred_username);
                                    _this.roles = response;
                                    _this.arrayRoles = response.roles;
                                    // this.arrayRoles = this.splitRoles(response);
                                }
                                if (response.code != null) {
                                    console.log('Код:' + response.code + '  ' + response.description);
                                    console.log('Неуспешная попытка: ' + attempt);
                                }
                            }, function (error) {
                                console.log('Ошибка обращения к сервису: ' + error);
                            });
                        })];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    AuthService.prototype.splitRoles = function (response) {
        if (response.roles === null) {
            return null;
        }
        var inroles = response.roles;
        var cutted = inroles.replace('[', '');
        var cutted2 = cutted.replace(']', '');
        return cutted2.split(',');
    };
    AuthService.prototype.authUser = function (login, password) {
        var body = new _model_auth_loginrequest_model__WEBPACK_IMPORTED_MODULE_5__["LoginrequestModel"]();
        body.username = login;
        body.password = password;
        console.log('LOGIN' + body.username + '  ' + login);
        return this.http.post(this.authURL, body);
    };
    AuthService.prototype.authUser2fa = function (login, password) {
        var body = new _model_auth_loginrequest_model__WEBPACK_IMPORTED_MODULE_5__["LoginrequestModel"]();
        body.username = login;
        body.password = password;
        console.log('LOGIN' + body.username);
        return this.http.post(this.auth2fURL, body);
    };
    AuthService.prototype.authKspd = function (login, password) {
        var body = new _model_auth_loginrequest_model__WEBPACK_IMPORTED_MODULE_5__["LoginrequestModel"]();
        body.username = login;
        body.password = password;
        // return this.http.post()
    };
    AuthService.prototype.logoutPost = function () {
        var body = new _model_auth_logout_request_model__WEBPACK_IMPORTED_MODULE_6__["LogoutRequestModel"]();
        body.accessToken = this.token;
        body.refreshToken = this.refreshToken;
        return this.http.post(this.logoutUrl, body);
    };
    AuthService.prototype.postRequestRoles = function () {
        if (this.isAuthenticated()) {
            var body = { accessToken: this.token };
            return this.http.post(this.rolesURL, body);
        }
        return null;
    };
    AuthService.prototype.getRolesRequest = function (correlationId) {
        console.log('Запросили роли по COR_ID ' + correlationId);
        return this.http.get(this.getResponseURL + '/' + correlationId);
    };
    AuthService.prototype.getAuthToken = function (correlationId) {
        console.log('Запросили токен по COR_ID ' + correlationId);
        return this.http.get(this.getResponseURL + '/' + correlationId);
    };
    AuthService.prototype.getAuthTokenByCode = function (secretCode) {
        console.log('Запросили токен по коду ' + secretCode);
        return this.http.get(this.checkURL + '/' + secretCode);
    };
    AuthService.prototype.getRequest = function (correlationId) {
        console.log('Запросили Object по COR_ID ' + correlationId);
        return this.http.get(this.getResponseURL + '/' + correlationId);
    };
    AuthService.prototype.isAuthenticated = function () {
        return this.token != null;
    };
    AuthService.prototype.isCodeMode = function () {
        return this.codeMode === true;
    };
    AuthService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"], _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"]])
    ], AuthService);
    return AuthService;
}());



/***/ }),

/***/ "./src/app/services/crm.service.ts":
/*!*****************************************!*\
  !*** ./src/app/services/crm.service.ts ***!
  \*****************************************/
/*! exports provided: CrmService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CrmService", function() { return CrmService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");





var CrmService = /** @class */ (function () {
    function CrmService(http) {
        this.http = http;
        this.serviceURL = _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].crmUrl;
        this.crmGasStationListURL = this.serviceURL + '/crm/fltGasStationsList';
        this.crmtGasStationDetailedListURL = this.serviceURL + '/crm/fltGasStationsDetailedList';
        this.crmGasStationDetails = this.serviceURL + '/crm/fltGasStationDetails';
        this.crmGetResultURL = this.serviceURL + '/crm/getResponse';
    }
    /**
     * Получение списка АЗС.
     */
    CrmService.prototype.postfltGasStationsList = function (body) {
        return this.http.post(this.crmGasStationListURL, body);
        // .pipe(
        //     this.handleError(new HttpErrorResponse(body))
        // );
    };
    /**
     * Результаты по списку АЗС.
     * @param corId
     */
    CrmService.prototype.getResultGasStationsList = function (corId) {
        return this.http.get(this.crmGetResultURL + '/' + corId);
    };
    CrmService.prototype.postfltGasStationsDetailedList = function (body) {
        return this.http.post(this.crmtGasStationDetailedListURL, body);
    };
    CrmService.prototype.azsDetails = function (body) {
        return this.http.post(this.crmGasStationDetails, body);
    };
    /**
     * Результаты по списку АЗС.
     * @param corId
     */
    CrmService.prototype.getResultAzsDetails = function (corId) {
        return this.http.get(this.crmGetResultURL + '/' + corId);
    };
    //TODO сделать обработку ошибок.
    CrmService.prototype.handleError = function (error) {
        if (error.error instanceof ErrorEvent) {
            // A client-side or network error occurred. Handle it accordingly.
            console.error('An error occurred:', error.error.message);
        }
        else {
            // The backend returned an unsuccessful response code.
            // The response body may contain clues as to what went wrong,
            console.error("Backend returned code " + error.status + ", " +
                ("body was: " + error.error));
        }
        // return an observable with a user-facing error message
        return Object(rxjs__WEBPACK_IMPORTED_MODULE_4__["throwError"])('Something bad happened; please try again later.');
    };
    ;
    CrmService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], CrmService);
    return CrmService;
}());



/***/ }),

/***/ "./src/app/services/etran.service.ts":
/*!*******************************************!*\
  !*** ./src/app/services/etran.service.ts ***!
  \*******************************************/
/*! exports provided: EtranService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EtranService", function() { return EtranService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");




var EtranService = /** @class */ (function () {
    function EtranService(http) {
        this.http = http;
        this.serviceURL = _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].etranUrl;
        this.invoidUrl = this.serviceURL + '/etran/api/getInvoice';
    }
    EtranService.prototype.getInvoice = function (requestBody) {
        //stub - возвращает заглушку
        // const httpOptions = {
        //    headers: new HttpHeaders({
        //        'accept': 'application/json',
        //        'content-type': 'application/json',
        //        'Stub': 'notnull'
        //     })
        // };
        return this.http.post(this.invoidUrl, requestBody);
    };
    EtranService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], EtranService);
    return EtranService;
}());



/***/ }),

/***/ "./src/app/services/mc.service.ts":
/*!****************************************!*\
  !*** ./src/app/services/mc.service.ts ***!
  \****************************************/
/*! exports provided: McService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "McService", function() { return McService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../environments/environment */ "./src/environments/environment.ts");




var McService = /** @class */ (function () {
    function McService(http) {
        this.http = http;
        this.serviceURL = _environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].mcUrl;
        this.mcGetAzsUrl = this.serviceURL + '/mc/api/getAzsList';
        this.mcGetShiftByDateUrl = this.serviceURL + '/mc/api/getShiftsByDateRange';
        this.mcGetShiftReportUrl = this.serviceURL + '/mc/api/getShiftReport';
        this.httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'accept': 'application/json',
                'content-type': 'application/json',
                'Authorization': 'Basic: bWM6bWM'
            })
        };
    }
    McService.prototype.getAzsList = function () {
        return this.http.get(this.mcGetAzsUrl, this.httpOptions);
    };
    McService.prototype.getShiftsByDateRange = function (azsId, startDate, endDate) {
        return this.http.get(this.mcGetShiftByDateUrl + '?azsId=' + azsId +
            '&startDate=' + startDate + '&endDate=' + endDate, this.httpOptions);
    };
    McService.prototype.getShiftReport = function (azsId, startShiftId, endShiftId) {
        return this.http.get(this.mcGetShiftReportUrl + '?azsId=' + azsId
            + '&startId=' + startShiftId + '&endId=' + endShiftId, this.httpOptions);
    };
    McService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
    ], McService);
    return McService;
}());



/***/ }),

/***/ "./src/app/services/ofd.service.ts":
/*!*****************************************!*\
  !*** ./src/app/services/ofd.service.ts ***!
  \*****************************************/
/*! exports provided: OfdService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OfdService", function() { return OfdService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var _model_ofd_AuthRequest__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../model/ofd/AuthRequest */ "./src/app/model/ofd/AuthRequest.ts");





var OfdService = /** @class */ (function () {
    function OfdService(http) {
        this.http = http;
        this.serviceURL = _environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].ofdUrl;
        this.authUrl = this.serviceURL + '/ofd/api/authorization/createAuthToken';
        this.innUrl = this.serviceURL + '/ofd/api/inn/';
    }
    /**
     * Авторизация.
     * AHTUNG:  Хард-код логин пароля.
     */
    OfdService.prototype.authorise = function () {
        var _this = this;
        this.authToken = null;
        var login = 'esb';
        var password = 'esb123';
        this.postAuth(login, password).subscribe(function (response) {
            if (response != null) {
                _this.authToken = response.AuthToken;
                _this.tokenExpDate = response.ExpirationDateUtc;
            }
        }, function (error) {
            console.log('Ошибка обращения к сервису авторизации!');
            console.log(error);
        });
    };
    OfdService.prototype.logout = function () {
        this.authToken = null;
    };
    OfdService.prototype.postAuth = function (login, password) {
        var body = new _model_ofd_AuthRequest__WEBPACK_IMPORTED_MODULE_4__["AuthRequest"]();
        body.login = login;
        body.password = password;
        return this.http.post(this.authUrl, body);
    };
    OfdService.prototype.getKkts = function (inn) {
        var httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({
                'accept': 'application/json',
                'content-type': 'application/json',
                'AuthToken': this.authToken
            })
        };
        return this.http.get(this.innUrl + inn + '/kkts', httpOptions);
    };
    OfdService.prototype.getReceiptsStatistic = function (inn, kkt, marker, dateFrom, dateTo) {
        var httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({
                'accept': 'application/json',
                'content-type': 'application/json',
                'AuthToken': this.authToken
            })
        };
        var pathstring = this.parseDates(dateFrom, dateTo);
        return this.http.get(this.innUrl + inn + '/kkt/' + kkt +
            '/statistics' + pathstring +
            '&marker=' + marker, httpOptions);
    };
    OfdService.prototype.parseDates = function (dateFrom_in, dateTo_in) {
        var parsed = '?dateFrom=' + dateFrom_in.getFullYear() + '-' + dateFrom_in.getMonth()
            + '-' + dateFrom_in.getDate() + 'T' + this.localise(dateFrom_in.getHours()) + ':'
            + this.localise(dateFrom_in.getMinutes()) + ':' + '00' +
            '&dateTo=' + dateTo_in.getFullYear() + '-' + dateTo_in.getMonth() + '-' + dateTo_in.getDate() + 'T' +
            this.localise(dateTo_in.getHours()) + ':' + this.localise(dateTo_in.getMinutes()) + ':' + '00';
        return parsed;
    };
    OfdService.prototype.localise = function (digit) {
        return digit.toLocaleString('ru-RU', { minimumIntegerDigits: 2 });
    };
    OfdService.prototype.getReceiptsList = function (inn, kkt, dateFrom, dateTo) {
        var httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({
                'accept': 'application/json',
                'content-type': 'application/json',
                'AuthToken': this.authToken
            })
        };
        return this.http.get(this.innUrl + inn + '/kkts', httpOptions);
    };
    OfdService.prototype.isAuthenticated = function () {
        return this.authToken != null;
    };
    OfdService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], OfdService);
    return OfdService;
}());



/***/ }),

/***/ "./src/app/services/scout.service.ts":
/*!*******************************************!*\
  !*** ./src/app/services/scout.service.ts ***!
  \*******************************************/
/*! exports provided: ScoutService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ScoutService", function() { return ScoutService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../environments/environment */ "./src/environments/environment.ts");




/**
 * Сервис для обращения к шине данныех.
 */
var ScoutService = /** @class */ (function () {
    function ScoutService(http) {
        this.http = http;
        this.serviceURL = _environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].scoutUrl;
        this.scoutIdsURL = this.serviceURL + '/scout/api/units/availableIds';
        this.scoutFuelURL = this.serviceURL + '/scout/statistics/fuel?';
        this.scoutOdoURL = this.serviceURL + '/scout/statistics/odometer?';
        this.scoutTracksURL = this.serviceURL + '/scout/tracks?';
    }
    /**
     * Получение списка техники.
     */
    ScoutService.prototype.getUnits = function () {
        return this.http.get(this.scoutIdsURL);
    };
    /**
     * Получение статистики по топливу.
     * @param unit - ид техники
     * @param startDate - начало
     * @param endDate - окончание
     * @param dayly - тип отчета по дням
     */
    ScoutService.prototype.getFuel = function (unit, startDate, endDate, dayly) {
        console.log('ENV VALUE: ' + this.serviceURL);
        console.log('Date To String:' + startDate.getFullYear() + '%2F' + startDate.getMonth() + '%2F' + startDate.getDate() + ' 00:00:00');
        return this.http.get(this.scoutFuelURL +
            this.requestHelper(unit, startDate, endDate, dayly));
    };
    /**
     * Получение статистики по одометру.
     * @param unit - ид техники
     * @param startDate - начало
     * @param endDate - окончание
     * @param dayly - тип отчета по дням
     */
    ScoutService.prototype.getOdometer = function (unit, startDate, endDate, dayly) {
        return this.http.get(this.scoutOdoURL +
            this.requestHelper(unit, startDate, endDate, dayly));
    };
    ScoutService.prototype.getTracks = function (unit, startDate, endDate, dayly) {
        return this.http.get(this.scoutTracksURL +
            this.requestHelper(unit, startDate, endDate, dayly));
    };
    ScoutService.prototype.requestHelper = function (unit, startDate, endDate, dayly) {
        var pathstring = 'request.unitId=' + unit.id + '&request.beginDateTime=' + startDate.getFullYear() + '%2F' + (startDate.getMonth() + 1)
            + '%2F' + startDate.getDate() + ' 00:00:00' + '&request.endDateTime=' + endDate.getFullYear()
            + '%2F' + (endDate.getMonth() + 1) + '%2F' + endDate.getDate() + ' 00:00:00';
        if (dayly) {
            pathstring = pathstring + '&request.interval=day';
        }
        console.log('Handled date request:' + pathstring);
        return pathstring;
    };
    ScoutService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], ScoutService);
    return ScoutService;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false,
    scoutUrl: 'https://scoutintegration-scoutintegration.kspd-apps.demo.rht.ru',
    // scoutUrl: 'http://localhost:8092',
    crmUrl: 'https://crmintegration-crmintegration.dmz-apps.demo.rht.ru',
    // mcUrl: 'http://localhost:8080/eap-mc-web',
    // mcUrl: 'http://eap-mc-ear-mobilcard.kspd-apps.demo.rht.ru/eap-mc-web',
    mcUrl: 'https://fuse-mc-rest-mobilcard.kspd-apps.demo.rht.ru',
    // crmUrl: 'http://localhost:8085'
    authUrl: 'https://authdmzservice-authdmzservice.dmz-apps.demo.rht.ru',
    authDmzUrl: '',
    authKspdUrl: 'https://sso-crm.kspd-apps.demo.rht.ru/auth/realms/spring-security-quickstart/protocol/openid-connect',
    // authUrl: 'http://localhost:8080/eap-mc-web/rest',
    etranUrl: 'https://etrankspdservice-etrankspdservice.kspd-apps.demo.rht.ru',
    // etranUrl: 'http://localhost:8090',
    esbBackUrl: 'https://backforfront-backforfront.dmz-apps.demo.rht.ru',
    // esbBackUrl: 'http://localhost:8080',
    ofdUrl: 'https://ofd-ofdkspdservice.kspd-apps.demo.rht.ru'
    // ofdUrl: 'http://localhost:8090'
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.error(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\WORKSPACE\fronteap\src\main\front\src\main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map