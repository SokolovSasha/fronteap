// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  scoutUrl: 'https://scoutintegration-scoutintegration.kspd-apps.demo.rht.ru',
  // scoutUrl: 'http://localhost:8092',
  crmUrl: 'https://crmintegration-crmintegration.dmz-apps.demo.rht.ru',
  // mcUrl: 'http://localhost:8080/eap-mc-web',
  // mcUrl: 'http://eap-mc-ear-mobilcard.kspd-apps.demo.rht.ru/eap-mc-web',
  mcUrl: 'https://fuse-mc-rest-mobilcard.kspd-apps.demo.rht.ru',
  // crmUrl: 'http://localhost:8085'
  authUrl: 'https://authdmzservice-authdmzservice.dmz-apps.demo.rht.ru',
  authDmzUrl: '',
  authKspdUrl: 'https://sso-crm.kspd-apps.demo.rht.ru/auth/realms/spring-security-quickstart/protocol/openid-connect',
  // authUrl: 'http://localhost:8080/eap-mc-web/rest',
  etranUrl: 'https://etrankspdservice-etrankspdservice.kspd-apps.demo.rht.ru',
  // etranUrl: 'http://localhost:8090',
  esbBackUrl: 'https://backforfront-backforfront.dmz-apps.demo.rht.ru',
  // esbBackUrl: 'http://localhost:8080',
  ofdUrl: 'https://ofd-ofdkspdservice.kspd-apps.demo.rht.ru'
  // ofdUrl: 'http://localhost:8090'
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
