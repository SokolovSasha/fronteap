export const environment = {
  production: false,
  test: true,
  scoutUrl: 'http://scoutintegration-scoutintegration.kspd-apps.demo.rht.ru',
  crmUrl: 'http://crmintegration-crmintegration.dmz-apps.demo.rht.ru',
  // mcUrl: 'http://localhost:8080/eap-mc-web',
  mcUrl: 'http://eap-mc-ear-mobilcard.kspd-apps.demo.rht.ru/eap-mc-web',
  // scoutUrl: 'http://localhost:8090',
  // crmUrl: 'http://localhost:8085'
  authUrl: 'http://authdmzservice-authdmzservice.dmz-apps.demo.rht.ru',
  // authUrl: 'http://localhost:8080/eap-mc-web/rest',
  esbBackUrl: 'http://localhost:8080/'
};
