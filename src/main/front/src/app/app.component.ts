import {Component} from '@angular/core';
import {AuthService} from "./services/auth.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  menuLabel: string;
  constructor(public authService: AuthService) {
    console.warn("Можешь лучше - пиши нам esb_drcp@gazprom-neft.ru");
  }

  private updateMenuLabel(): void {
    if (this.authService.roles != null && this.authService.roles.preferred_username != null) {
      this.menuLabel = this.authService.roles.preferred_username;
    }else {
      this.menuLabel = 'Управление';
    }
  }

}
