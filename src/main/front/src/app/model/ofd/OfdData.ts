export class OfdData {
            "Tag": number;
            "User": "string";
            "UserInn": "string";
            "Number": number;
            "DateTime": "string";
            "ShiftNumber": number;
            "OperationType": "string";
            "TaxationType": number;
            "Operator": "string";
            "KktRegNumber": "string";
            "FnFactoryNumber": "string";
            "Items": [
                {
                    "Name": "string",
                    "Price": number,
                    "Quantity": "string",
                    "Nds00TotalSumm": number,
                    "Total": number,
                    "CalculationMethod": number,
                    "NdsRate": number,
                    "NdsSumm": number
                }
                ];
            "RetailPlaceAddress": "string";
            "SenderAddress": "string";
            "BuyerAddress": "string";
            "Nds18TotalSumm": number;
            "Nds10TotalSumm": number;
            "NdsNATotalSumm": number;
            "Nds18CalculatedTotalSumm": number;
            "Nds10CalculatedTotalSumm": number;
            "Nds00TotalSumm": number;
            "AmountTotal": number;
            "AmountCash": number;
            "AmountECash": number;
            "DocumentNumber": number;
            "FiscalSign": "string";
            "DecimalFiscalSign": "string";
            "FormatVersion": number;
            "AmountAdvance": number;
            "AmountLoan": number;
            "AmountGranting": number;
            "TaxAuthoritySite": "string";
            "CalculationPlace": "string";
            "OperatorINN": "string";
            "ExtraProperty": [
                {
                    "ExtraPropertyName": "string",
                    "ExtraPropertyValue": number
                }
                ]

}