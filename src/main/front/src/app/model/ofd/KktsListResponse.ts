export class KktsListResponse {
 public status: string;
 public elapsed: string;
 public erros: string[];
 public data: [
     {
         id: string;
         KktRegId: string;
         serialNumber: string;
         fnNumber: string;
         createDate: string;
         checkDate: string;
         firstDocumentDate: string;
         paymentDate: string;
         signDate: string;
         activationDate: string;
         contractStartDate: string;
         contractEndDate: string;
         lastDocOnKktDateTime: string;
         lastDocOnOfdDateTimeUtc: string;
         path: string;
         kktModel: string;
     }
     ]

}


