export class WidgetStatInfo {
    public statisticsMarker: string;
    public name: string;
    public value: string;
}