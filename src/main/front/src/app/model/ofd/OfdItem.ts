export class OfdItem {
    public Name: "string";
    public Price: number;
    public Quantity: "string";
    public Nds00TotalSumm: number;
    public Total: number;
    public CalculationMethod: number;
    public NdsRate: number;
    public NdsSumm: number
}