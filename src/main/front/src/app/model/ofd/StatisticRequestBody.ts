export class StatisticRequestBody {
    public dateFrom: Date;
    public dateTo: Date;
    public marker: string;

}