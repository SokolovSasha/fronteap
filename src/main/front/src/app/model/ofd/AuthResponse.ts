export class AuthResponse {
    public AuthToken: string;
    public ExpirationDateUtc: Date;
}