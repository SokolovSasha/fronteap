export class DetailedReceiptsListWithStatResponse{

    "Status": "string";
    "Elapsed": "string";
    "Errors": [
        "string"
        ];
    "Statistics": [
        {
            "StatisticsMarker": string,
            "Name": string,
            "Value": number
        }
        ];
    public Data: [
        {
            "Tag": 0,
            "User": "string",
            "UserInn": "string",
            "Number": 0,
            "DateTime": "string",
            "ShiftNumber": 0,
            "OperationType": "string",
            "TaxationType": 0,
            "Operator": "string",
            "KktRegNumber": "string",
            "FnFactoryNumber": "string",
            "Items": [
                {
                    "Name": "string",
                    "Price": 0,
                    "Quantity": "string",
                    "Nds00TotalSumm": 0,
                    "Total": 0,
                    "CalculationMethod": 0,
                    "NdsRate": 0,
                    "NdsSumm": 0
                }
                ],
            "RetailPlaceAddress": "string",
            "SenderAddress": "string",
            "BuyerAddress": "string",
            "Nds18TotalSumm": 0,
            "Nds10TotalSumm": 0,
            "NdsNATotalSumm": 0,
            "Nds18CalculatedTotalSumm": 0,
            "Nds10CalculatedTotalSumm": 0,
            "Nds00TotalSumm": 0,
            "AmountTotal": 0,
            "AmountCash": 0,
            "AmountECash": 0,
            "DocumentNumber": 0,
            "FiscalSign": "string",
            "DecimalFiscalSign": "string",
            "FormatVersion": 0,
            "AmountAdvance": 0,
            "AmountLoan": 0,
            "AmountGranting": 0,
            "TaxAuthoritySite": "string",
            "CalculationPlace": "string",
            "OperatorINN": "string",
            "ExtraProperty": [
                {
                    "ExtraPropertyName": "string",
                    "ExtraPropertyValue": 0
                }
                ]
        }
        ]
}