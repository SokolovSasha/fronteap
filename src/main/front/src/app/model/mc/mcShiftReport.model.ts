export class McShiftReport {
    public azsId: number;
    public begShiftID: number;
    public dept: [{
        "cashPay": number,
        "creditPay": number
        "deptName": string,
        "nonCashPay": number,
        "orderNo": number,
        "mGroupNo": number,
        "id": number
    }];

    public diverg: any;
    public ecrZnums: [
        {
            "amountinCash": number,
            "cashBackSum": number,
            "collectionSum": number,
            "dateTime": string,
            "deposition": number,
            "saleSum": number,
            "serialNumber": string,
            "nZrep": number,
            "id": number
        }];
    public endShiftID: number;
    public expense: [
        {
            "articleName": string,
            "endRestVol": number,
            "externalStrID": string,
            "orderNo": number,
            "payType": string,
            "payTypeName": string,
            "paytypeID": number,
            "saleMon": number,
            "saleVol": number,
            "id": number
        }];
    public goods: [
        {
            "beginRestMon": string,
            "endRestMon": string,
            "expenseMon": string,
            "receiptMon": string,
            "revisionMon": string,
            "saleMon": string,
            "sectionCode": string,
            "sectionName": string,
            "id": number
        }
        ];
    public operators: string;
    public overflow: any;
    public receipt: any;
    public saldo: [
        {
            "nameSaldo": string,
            "saldoMon": string,
            "st5Key": string,
            "id": number
        }
    ];

    public  trk: [
        {
            "articleName": string,
            "beginBookRest": number,
            "beginBookRestMass": number,
            "beginCounter": number,
            "beginFactRest": number,
            "beginFactRestMass": number,
            "bookExpenseVol": number,
            "commonLev": number,
            "deficitMass": number,
            "deficitVol": number,
            "endBookRest": number,
            "endBookRestMass": number,
            "endCounter": number,
            "endFactRest": number,
            "endFactRestMass": number,
            "errorNozzle": any,
            "errorNozzleRel": any,
            "expenseMass": number,
            "expenseVol": number,
            "externalStrID": string,
            "measurementMsg": string,
            "mernikVol": any,
            "nozzle": number,
            "pipeMass": number,
            "pipeVol": number,
            "receipMmass": number,
            "receipt": number,
            "relocMass": number,
            "relocVol": number,
            "surplusMass": number,
            "surplusVol": number,
            "tankDensity": number,
            "tankID": string,
            "tankName": string,
            "trkNo": number,
            "waterLev": number,
            "id": number
        }];
    public username: string;
}