export class McShift {
    public closeSoftVersion: string;
    public dateBeg: string;
    public dateEnd: string;
    public openSoftVersion: string;
    public userName: string;
    public id: number;
}