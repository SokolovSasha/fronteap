export class PropPairModel {
    constructor(key: number, val: string){
        this.key = key;
        this.val = val;
    }
    public key: number;
    public val: string;
}