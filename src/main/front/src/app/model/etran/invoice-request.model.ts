export class InvoiceRequest {

public invoiceID: {
        value: string,
        valueAttribute: string
    };
public invNumber: {
        value: string,
        valueAttribute: string
    };
public useMod11: "string";
public useArh: "string";
public usePay: "string";
public useVohr: "string";
public useDocLink: {
        "value": "string",
        "valueAttribute": "string"
    };
public useValid: "string";
public useCustomBroker: "string";
public version: "string";
}