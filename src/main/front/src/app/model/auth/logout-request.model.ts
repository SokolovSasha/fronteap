export class LogoutRequestModel {
    public accessToken: string;
    public refreshToken: string;
}