export class UserInfoModel {
    public sub: string;
    public email_verified: boolean;
    public preferred_username: string;
    public given_name: string;
    public family_name: string;
    public clientroles: string[];
    public email: string;
    public phone: string;
}
