export class AuthRolesModel {
    public sub: string;
    public roles: string[];
    public preferred_username: string;
}