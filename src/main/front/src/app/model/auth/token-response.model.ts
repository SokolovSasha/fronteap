export class TokenResponseModel {
    public access_token: string;
    public expires_in: number;
    public refresh_expires_in: number;
    public refresh_token: string;
    public token_type: string;
    public "not-before-policy": string;
    public session_state: string;
}