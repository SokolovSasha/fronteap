export class ErrorResponse {
    public code: string;
    public description: string;
}