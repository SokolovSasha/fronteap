import {Component, Inject, Input} from "@angular/core";
import { DOCUMENT } from '@angular/common';

@Component({
    selector: 'app-cp',
    templateUrl: './copy-paste.component.html',
    styleUrls: ['./copy-paste.component.css'],
})
export class CopyPasteComponent {
    @Input()
    inputText: string;
    constructor(@Inject(DOCUMENT) public document: any){
    }
    public copypaste(text: string) {
        console.log('Fired: ' + text);
        var textArea = document.createElement("textarea");
        textArea.style.position = 'fixed';
        textArea.style.top = '-999px';
        textArea.style.left = '-999px';
        textArea.style.width = '2em';
        textArea.style.height = '2em';
        textArea.style.padding = '0';
        textArea.style.border = 'none';
        textArea.style.outline = 'none';
        textArea.style.boxShadow = 'none';
        textArea.style.background = 'transparent';
        textArea.value = text;
        document.body.appendChild(textArea);
        textArea.select();
        try {
            this.document.execCommand('copy');
        }catch(error){
            console.log('Невозможно скопировать!' + error);
        }
    }
}