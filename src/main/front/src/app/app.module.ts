import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {
  AccordionModule,
  ButtonModule,
  CalendarModule,
  CheckboxModule,
  ChipsModule,
  DialogModule,
  DropdownModule,
  GMapModule,
  InputSwitchModule,
  InputTextModule,
  PaginatorModule,
  PanelMenuModule,
  PanelModule,
  TabViewModule,
  TooltipModule
} from 'primeng/primeng';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {FormsModule} from '@angular/forms';
import {ScoutService} from './services/scout.service';
import {HttpClientModule} from '@angular/common/http';
import {TableModule} from 'primeng/table';
import {RouterModule, Routes} from '@angular/router';
import {CrmComponent} from './pages/crm/crm.component';
import {ScoutComponent} from './pages/scout/scout.component';
import {UnitPickerComponent} from './pages/scout/unit-picker.component';
import {CrmService} from './services/crm.service';
import {ToastModule} from 'primeng/toast';
import {McComponent} from './pages/mc/mc.component';
import {McService} from './services/mc.service';
import {PreloaderComponent} from './pages/preloader/preloader.component';
import {SigninComponent} from './auth/signin/signin.component';
import {AuthService} from './services/auth.service';
import {PagenotfoundComponent} from './pages/404/pagenotfound.component';
import {MonitoringComponent} from './pages/monitoring/monitoring.component';
import {WikiModule} from './pages/wiki/wiki.module';
import {Signin2Component} from './auth/signin2/signin2.component';
import {EtranComponent} from './pages/etran/etran.component';
import {EtranService} from './services/etran.service';
import {MailerComponent} from './pages/mailer/mailer.component';
import {SignindmzComponent} from './auth/signindmz/signindmz.component';
import {SigninkspdComponent} from './auth/signinkspd/signinkspd.component';
import {OfdComponent} from './pages/ofd/ofd.component';
import {OfdService} from './services/ofd.service';
import { SuccessComponent } from './auth/success/success.component';
import {SsoService} from "./services/sso.service";

const routes: Routes = [
  {path: '', redirectTo: '/wiki/esb', pathMatch: 'full'},
  {path: 'scout', component: ScoutComponent},
  {path: 'crm', component: CrmComponent},
  {path: 'mc', component: McComponent},
  {path: 'etran', component: EtranComponent},
  {path: 'monitoring', component: MonitoringComponent},
  {path: 'ofd', component: OfdComponent},
  {path: 'signin', component: SigninComponent},
  {path: 'signin2', component: Signin2Component},
  {path: 'signin_dmz', component: SignindmzComponent},
  {path: 'signin_kspd', component: SigninkspdComponent},
  {path: 'success/:authCode2/:state', component: SuccessComponent},
  {path: '**', component: PagenotfoundComponent}
];

@NgModule({
  declarations: [
    AppComponent,
    CrmComponent,
    McComponent,
    PagenotfoundComponent,
    ScoutComponent,
    SigninComponent,
    UnitPickerComponent,
    PreloaderComponent,
    MonitoringComponent,
    Signin2Component,
    EtranComponent,
    MailerComponent,
    SignindmzComponent,
    SigninkspdComponent,
    OfdComponent,
    SuccessComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AccordionModule,
    CheckboxModule,
    ChipsModule,
    InputTextModule,
    ToastModule,
    PaginatorModule,
    DialogModule,
    TableModule,
    TabViewModule,
    HttpClientModule,
    InputSwitchModule,
    CalendarModule,
    PanelModule,
    PanelMenuModule,
    FormsModule,
    ButtonModule,
    GMapModule,
    DropdownModule,
    WikiModule,
    TooltipModule,
    RouterModule.forRoot(routes, { useHash: true})
  ],
  exports: [RouterModule],
  providers: [AuthService, ScoutService, CrmService, McService, EtranService, OfdService, SsoService],
  bootstrap: [AppComponent]
})


export class AppModule {

}
