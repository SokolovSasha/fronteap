import { Component, OnInit } from '@angular/core';
import {NgForm} from "@angular/forms";
import {AuthService} from "../../services/auth.service";
import {MessageService} from "primeng/api";

@Component({
  selector: 'app-signin2',
  templateUrl: './signin2.component.html',
  styleUrls: ['./signin2.component.css'],
  providers: [MessageService]
})
export class Signin2Component implements OnInit {

  constructor(public authService: AuthService) { }

  ngOnInit() {
    console.log ('CODEMODE: ' + this.authService.codeMode);
  }

  onSignin2(form: NgForm) {
    const login = form.value.login;
    const password = form.value.password;
    this.authService.signin2faUser(login, password);
  }

  onCodeSubmit(form: NgForm) {
    const secretCode = form.value.secretCode;
    this.authService.checkToken(secretCode);
  }

  returnToLogin(){
    console.log('CodeMode Off');
    this.authService.codeMode = false;
    console.log('CodeMode Off');
    if (this.authService.isAuthenticated()) {
      console.log('Запуск разлогина!');
      this.authService.logout();
    }
  }
}
