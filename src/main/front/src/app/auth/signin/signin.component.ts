import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';

import { AuthService } from '../../services/auth.service';
import {MessageService} from "primeng/api";
import {DomSanitizer} from "@angular/platform-browser";
import {environment} from "../../../environments/environment";

@Component({
    selector: 'app-signin',
    templateUrl: './signin.component.html',
    styleUrls: ['./signin.component.css'],
    providers: [MessageService]
})
export class SigninComponent implements OnInit {

    constructor(public authService: AuthService, private sanitizer: DomSanitizer) {
        this._swaggerUrl = environment.authUrl + '/webjars/swagger-ui/index.html?url=/auth/api-doc';
        this.trustedSwaggerUrl = sanitizer.bypassSecurityTrustResourceUrl(this._swaggerUrl);
    }
    private _swaggerUrl:string;

    trustedSwaggerUrl: any;
    ngOnInit() {
    }

    onSignin(form: NgForm) {
        const email = form.value.email;
        const password = form.value.password;
        this.authService.signinUser(email, password);
    }

}