import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, ParamMap} from '@angular/router';
import {Observable} from 'rxjs';
import {map, switchMap} from 'rxjs/operators';
import {SsoService} from '../../services/sso.service';
import {UserInfoModel} from "../../model/auth/user-info.model";
import {TokenResponseModel} from "../../model/auth/token-response.model";
// import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-success',
  templateUrl: './success.component.html',
  styleUrls: ['./success.component.css']
})
export class SuccessComponent implements OnInit {

  constructor(private route: ActivatedRoute, private ssoService: SsoService) {
  }


  token: string;
  refreshToken: string;
  authCode2: string;
  private sub: any;
  private userInfo: UserInfoModel;
  private tokenResponse: TokenResponseModel;

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      this.authCode2 = params['authCode2'];
      console.log(params['authCode2']); // (+) converts string 'id' to a number
    });
    this.auth(this.authCode2);
  }
  private auth(authCode: string) {
      this.ssoService.getTokenByAuthCode2(authCode).subscribe(
          (response: any) => {
              console.log('Пришел ответ по AuthCode2: ' + this.authCode2);
              if (response.access_token != null) {
                  this.tokenResponse = response;
                  this.token = response.access_token;
                  this.refreshToken = response.refresh_token;
                  console.log('ТОКЕН ПОЛУЧЕН: ' + this.token);
              }

              if (response.code != null) {
                  console.log('Код:' + response.code + '  ' + response.description);
              }

          },
          error => {
              console.log('Ошибка обращения к сервису: ' + error);
          }, () => {
              console.log('Авторизовались, получаем детали!');
              this.getUserInfo();
              console.log('Запрос деталей выполнен');
          }
      );
      this.getUserInfo();
      console.log('Авторизовались!' + this.token);
  }
  private getUserInfo() {
      this.ssoService.getUserInfo(this.getToken()).subscribe(
          (response: UserInfoModel) => {
            this.userInfo = response;
            console.log('Добро пожаловать, ');
          },
          error => {
              console.log('Ошибка обращения к сервису: ' + error);
          },
          () => {
              console.log('Детали получены');
          }
      );
  }
  private getToken (): string {
    return this.tokenResponse.access_token;
  }

}
