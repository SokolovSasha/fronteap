import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SigninkspdComponent } from './signinkspd.component';

describe('SigninkspdComponent', () => {
  let component: SigninkspdComponent;
  let fixture: ComponentFixture<SigninkspdComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SigninkspdComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SigninkspdComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
