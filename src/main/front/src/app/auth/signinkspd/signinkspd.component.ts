import { Component, OnInit } from '@angular/core';
import {AuthService} from "../../services/auth.service";
import {NgForm} from "@angular/forms";

@Component({
  selector: 'app-signinkspd',
  templateUrl: './signinkspd.component.html',
  styleUrls: ['./signinkspd.component.css']
})
export class SigninkspdComponent implements OnInit {

  constructor(public authService: AuthService) {

  }
  ngOnInit() {
  }

  onSignin(form: NgForm) {
    const email = form.value.email;
    const password = form.value.password;
    this.authService.signInKspd(email, password);
  }

}
