import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SignindmzComponent } from './signindmz.component';

describe('SignindmzComponent', () => {
  let component: SignindmzComponent;
  let fixture: ComponentFixture<SignindmzComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SignindmzComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SignindmzComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
