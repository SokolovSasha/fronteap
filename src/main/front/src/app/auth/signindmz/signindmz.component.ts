import { Component, OnInit } from '@angular/core';
import {AuthService} from "../../services/auth.service";
import {DomSanitizer} from "@angular/platform-browser";
import {environment} from "../../../environments/environment";
import {NgForm} from "@angular/forms";

@Component({
  selector: 'app-signindmz',
  templateUrl: './signindmz.component.html',
  styleUrls: ['./signindmz.component.css']
})
export class SignindmzComponent implements OnInit {

  constructor(public authService: AuthService) {

  }
  ngOnInit() {
  }

  onSignin(form: NgForm) {
    const email = form.value.email;
    const password = form.value.password;
    this.authService.signinUser(email, password);
  }

}
