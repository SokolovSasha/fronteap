import {Component, OnInit} from "@angular/core";
import {McService} from "../../services/mc.service";
import {McAzs} from "../../model/mc/mcAzs.model";
import {MessageService} from "primeng/api";
import {McShift} from "../../model/mc/mcShift.model";
import {McShiftReport} from "../../model/mc/mcShiftReport.model";
import {environment} from "../../../environments/environment";
import {DomSanitizer} from "@angular/platform-browser";

@Component({
    selector: 'app-mc',
    templateUrl: './mc.component.html',
    styleUrls: ['./mc.component.css'],
    providers: [MessageService]
})
export class McComponent implements OnInit {
    constructor (private mcService: McService, private messageService: MessageService,
                 private sanitizer: DomSanitizer) {
        this._swaggerUrl = environment.mcUrl + '/webjars/swagger-ui/index.html?url='
            + environment.mcUrl + '/mc/api-doc/swagger.json';
        this.trustedSwaggerUrl = sanitizer.bypassSecurityTrustResourceUrl(this._swaggerUrl);
    }
    ngOnInit(): void {

        this.getAzsList();
    }
    private _swaggerUrl:string;
    trustedSwaggerUrl: any;
    public frozencols: any[];
    public azsList: McAzs[];
    public shiftList: McShift[];
    public report: McShiftReport;
    startDate: Date;
    endDate: Date;
    selectedAzs: McAzs;
    selectedShift: McShift;
    reportDialogVisible: boolean = false;
    submitAvailable: boolean = false;
    public onSubmit(){
        if(this.selectedAzs === undefined) {
            console.log('Не выбрана АЗС!');

        }
        console.log('startDate: ' + this.startDate);
        console.log('endDate: ' + this.endDate);
        console.log('selectedAzsId: ' + this.selectedAzs.id);
        this.getShiftList(this.selectedAzs.id, this.startDate, this.endDate);

    }

    validate(event){

    }

    onShiftSelect(event){
        console.log('Получение отчета');
        this.getShiftReport(this.selectedAzs.id, this.selectedShift.id);
    }


    getAzsList(){
        console.log('Получение списка АЗС из МК');
        this.mcService.getAzsList().subscribe(
            (azsListResponse: McAzs[]) => {
                this.azsList = azsListResponse;
                this.showSuccess('Данные по АЗС получены.');
            },
            error => {
                this.showError(error);
                console.log(error);
            }
        );
    }

    getShiftList(azsId: number, startDate: Date, endDate: Date){
        console.log('Получение списка смен по АЗС id= ' + azsId);
        this.mcService.getShiftsByDateRange(azsId, startDate.toISOString(), endDate.toISOString()).subscribe(
            (azsListResponse: McShift[]) => {
                this.shiftList = azsListResponse;
                this.showSuccess('Данные по сменам получены.');
            },
            error => {
                this.showError(error);
                console.log(error);
            }
        );
    }

    getShiftReport(azsId: number, shiftId: number){
        console.log('Получение отчета по АЗС id= ' + azsId + ' и смене id= ' + shiftId);
        this.mcService.getShiftReport(azsId, shiftId, shiftId).subscribe(
            (reportResponse: McShiftReport) => {
                this.report = reportResponse;
                this.showSuccess('Сменный отчет загружен');
                this.reportDialogVisible = true;
            },
            error => {
                this.showError(error);
                console.log(error);
            }
        );
    }


    showError(serviceMessage: string) {
        this.messageService.add({severity:'error', summary: 'Ошибка', detail:'Ошибка обращения к сервису: ' +
                serviceMessage});
    }

    showSuccess(serviceMessage: string) {
        this.messageService.add({severity:'success', summary: 'Получение', detail:serviceMessage});
    }
}