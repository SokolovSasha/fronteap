import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EtranComponent } from './etran.component';

describe('EtranComponent', () => {
  let component: EtranComponent;
  let fixture: ComponentFixture<EtranComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EtranComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EtranComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
