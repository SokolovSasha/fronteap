import { Component, OnInit } from '@angular/core';
import {MessageService} from "primeng/api";
import {EtranService} from "../../services/etran.service";
import {InvoiceRequest} from "../../model/etran/invoice-request.model";
import {NgForm} from "@angular/forms";
import {GetInvoiceModel} from "../../model/etran/get-invoice.model";
import {environment} from "../../../environments/environment";
import {DomSanitizer, SafeResourceUrl} from "@angular/platform-browser";

@Component({
  selector: 'app-etran',
  templateUrl: './etran.component.html',
  styleUrls: ['./etran.component.css'],
  providers: [MessageService]
})
export class EtranComponent implements OnInit {

  constructor(private messageService: MessageService, private etranService: EtranService,
              private sanitizer: DomSanitizer) {
      this._swaggerUrl = environment.etranUrl + '/webjars/swagger-ui/index.html?url='
          + environment.etranUrl + '/etran/api-doc';
      this.trustedSwaggerUrl = sanitizer.bypassSecurityTrustResourceUrl(this._swaggerUrl);
  }

  private _swaggerUrl:string;
  trustedSwaggerUrl: SafeResourceUrl;
  invoice: GetInvoiceModel;
  arrayObj: any;
  arrayed: [];


  ngOnInit() {
  }

  onSubmit(form: NgForm){
    console.log('Получаем данные из этрана!');
    let invoiceRequest = new InvoiceRequest();

    if(form.value && form.value.invoiceId !== undefined){
      let property = {value: form.value.invoiceId, valueAttribute: ""};
      console.log(form.value.invoiceId);
      invoiceRequest.invoiceID = property;
      // invoiceRequest.invoiceID.value = form.value.invoiceId;
    }
    if (form.value.invoiceNumber !== undefined) {
      let property = {value: form.value.invoiceId, valueAttribute: ""};
      console.log(form.value.invoiceNumber);
      invoiceRequest.invNumber = property;
      invoiceRequest.invNumber.value = form.value.invoiceNumber;

    }
    this.getInvoice(invoiceRequest);
  }


  private getInvoice(invoiceRequest: InvoiceRequest) {

      // this.invoice =
      //     {"invoiceID":{"value":"877523492"},"invUNP":{"value":"877523492"},"invDateCreate":{"value":"19.03.2019 06:46:14"},"invoiceStateID":{"value":"44"},"invoiceState":{"value":"Работа с документом окончена"},"invLastOper":{"value":"20.03.2019 19:52:54"},"invNeedForECP":{"value":"0"},"invECPSign":{"value":"1"},"invTypeID":{"value":"3"},"invTypeName":{"value":"Досылочная накладная"},"invBlankTypeID":{"value":"94"},"invBlankType":{"value":"94"},"invBlankTypeName":{"value":"Универсальный перевозочный документ на все виды отправок"},"invSenderID":{"value":"2518"},"invSenderName":{"value":"ЛОСИНООСТРОВСКАЯ"},"invSenderAddressID":{"value":"0"},"invSenderAddress":{"value":"ДС"},"invSenderTGNL":{"value":"6302"},"invFromCountryCode":{"value":"643"},"invFromCountryName":{"value":"РОССИЯ"},"invFromStationCode":{"value":"19540"},"invFromStationName":{"value":"ЛОСИНООСТРОВСКАЯ"},"invRecipID":{"value":"571174"},"invRecipOKPO":{"value":"47859907"},"invRecipName":{"value":"Акционерное общество \"Газпромнефть-Терминал\""},"invRecipAddressID":{"value":"0"},"invRecipAddress":{"value":"630099, НОВОСИБИРСКАЯ ОБЛАСТЬ, г.Новосибирск, ул.М.Горького - д.80, тел: (383) 218-94-85,(383) 218-96-18"},"invRecipTGNL":{"value":"6302"},"invToCountryCode":{"value":"643"},"invToCountryName":{"value":"РОССИЯ"},"invToStationCode":{"value":"23790"},"invToStationName":{"value":"ЛОБНЯ"},"invToLoadWay":{"value":"п/п Павельцевская нефтебаза  АО /Газпром"},"invSendSpeedID":{"value":"2"},"invSendSpeedName":{"value":"Грузовая"},"invSendKindID":{"value":"1"},"invSendKindName":{"value":"Повагонная"},"invPayPlaceID":{"value":"9"},"invPayPlaceName":{"value":"бесплатная перевозка"},"invPayFormID":{"value":"60"},"invPayFormName":{"value":"Досылка"},"invPlanCarTypeID":{"value":"70"},"invPlanCarTypeCode":{"value":"70"},"invPlanCarTypeName":{"value":"цистерны"},"invPlanCarCount":{"value":"1"},"invPlanCarOwnerTypeID":{"value":"1"},"invPlanCarOwnerTypeName":{"value":"Собственный"},"invLoadTypeID":{"value":"11"},"invLoadTypeName":{"value":"БЕЗ ЗАЧЕТА В ПОГРУЗКУ"},"invAnnounceValue":{"value":"0"},"invAVCurrencyID":{"value":"1"},"invDispKindID":{"value":"10"},"invRespPerson":{"value":"ЕГОРОВА"},"invDateExpire":{"value":"20.03.2019 00:00:00"},"invScaleTypeID":{"value":"14"},"invScaleTypeName":{"value":"Вагон. весы, в движении, Мах 150 т"},"invScalePersonID":{"value":"2"},"invScalePersonName":{"value":"Отправителем"},"invScalePrecision":{"value":".5"},"invCheckDepID":{"value":"0"},"invCheckDepName":{"value":"смешанная"},"invDeplPerson":{"value":"Оператор товарный Сидоров В.С."},"invDepNormDocID":{"value":"1"},"invDepNormDocName":{"value":"Правилам перевозок"},"invDateLoad":{"value":"14.03.2019 00:00:00"},"invFactDateToLoad":{"value":"19.03.2019 07:03:52"},"invFactDateToLoadLocal":{"value":"19.03.2019 07:03:52"},"invFactDateAccept":{"value":"19.03.2019 07:04:14"},"invFIOAccept":{"value":"ЕГОРОВА"},
      //  "invFreight":[{"freightCode":{"value":"21105"},"freightName":{"value":"БЕНЗИН МОТОРНЫЙ (АВТОМОБИЛЬНЫЙ) НЕЭТИЛИРОВАННЫЙ"},"freightExactName":{"value":"Бензин АИ-92-К5"},"freightAdditional":{"value":"Бензин моторный автомобильны й н/эт /Бензин неэтилированн ый марки АИ-92-К5 по ГОСТ325 13-2013/Автомобильный бензин экологического класса К5 ма рки АИ-92-К5/"},"freightPackTypeID":{"value":"24"},"freightPackTypeName":{"value":"Налив"},"freightWeight":{"value":"48700"},"freightRealWeight":{"value":"48700"},"freightDangerSignID":{"value":"2"},"freightDangerSignName":{"value":"Опасный груз"},"freightDangerID":{"value":"15077"},"freightDangerName":{"value":"БЕНЗИН МОТОРНЫЙ"},"freightAccidentCardID":{"value":"98"},"freightAccidentCard":{"value":"305"}}],"invDistance":[{"distCountryCode":{"value":"643"},"distCountryName":{"value":"РОССИЯ"},"distStationCountryId":{"value":"178"},"distStationCode":{"value":"19540"},"distStationName":{"value":"ЛОСИНООСТРОВСКАЯ"},"distTrackTypeID":{"value":"1"},"distTrackTypeName":{"value":"Широкая"},"distTranspTypeID":{"value":"1"},"distTranspTypeName":{"value":"Ж.Д."},"distMinWay":{"value":"167"},"distRecipID":{"value":"2518"},"distRecipName":{"value":"ЛОСИНООСТРОВСКАЯ"},"distRecipAddressID":{"value":"0"},"distRecipAddress":{"value":"ДС"},"distRecipTGNL":{"value":"6302"},"distSign":{"value":"10101"},"distCarrierID":{"value":"1"},"distPowerKind":{"value":"3"}},{"distCountryCode":{"value":"643"},"distCountryName":{"value":"РОССИЯ"},"distStationCountryId":{"value":"178"},"distStationCode":{"value":"23790"},"distStationName":{"value":"ЛОБНЯ"},"distLoadWay":{"value":"п/п Павельцевская нефтебаза  АО /Газпром"},"distTrackTypeID":{"value":"1"},"distTrackTypeName":{"value":"Широкая"},"distTranspTypeID":{"value":"1"},"distTranspTypeName":{"value":"Ж.Д."},"distMinWay":{"value":"0"},"distRecipID":{"value":"571174"},"distRecipOKPO":{"value":"47859907"},"distRecipName":{"value":"Акционерное общество \"Газпромнефть-Терминал\""},"distRecipAddressID":{"value":"0"},"distRecipAddress":{"value":"630099, НОВОСИБИРСКАЯ ОБЛАСТЬ, г.Новосибирск, ул.М.Горького - д.80, тел: (383) 218-94-85,(383) 218-96-18"},"distRecipTGNL":{"value":"6302"},"distSign":{"value":"101010"},"distCarrierID":{"value":"1"},"distPowerKind":{"value":"3"}}],"invSPC":[{"spcTranspClauseID":{"value":"1"},"spcTranspClauseDesc":{"value":"Другие отметки отправителя Росс., СМГС, Росс-Фин. г.4 и ЦИМ г. 7 отметка 16"},"spcCustomText":{"value":"Для АО /ГАЛА-ФОРМ/  /Ресурс ПА О /НК/Роснефть/. тел. /383/218 -94-84 03.04.2019"}},{"spcTranspClauseID":{"value":"9"},"spcTranspClauseDesc":{"value":"Легко воспламеняется"}},{"spcTranspClauseID":{"value":"1326"},"spcTranspClauseDesc":{"value":"Прикрытие 0-0-1"}},{"spcTranspClauseID":{"value":"1184"},"spcTranspClauseDesc":{"value":"Вагон (котел) и арматура исправны и соответствуют установленным требованиям"}},{"spcTranspClauseID":{"value":"882"},"spcTranspClauseDesc":{"value":"Другие в верхней части накладной"},"spcCustomText":{"value":"Возврат порожнего вагона по указанию собственника подвижного состава"}},{"spcTranspClauseID":{"value":"398"},"spcTranspClauseDesc":{"value":"Охрана"},"spcCustomText":{"value":"9"}}],"invDOC":[{"docTypeID":{"value":"853"},"docNumber":{"value":"№9/НОР-3/1107/ЮТС/819/2012.от.10,01,2012.г,"}},{"docTypeID":{"value":"794"},"docNumber":{"value":"07936 от 11.03.2019"}},{"docTypeID":{"value":"872"}}],"invCar":[{"carTypeID":{"value":"70"},"carTypeCode":{"value":"70"},"carTypeName":{"value":"цистерны"},"carNumber":{"value":"51734341"},"carOrder":{"value":"1"},"carOwnerCountryCode":{"value":"643"},"carOwnerCountryName":{"value":"РОССИЯ"},"carOwnerTypeID":{"value":"1"},"carOwnerTypeName":{"value":"Собственный"},"carOwnerID":{"value":"68140"},"carOwnerOKPO":{"value":"52142548"},"carOwnerName":{"value":"ООО \"БалтТрансСервис\""},"carTonnage":{"value":"66"},"carAxles":{"value":"4"},"carVolume":{"value":"74"},"carWeightDep":{"value":"270"},"carWeightDepReal":{"value":"266"},"carWeightGross":{"value":"75300"},"carWeightNet":{"value":"48700"},"carGuideCount":{"value":"0"},"carLiquidTemperature":{"value":"0"},"carLiquidHeight":{"value":"0"},"carLiquidDensity":{"value":"0"},"carLiquidVolume":{"value":"74"},"carTankType":{"value":"85"},"carRolls":{"value":"1"},"carConnectCode":{"value":"0"},"carIsCover":{"value":"0"},"carLength":{"value":"12.02"},"carCSL":{"sealTypeID":{"value":"109"},"sealTypeName":{"value":"ТП50"},"sealMarks":{"value":"РЖДА2487714"},"sealOwnerTypeID":{"value":"2"},"sealOwnerTypeName":{"value":"отправитель"},"sealRailwayID":{"value":"7"},"sealRailwayCode":{"value":"17"},"sealRailwayName":{"value":"МОСКОВСКАЯ"}}}],"wayCLS":[{"wayTranspClauseID":{"value":"1320"},"wayTranspClauseName":{"value":"Расстояние перевозки с учетом п. 20 приказа № 245"},"wayCustomText":{"value":"70"},"wayRwPanish":{"value":"0"}}],"invNumber":{"value":"ЭЖ683862"},"invUniqueNumber":{"value":"877523492"},"invSignRouteNumCirc":{"value":"0"},"invGoodsCashier":{"value":"Глубокова Олеся Николаевна"},"invGoodsCashierPost":{"value":"Агент сист фирм тр об (СФТО) I категории"},"invDateReady":{"value":"19.03.2019 07:04:14"},"invDateReadyLocal":{"value":"19.03.2019 07:04:14"},"invDateDeparture":{"value":"19.03.2019 07:05:09"},"invDateArrive":{"value":"20.03.2019 05:58:00"},"invDateArriveLocal":{"value":"20.03.2019 05:58:00"},"invDateDelivery":{"value":"20.03.2019 09:02:02"},"invDateDeliveryLocal":{"value":"20.03.2019 09:02:02"},"invDateRaskrEl":{"value":"20.03.2019 09:13:23"},"invDateNotification":{"value":"20.03.2019 06:39:43"},"invNotification":{"value":"Информирование по e-mail gpnl-dc@gazprom-neft.ru"},"invNum410":{"value":"82622"},"invKPZ":{"value":"195406"},"invParentID":{"value":"876185371"},"invParentNumber":{"value":"ЭЖ454359"}};

    this.etranService.getInvoice(invoiceRequest).subscribe(
        (reportResponse: GetInvoiceModel) => {
            this.invoice = reportResponse;
        },
        error => {
          this.showError(error);
          console.log(error.toString());
        },
        () => {
          console.log('Данные получены');
          this.showSuccess('Данные из Этран получены')
        }
    );
  }

  private showError(serviceMessage: string){
    this.messageService.add({severity:'error', summary: 'Ошибка', detail:'Ошибка обращения к сервису: ' +
          serviceMessage});
  }

  showSuccess(serviceMessage: string) {
    this.messageService.add({severity:'success', summary: 'Получение', detail:serviceMessage});
  }

}
