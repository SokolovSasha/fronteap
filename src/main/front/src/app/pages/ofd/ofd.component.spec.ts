import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OfdComponent } from './ofd.component';

describe('OfdComponent', () => {
  let component: OfdComponent;
  let fixture: ComponentFixture<OfdComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OfdComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OfdComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
