import { Component, OnInit } from '@angular/core';
import {MessageService} from "primeng/api";
import {OfdService} from "../../services/ofd.service";
import {KktsListResponse} from "../../model/ofd/KktsListResponse";
import {NgForm} from "@angular/forms";
import {DetailedReceiptsListWithStatResponse} from "../../model/ofd/DetailedReceiptsListWithStatResponse";
import {WidgetStatInfo} from "../../model/ofd/WidgetStatInfo";
import {OfdData} from "../../model/ofd/OfdData";
import {OfdItem} from "../../model/ofd/OfdItem";
import {DomSanitizer, SafeResourceUrl} from "@angular/platform-browser";
import {environment} from "../../../environments/environment";

@Component({
  selector: 'app-ofd',
  templateUrl: './ofd.component.html',
  styleUrls: ['./ofd.component.css'],
  providers: [MessageService]
})
export class OfdComponent implements OnInit {

  constructor(private messageService: MessageService, private ofdService: OfdService, private sanitizer: DomSanitizer) {

    const swaggerUrl = environment.ofdUrl + '/webjars/swagger-ui/index.html?url='
        + environment.ofdUrl + '/ofd/api-doc';
    this.trustedSwaggerUrl = sanitizer.bypassSecurityTrustResourceUrl(swaggerUrl);
  }

  trustedSwaggerUrl: SafeResourceUrl;

  private example: any;

  ngOnInit() {
    // this.inn = 1646014874;
    this.inn = 7709359770;
    this.kkt = '0000498268002903';
    // this.inn = 7709359770;
    this.example = {"Status":"Success","Elapsed":"00:00:06.644","Statistics":[{"StatisticsMarker":"CASH","Name":"Total cash","Value":"278900"},{"StatisticsMarker":"ECASH","Name":"Total eCash","Value":"0"},{"StatisticsMarker":"AZS","Name":"г. Елабуга, ул. Окружное Шоссе, д. 37","Value":"278900"},{"StatisticsMarker":"ITEM","Name":"Молоко \"Домик в деревне\" ультрапастеризо","Value":"259150"},{"StatisticsMarker":"OPERATOR","Name":"Михайлов Александр","Value":"278900"}],"Data":[{"Tag":3,"User":"ООО \"Оптовик\"","UserInn":"1646014874","Number":1,"DateTime":"2019-02-05T08:56:00","ShiftNumber":35,"OperationType":"1","TaxationType":1,"Operator":"Михайлов Александр","KKT_RegNumber":"0000000002008480    ","FN_FactoryNumber":"9999078900011482","Items":[{"Name":"Молоко \"Домик в деревне\" ультрапастеризо","Price":4300,"Quantity":"1.000","Total":4300,"CalculationMethod":4,"NDS_Rate":2,"NDS_Summ":391}],"RetailPlaceAddress":"г. Елабуга, ул. Окружное Шоссе, д. 37","Sender_Address":"sender@mail.ru","Nds18_TotalSumm":0,"Nds10_TotalSumm":391,"NdsNA_TotalSumm":0,"Nds18_CalculatedTotalSumm":0,"Nds10_CalculatedTotalSumm":0,"Nds00_TotalSumm":0,"Amount_Total":4300,"Amount_Cash":4300,"Amount_ECash":0,"Document_Number":247,"FiscalSign":"MQR1/KG+","DecimalFiscalSign":"1979490750","Format_Version":2,"Amount_Advance":0,"Amount_Loan":0,"Amount_Granting":0,"TaxAuthority_Site":"nalog.ru","Calculation_Place":"Магазин \"ЭССЕН\"","Operator_INN":""},{"Tag":3,"User":"ООО \"Оптовик\"","UserInn":"1646014874","Number":2,"DateTime":"2019-02-05T09:01:00","ShiftNumber":35,"OperationType":"1","TaxationType":1,"Operator":"Михайлов Александр","KKT_RegNumber":"0000000002008480    ","FN_FactoryNumber":"9999078900011482","Items":[{"Name":"Молоко \"Домик в деревне\" ультрапастеризо","Price":4300,"Quantity":"1.000","Total":4300,"CalculationMethod":4,"NDS_Rate":2,"NDS_Summ":391}],"RetailPlaceAddress":"г. Елабуга, ул. Окружное Шоссе, д. 37","Sender_Address":"sender@mail.ru","Nds18_TotalSumm":0,"Nds10_TotalSumm":391,"NdsNA_TotalSumm":0,"Nds18_CalculatedTotalSumm":0,"Nds10_CalculatedTotalSumm":0,"Nds00_TotalSumm":0,"Amount_Total":4300,"Amount_Cash":4300,"Amount_ECash":0,"Document_Number":248,"FiscalSign":"MQRiavrl","DecimalFiscalSign":"1651178213","Format_Version":2,"Amount_Advance":0,"Amount_Loan":0,"Amount_Granting":0,"TaxAuthority_Site":"nalog.ru","Calculation_Place":"Магазин \"ЭССЕН\"","Operator_INN":""},{"Tag":3,"User":"ООО \"Оптовик\"","UserInn":"1646014874","Number":3,"DateTime":"2019-02-05T09:36:00","ShiftNumber":35,"OperationType":"1","TaxationType":1,"Operator":"Михайлов Александр","KKT_RegNumber":"0000000002008480    ","FN_FactoryNumber":"9999078900011482","Items":[{"Name":"Молоко \"Домик в деревне\" ультрапастеризо","Price":2550,"Quantity":"1.000","Total":2550,"CalculationMethod":4,"NDS_Rate":2,"NDS_Summ":232}],"RetailPlaceAddress":"г. Елабуга, ул. Окружное Шоссе, д. 37","Sender_Address":"sender@mail.ru","Nds18_TotalSumm":0,"Nds10_TotalSumm":232,"NdsNA_TotalSumm":0,"Nds18_CalculatedTotalSumm":0,"Nds10_CalculatedTotalSumm":0,"Nds00_TotalSumm":0,"Amount_Total":2550,"Amount_Cash":2550,"Amount_ECash":0,"Document_Number":249,"FiscalSign":"MQSp2Wfh","DecimalFiscalSign":"2849597409","Format_Version":2,"Amount_Advance":0,"Amount_Loan":0,"Amount_Granting":0,"TaxAuthority_Site":"nalog.ru","Calculation_Place":"Магазин \"ЭССЕН\"","Operator_INN":""},{"Tag":3,"User":"ООО \"Оптовик\"","UserInn":"1646014874","Number":4,"DateTime":"2019-02-05T09:40:00","ShiftNumber":35,"OperationType":"1","TaxationType":1,"Operator":"Михайлов Александр","KKT_RegNumber":"0000000002008480    ","FN_FactoryNumber":"9999078900011482","Items":[{"Name":"Кофе \"Якобс\" монарх растворимый сублимир","Price":14750,"Quantity":"1.000","Total":14750,"CalculationMethod":4,"NDS_Rate":1,"NDS_Summ":2458}],"RetailPlaceAddress":"г. Елабуга, ул. Окружное Шоссе, д. 37","Sender_Address":"sender@mail.ru","Nds18_TotalSumm":2458,"Nds10_TotalSumm":0,"NdsNA_TotalSumm":0,"Nds18_CalculatedTotalSumm":0,"Nds10_CalculatedTotalSumm":0,"Nds00_TotalSumm":0,"Amount_Total":14750,"Amount_Cash":14750,"Amount_ECash":0,"Document_Number":250,"FiscalSign":"MQQx4QCV","DecimalFiscalSign":"836829333","Format_Version":2,"Amount_Advance":0,"Amount_Loan":0,"Amount_Granting":0,"TaxAuthority_Site":"nalog.ru","Calculation_Place":"Магазин \"ЭССЕН\"","Operator_INN":""},{"Tag":3,"User":"ООО \"Оптовик\"","UserInn":"1646014874","Number":5,"DateTime":"2019-02-05T09:41:00","ShiftNumber":35,"OperationType":"1","TaxationType":1,"Operator":"Михайлов Александр","KKT_RegNumber":"0000000002008480    ","FN_FactoryNumber":"9999078900011482","Items":[{"Name":"Молоко \"Домик в деревне\" ультрапастеризо","Price":6850,"Quantity":"1.000","Total":6850,"CalculationMethod":4,"NDS_Rate":2,"NDS_Summ":623}],"RetailPlaceAddress":"г. Елабуга, ул. Окружное Шоссе, д. 37","Sender_Address":"sender@mail.ru","Nds18_TotalSumm":0,"Nds10_TotalSumm":623,"NdsNA_TotalSumm":0,"Nds18_CalculatedTotalSumm":0,"Nds10_CalculatedTotalSumm":0,"Nds00_TotalSumm":0,"Amount_Total":6850,"Amount_Cash":6850,"Amount_ECash":0,"Document_Number":251,"FiscalSign":"MQSvxqV1","DecimalFiscalSign":"2949031285","Format_Version":2,"Amount_Advance":0,"Amount_Loan":0,"Amount_Granting":0,"TaxAuthority_Site":"nalog.ru","Calculation_Place":"Магазин \"ЭССЕН\"","Operator_INN":""},{"Tag":3,"User":"ООО \"Оптовик\"","UserInn":"1646014874","Number":6,"DateTime":"2019-02-05T14:39:00","ShiftNumber":35,"OperationType":"1","TaxationType":1,"Operator":"Михайлов Александр","KKT_RegNumber":"0000000002008480    ","FN_FactoryNumber":"9999078900011482","Items":[{"Name":"Молоко \"Домик в деревне\" ультрапастеризо","Price":6890,"Quantity":"10.000","Total":68900,"CalculationMethod":4,"NDS_Rate":2,"NDS_Summ":6264},{"Name":"Набор для выращивания растений \"Чудо Ого","Price":5000,"Quantity":"1.000","Total":5000,"CalculationMethod":4,"NDS_Rate":1,"NDS_Summ":833}],"RetailPlaceAddress":"г. Елабуга, ул. Окружное Шоссе, д. 37","Sender_Address":"sender@mail.ru","Nds18_TotalSumm":833,"Nds10_TotalSumm":6264,"NdsNA_TotalSumm":0,"Nds18_CalculatedTotalSumm":0,"Nds10_CalculatedTotalSumm":0,"Nds00_TotalSumm":0,"Amount_Total":73900,"Amount_Cash":73900,"Amount_ECash":0,"Document_Number":252,"FiscalSign":"MQTSsqWa","DecimalFiscalSign":"3534923162","Format_Version":2,"Amount_Advance":0,"Amount_Loan":0,"Amount_Granting":0,"TaxAuthority_Site":"nalog.ru","Calculation_Place":"Магазин \"ЭССЕН\"","Operator_INN":""},{"Tag":3,"User":"ООО \"Оптовик\"","UserInn":"1646014874","Number":7,"DateTime":"2019-02-05T15:15:00","ShiftNumber":35,"OperationType":"1","TaxationType":1,"Operator":"Михайлов Александр","KKT_RegNumber":"0000000002008480    ","FN_FactoryNumber":"9999078900011482","Items":[{"Name":"Молоко \"Домик в деревне\" ультрапастеризо","Price":6890,"Quantity":"25.000","Total":172250,"CalculationMethod":4,"NDS_Rate":2,"NDS_Summ":15659}],"RetailPlaceAddress":"г. Елабуга, ул. Окружное Шоссе, д. 37","Sender_Address":"sender@mail.ru","Nds18_TotalSumm":0,"Nds10_TotalSumm":15659,"NdsNA_TotalSumm":0,"Nds18_CalculatedTotalSumm":0,"Nds10_CalculatedTotalSumm":0,"Nds00_TotalSumm":0,"Amount_Total":172250,"Amount_Cash":172250,"Amount_ECash":0,"Document_Number":253,"FiscalSign":"MQR3J7vJ","DecimalFiscalSign":"1999092681","Format_Version":2,"Amount_Advance":0,"Amount_Loan":0,"Amount_Granting":0,"TaxAuthority_Site":"nalog.ru","Calculation_Place":"Магазин \"ЭССЕН\"","Operator_INN":""}]};
  }

  kktsList: KktsListResponse;
  statistics: DetailedReceiptsListWithStatResponse;

  private azsStat: WidgetStatInfo;
  private cashStat: WidgetStatInfo;
  private operatorStat: WidgetStatInfo;
  private itemStat: WidgetStatInfo;
  private ecashStat: WidgetStatInfo;

  private selectedData: OfdData;
  private itemViewEnabled = false;
  private detailedDataViewEnabled = false;

  private itemsList: OfdItem[];
  private dataDetails: OfdData;

  private inn: number;
  private kkt: string;

  onData(data: OfdData) {
    console.log(data.User);
    this.dataDetails = data;
    this.detailedDataViewEnabled = true;
  }

  onDataItems(items: OfdItem[]) {
    this.itemsList = items;
    this.itemViewEnabled = true;
  }

  onSubmit(form: NgForm){
    console.log('Получаем данные из ОФД!');
    this.getStatistic(form.value.inn, form.value.kkt, 'all', form.value.dateFrom, form.value.dateTo);
  }

  onAuth(){
    if (!this.ofdService.isAuthenticated()){
      this.ofdService.authorise();
      console.log('Авторизовались!')
    };
  }

  onLogout(){
    this.ofdService.logout();
  }



  /**
   * Получение списка ККТС
   * @param inn - ИНН.
   */
  public getKktsList(inn: string){

    if (this.ofdService.isAuthenticated()) {
      this.ofdService.getKkts(inn).subscribe(
          (response: KktsListResponse) => {
                this.kktsList = response;
                this.showSuccess();
          },
          error  => {
            console.log('Ошибка обращения к сервису авторизации!');
            console.log(error);
            this.showError(error.valueOf());
          }
      )
    }
  }

  /**
   * Получение статистики.
   * @param inn - ИНН
   * @param marker - маркер для фильтра
   * @param dateFrom дата начала
   * @param dateTo - дата окончания
   */
  public getStatistic(inn: string, kkt: string, marker: string, dateFrom: Date, dateTo: Date, ) {
    if (this.ofdService.isAuthenticated()) {
      this.ofdService.getReceiptsStatistic(inn, kkt, marker, dateFrom, dateTo).subscribe(
          (response: DetailedReceiptsListWithStatResponse) => {
            if(response) {
              this.processStatResponse(response);
              this.statistics = response;
            }
          },
          error  => {
            console.log('Ошибка обращения к сервису авторизации!');
            console.log(error);
            this.showError(error.valueOf());
          },
          () =>{
            this.showSuccess();
          }
      )
    }
  }

  test(){
    this.statistics = this.example;
    this.processStatResponse(this.example)
  }

  processStatResponse(response: DetailedReceiptsListWithStatResponse){
    let stats = response.Statistics;

    let azs = stats.find((stats) => {return stats.StatisticsMarker === 'AZS'});
    this.azsStat = new WidgetStatInfo();
    this.azsStat.name = azs.Name;
    this.azsStat.value = azs.Value/100 + ' руб. ';

    let cash = stats.find((stats) => {return stats.StatisticsMarker == 'CASH'});
    this.cashStat = new WidgetStatInfo();
    this.cashStat.name = 'Оплата наличными';
    this.cashStat.value = cash.Value/100 + ' руб. ';

    let ecash = stats.find((stats) => {return stats.StatisticsMarker == 'ECASH'});
    this.ecashStat = new WidgetStatInfo();
    this.ecashStat.name = 'Оплата картой';
    this.ecashStat.value = ecash.Value/100 + ' руб. ';

    let operator = stats.find((stats) => {return stats.StatisticsMarker == 'OPERATOR'});
    this.operatorStat = new WidgetStatInfo();
    this.operatorStat.name = operator.Name;
    this.operatorStat.value = operator.Value/100 + ' руб. ';

    let item = stats.find((stats) => {return stats.StatisticsMarker == 'ITEM'});
    this.itemStat = new WidgetStatInfo();
    this.itemStat.name = item.Name;
    this.itemStat.value = item.Value/100 + ' руб. ';



  }



  //TODO: make shared super component
  showWarn(serviceMessage: string) {
    this.messageService.add({severity:'warn', summary: 'Ошибка получения данных', detail:'Данные ещё не обработаны: ' +
          serviceMessage});
  }
  showSuccess() {
    this.messageService.add({severity:'success', summary: 'Получение', detail:'Данные получены!'});
  }
  showError(serviceMessage: string) {
    this.messageService.add({severity:'error', summary: 'Ошибка', detail:'Ошибка обращения к сервису: ' +
          serviceMessage});
  }

}
