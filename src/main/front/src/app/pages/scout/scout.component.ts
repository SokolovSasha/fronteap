import {Component, OnInit} from '@angular/core';
import {Unit} from '../../model/unit.model';
import {FuelStatistic} from '../../model/fuelstatistic.model';
import {ScoutService} from '../../services/scout.service';
import {OdoStatistic} from '../../model/odo_statistic.model';
import {UnitPick} from '../../model/unit-pick.model';
import {DomSanitizer, SafeResourceUrl} from "@angular/platform-browser";
import {environment} from "../../../environments/environment";
import {TracksModel} from "../../model/scout/tracks-model";
import {MessageService} from "primeng/api";

@Component({
  selector: 'app-scout',
  templateUrl: './scout.component.html',
  styleUrls: ['./scout.component.css'],
  providers: [MessageService]
})
export class ScoutComponent implements OnInit {
  title = 'front';
  public units: Unit[];
  public fuelStatistic: FuelStatistic;
  public odoStatistic: OdoStatistic;
  public tracksStatistic: TracksModel;
  selectedUnit: Unit;
  public ids: number[];
  public daylyFuelChecked: boolean;
  public daylyOdoChecked: boolean;
  public daylyTracksChecked: boolean;
  startDate: Date;
  endDate: Date;
  unisel: UnitPick;

  constructor (private scoutService: ScoutService, private sanitizer: DomSanitizer, private messageService: MessageService) {
    this.units  = [{id: 51005}, {id: 51006}, {id: 51007}, {id: 51008}, {id: 51010}, {id: 51011}, {id: 51164}];
    this._swaggerUrl = environment.scoutUrl + '/webjars/swagger-ui/index.html?url='
        + environment.scoutUrl + '/scout/api-doc';
    this.trustedSwaggerUrl = sanitizer.bypassSecurityTrustResourceUrl(this._swaggerUrl);
  }

  private _swaggerUrl:string;
  trustedSwaggerUrl: SafeResourceUrl;

  ngOnInit(): void {

  }

  tracks(){
    console.log('Получение даннных о местоположении');
    this.scoutService.getTracks(this.selectedUnit, this.startDate, this.endDate, this.daylyTracksChecked).subscribe(
        (response: TracksModel) => {
          this.tracksStatistic = response;
        },
        error => {
          console.log('Ошибка обращения к сервису!');
          console.log(error);
        }, () => {
          this.showSuccess();
        }
    );
  }

  fuel() {
    console.log('Получение топлива');
    this.scoutService.getFuel(this.selectedUnit, this.startDate, this.endDate, this.daylyFuelChecked).subscribe(
      (fuelStatistic: FuelStatistic) => {
        this.fuelStatistic = fuelStatistic;
      },
      error => {
        console.log('факеншит!');
        console.log(error);
      }, () => {
          this.showSuccess();
        }
    );
  }
  odometer() {
    console.log('Статистика по одометру');
    this.scoutService.getOdometer(this.selectedUnit, this.startDate, this.endDate, this.daylyOdoChecked).subscribe(
      (odoStatistic: OdoStatistic) => {
        this.odoStatistic = odoStatistic;
      },
      error => console.log(error)
    ), () => {
      this.showSuccess();
    };
  }
  handleFuelDaylyChange(e) {
    this.fuel();
  }
  handleOdoDaylyChange(e) {
    this.odometer();
  }
  selectorChanged(selector: UnitPick) {
    console.log('Готово!');
    this.startDate = selector.startDate;
    this.endDate = selector.endDate;
    this.selectedUnit = selector.selectedUnit;
  }

  //TODO: make shared super component
  showWarn(serviceMessage: string) {
    this.messageService.add({severity:'warn', summary: 'Ошибка получения данных', detail:'Данные ещё не обработаны: ' +
          serviceMessage});
  }
  showSuccess() {
    this.messageService.add({severity:'success', summary: 'Получение', detail:'Данные получены!'});
  }
  showError(serviceMessage: string) {
    this.messageService.add({severity:'error', summary: 'Ошибка', detail:'Ошибка обращения к сервису: ' +
          serviceMessage});
  }
}
