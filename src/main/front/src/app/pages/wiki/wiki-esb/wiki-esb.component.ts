import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-wiki-esb',
  templateUrl: './wiki-esb.component.html',
  styleUrls: ['./wiki-esb.component.css']
})
export class WikiEsbComponent implements OnInit {

  constructor(
      private route: ActivatedRoute,
      private router: Router
  ) {}

  ngOnInit() {
  }

}
