import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WikiEsbComponent } from './wiki-esb.component';

describe('WikiEsbComponent', () => {
  let component: WikiEsbComponent;
  let fixture: ComponentFixture<WikiEsbComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WikiEsbComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WikiEsbComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
