import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WikiStackComponent } from './wiki-stack.component';

describe('WikiStackComponent', () => {
  let component: WikiStackComponent;
  let fixture: ComponentFixture<WikiStackComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WikiStackComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WikiStackComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
