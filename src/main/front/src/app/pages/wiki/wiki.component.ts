import {Component, OnInit} from "@angular/core";
import {MenuItem, MessageService} from "primeng/api";
import {DomSanitizer} from "@angular/platform-browser";
import {environment} from "../../../environments/environment";


@Component({
    selector: 'app-wiki',
    templateUrl: './wiki.component.html',
    styleUrls: ['./wiki.component.css'],
    providers: [MessageService]
})
export class WikiComponent implements OnInit{
    constructor (private messageService: MessageService,
                 private sanitizer: DomSanitizer) {

    }
    items: MenuItem[];

    ngOnInit(): void {

    }

}