import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WikiMetricsComponent } from './wiki-metrics.component';

describe('WikiMetricsComponent', () => {
  let component: WikiMetricsComponent;
  let fixture: ComponentFixture<WikiMetricsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WikiMetricsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WikiMetricsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
