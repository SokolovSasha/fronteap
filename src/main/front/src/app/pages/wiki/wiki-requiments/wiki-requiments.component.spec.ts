import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WikiRequimentsComponent } from './wiki-requiments.component';

describe('WikiRequimentsComponent', () => {
  let component: WikiRequimentsComponent;
  let fixture: ComponentFixture<WikiRequimentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WikiRequimentsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WikiRequimentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
