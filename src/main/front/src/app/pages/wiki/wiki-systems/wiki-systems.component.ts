import { Component, OnInit } from '@angular/core';
import {EsbSystemModel} from "../model/esb-system.model";
import {MenuItem} from "primeng/api";

@Component({
  selector: 'app-wiki-systems',
  templateUrl: './wiki-systems.component.html',
  styleUrls: ['./wiki-systems.component.css']
})
export class WikiSystemsComponent implements OnInit {

  systems: EsbSystemModel[];
  items: MenuItem[];
  active: number = 2;
  statusEnum: StatusEnum;
  constructor() { }

  ngOnInit() {
    this.systems = [
      {id: 1, name: "АСУ АЗС и DataLake (1 ОЦ)", description: "Интеграция между мобильной картой и озером данных", status: "Релиз", stage: 5, expectedTime: "1-квартал 2019"},
      {id: 2, name: "CRM и личный кабинет", description: "Интеграция между CRM Siebel и личным кабинетом (корпоративные продажи) в CRM систему", status: "В разработке", stage: 2, expectedTime: "2-квартал 2019"},
      {id: 3, name: "Скаут", description: "Интеграция с системой контроля транспорта (бензовозы)", status: "Разработана", stage: 3, expectedTime: "2-квартал 2019"},
      {id: 4, name: "Авторизация", description: "Механизм авторизации пользователей из DMZ  в КСПД", status: "В разработке", stage: 2, expectedTime: "3-квартал 2019"},
      {id: 5, name: "Контур.EDI", description: "добавить описание", status: "Разработана", stage: 3, expectedTime: "2-квартал 2019"},
      {id: 6, name: "Namos", description: "добавить описание", status: "Разработана", stage: 3, expectedTime: "2-квартал 2019"},
      {id: 7, name: "b2b КП", description: "добавить описание", status: "Бэклог", stage: 0, expectedTime: "2-квартал 2019"},
      {id: 8, name: "Этран", description: "добавить описание", status: "Разработана", stage: 3, expectedTime: "3-квартал 2019"},
      {id: 9, name: "МЦ ТЛК", description: "добавить описание", status: "Бэклог", stage: 0, expectedTime: "3-квартал 2019"},
      {id: 10, name: "SAP PI", description: "добавить описание", status: "Бэклог", stage: 0, expectedTime: "3-квартал 2019"},
      {id: 11, name: "Лк HR", description: "добавить описание", status: "Бэклог", stage: 0, expectedTime: "3-квартал 2019"},
      {id: 12, name: "G-Manager", description: "добавить описание", status: "Бэклог", stage: 0, expectedTime: "3-квартал 2019"},
      {id: 13, name: "Портал проектов ДРП", description: "", status: "В разработке", stage: 2, expectedTime: "3-квартал 2019"},
      {id: 14, name: "ЕАСКУ", description: "добавить описание", status: "Бэклог", stage: 0, expectedTime: "3-квартал 2019"},
      {id: 15, name: "Автозаказ", description: "добавить описание", status: "Бэклог", stage: 0, expectedTime: "4-квартал 2019"},
      {id: 16, name: "АСКУ НБ", description: "добавить описание", status: "Бэклог", stage: 0, expectedTime: "4-квартал 2019"},
      {id: 17, name: "Way4", description: "добавить описание", status: "Бэклог", stage: 0, expectedTime: "4-квартал 2019"},
      {id: 18, name: "Платформа ЭДО", description: "добавить описание", status: "В разработке", stage: 2, expectedTime: "4-квартал 2019"},
      {id: 19, name: "TBD", description: "добавить описание", status: "Бэклог", stage: 0, expectedTime: "4-квартал 2019"},
    ];

    this.items = [
      {label: 'Бэклог'}, //0
      {label: 'Аналитика'}, //1
      {label: 'В разработке'}, //2
      {label: 'Тестирование'}, //3
      {label: 'Разработана'}, //4
      {label: 'Релиз'}, //5
    ];
  }

}
