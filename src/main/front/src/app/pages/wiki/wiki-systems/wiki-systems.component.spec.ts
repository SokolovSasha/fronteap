import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WikiSystemsComponent } from './wiki-systems.component';

describe('WikiSystemsComponent', () => {
  let component: WikiSystemsComponent;
  let fixture: ComponentFixture<WikiSystemsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WikiSystemsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WikiSystemsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
