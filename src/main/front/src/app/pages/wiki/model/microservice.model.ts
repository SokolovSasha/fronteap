export class Microservice {
    public id: number;
    public name: string;
    public description: string;
    public stage: number;
    public status: string;
    public type: string;
    public urls: {
        id: number,
        stage: string,
        url: string,
        port: number
    }[];
    public resources: {
        id: number,
        stage: string,
        cores: number,
        memory: number,
        pv: number
    }[];

}