export class ServerEsb {

    constructor(id: number, name: string, address: string, description: string, stage: string) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.description = description;
        this.stage = stage;
    }

    id: number;
    name: string;
    address: string;
    description: string;
    stage: string;

}