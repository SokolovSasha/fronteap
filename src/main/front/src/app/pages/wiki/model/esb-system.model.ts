
export class EsbSystemModel {
    public id: number;
    public name: string;
    public description: string;
    public status: any;
    public stage: number;
    public expectedTime: string;
}