import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WikiMicroservicesComponent } from './wiki-microservices.component';

describe('WikiMicroservicesComponent', () => {
  let component: WikiMicroservicesComponent;
  let fixture: ComponentFixture<WikiMicroservicesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WikiMicroservicesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WikiMicroservicesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
