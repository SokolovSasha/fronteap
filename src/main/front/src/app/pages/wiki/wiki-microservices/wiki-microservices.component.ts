import { Component, OnInit } from '@angular/core';
import {Microservice} from "../model/microservice.model";
import {MenuItem} from "primeng/api";

@Component({
  selector: 'app-wiki-microservices',
  templateUrl: './wiki-microservices.component.html',
  styleUrls: ['./wiki-microservices.component.css']
})
export class WikiMicroservicesComponent implements OnInit {

  constructor() { }
  microservices: Microservice[];
  items: MenuItem[];
  showEditorForm: boolean = false;
  blockEditorForm: boolean = false;

  selectedService: Microservice;

  ngOnInit() {
    this.microservices = [
      {id:1, name: "fuse-mc", description: "Прокси сервис маршрутизации из ESB в Мобильную карту", stage: 2,
       status : "В разработке", type: "fuse", urls:[{id: 1, stage: "TEST", url: "http://www.fuse.spb.ru", port: 8080},
          {id: 1, stage: "PROD", url: "http://www.fuse.kspd.ru", port: 80}],
      resources: [{id: 1, stage: "TEST", cores: 2, memory: 4, pv: 1},
                  {id: 2, stage: "PROD", cores: 4, memory: 8, pv:1}
                  ]},
      {id:2, name: "eap-mc", description: "Rest сервис для мобильной карты", stage: 2,
        status : "В разработке", type: "eap", urls:[{id: 1, stage: "TEST", url: "http://www.eap.spb.ru", port: 8080},
          {id: 1, stage: "PROD", url: "http://www.eap.kspd.ru", port: 80}],
        resources: [{id: 1, stage: "TEST", cores: 2, memory: 4, pv: 1},
          {id: 2, stage: "PROD", cores: 2, memory: 4, pv:1}
        ]}
    ];

    this.items = [
      {label: 'Бэклог'}, //0
      {label: 'Аналитика'}, //1
      {label: 'В разработке'}, //2
      {label: 'Тестирование'}, //3
      {label: 'Разработана'}, //4
      {label: 'Релиз'}, //5
    ];
  }

  public show(service: Microservice){
    this.selectedService = service;
    console.log('Show id: ' + service.id);
    this.blockEditorForm = true;
    this.showEditorForm = true;
  }

  public add(){
    console.log('add new element');
    this.blockEditorForm = false;
    this.showEditorForm = true;
    this.selectedService = new Microservice();

  }

  public edit(service: Microservice){
    this.selectedService = service;
    console.log('Edit id: ' + service.id);
    this.blockEditorForm = false;
    this.showEditorForm = true;
  }

  public saveSelected(){

  }

}
