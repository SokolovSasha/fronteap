import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WikiInfrastructureComponent } from './wiki-infrastructure.component';

describe('WikiInfrastructureComponent', () => {
  let component: WikiInfrastructureComponent;
  let fixture: ComponentFixture<WikiInfrastructureComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WikiInfrastructureComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WikiInfrastructureComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
