import { Component, OnInit } from '@angular/core';
import {ServerEsb} from "../model/server.model";
import {SelectItem} from "primeng/api";


@Component({
  selector: 'app-wiki-infrastructure',
  templateUrl: './wiki-infrastructure.component.html',
  styleUrls: ['./wiki-infrastructure.component.css']
})
export class WikiInfrastructureComponent implements OnInit {

  constructor() {
  }

  public servers: ServerEsb[];
  cols: any[];
  stages: SelectItem[];


  ngOnInit() {
    this.cols = [
      { field: 'id', header: 'ИД' },
      { field: 'name', header: 'Имя' },
      { field: 'address', header: 'Адрес' },
      { field: 'description', header: 'Описание' },
      { field: 'stage', header: 'Фильтр' }
    ];

    this.stages = [
      { label: 'КСПД', value: 'TEST-KSPD' },
      { label: 'ДМЗ', value: 'TEST-DMZ' },
    ];


    this.servers = [
      {id: 1, name: 'SPB26-ESB-M1D', address: '10.44.211.133', description: 'Мастер узел',stage: 'TEST-KSPD'},
      {id: 2, name: 'SPB26-ESB-M2D', address: '10.44.211.134', description: 'Мастер узел',stage: 'TEST-KSPD'},
      {id: 3, name: 'SPB26-ESB-M3D', address: '10.44.211.146', description: 'Мастер узел',stage: 'TEST-KSPD'},
      {id: 4, name: 'SPB26-ESDMZ-M1D', address: '10.44.211.170', description: 'Мастер узел',stage: 'TEST-DMZ'},
      {id: 5, name: 'SPB26-ESDMZ-M2D', address: '10.44.211.171', description: 'Мастер узел',stage: 'TEST-DMZ'},
      {id: 6, name: 'SPB26-ESDMZ-M3D', address: '10.44.211.174', description: 'Мастер узел',stage: 'TEST-DMZ'},
      {id: 7, name: 'SPB26-ESDMZ-W1D', address: '10.44.211.162', description: 'Рабочий узел',stage: 'TEST-DMZ'},
      {id: 8, name: 'SPB26-ESDMZ-W2D', address: '10.44.211.163', description: 'Рабочий узел',stage: 'TEST-DMZ'},
      {id: 9, name: 'SPB26-ESDMZ-W3D', address: '10.44.211.164', description: 'Рабочий узел',stage: 'TEST-DMZ'},
      {id:10, name: 'SPB26-ESDMZ-W4D', address: '10.44.211.165', description: 'Рабочий узел',stage: 'TEST-DMZ'},
      {id:11, name: 'SPB26-ESDMZ-W5D', address: '10.44.211.166', description: 'Рабочий узел',stage: 'TEST-DMZ'},
      {id:12, name: 'SPB26-ESB-W1D', address: '10.44.211.135', description: 'Рабочий узел',stage: 'TEST-KSPD'},
      {id:13, name: 'SPB26-ESB-W2D', address: '10.44.211.136', description: 'Рабочий узел',stage: 'TEST-KSPD'},
      {id:14, name: 'SPB26-ESB-W3D', address: '10.44.211.137', description: 'Рабочий узел',stage: 'TEST-KSPD'},
      {id:15, name: 'SPB26-ESB-W4D', address: '10.44.211.138', description: 'Рабочий узел',stage: 'TEST-KSPD'},
      {id:16, name: 'SPB26-ESB-W5D', address: '10.44.211.139', description: 'Рабочий узел',stage: 'TEST-KSPD'},
      {id:17, name: 'SPB26-ESB-W6D', address: '10.44.211.140', description: 'Рабочий узел',stage: 'TEST-KSPD'},
      {id:18, name: 'SPB26-ESB-W7D', address: '10.44.211.141', description: 'Рабочий узел',stage: 'TEST-KSPD'},
      {id:19, name: 'SPB26-ESB-W8D', address: '10.44.211.142', description: 'Рабочий узел',stage: 'TEST-KSPD'},
      {id:20, name: 'SPB26-ESB-I1D', address: '10.44.211.130', description: 'Инфраструктурный узел',stage: 'TEST-KSPD'},
      {id:21, name: 'SPB26-ESB-I2D', address: '10.44.211.131', description: 'Инфраструктурный узел',stage: 'TEST-KSPD'},
      {id:22, name: 'SPB26-ESB-I3D', address: '10.44.211.132', description: 'Инфраструктурный узел',stage: 'TEST-KSPD'},
      {id:23, name: 'SPB26-ESDMZ-I1D', address: '10.44.211.167', description: 'Инфраструктурный узел',stage: 'TEST-DMZ'},
      {id:24, name: 'SPB26-ESDMZ-I2D', address: '10.44.211.168', description: 'Инфраструктурный узел',stage: 'TEST-DMZ'},
      {id:25, name: 'SPB26-ESDMZ-I3D', address: '10.44.211.169', description: 'Инфраструктурный узел',stage: 'TEST-DMZ'},
      {id:26, name: 'SPB26-ESDMZFS1D', address: '10.44.211.172', description: 'Файловый сервер',stage: 'TEST-DMZ'},
      {id:27, name: 'SPB26-ESB-FS1D', address: '10.44.211.143', description: 'Файловый сервер',stage: 'TEST-KSPD'},
      ]
  }


  // shell(){
  //   var wsh = new ActiveXObject('WScript.Shell');
  //   command = 'cmd /k ' + 'what ever ...';
  //   wsh.Run(command);
  // }

}
