import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {FormsModule} from "@angular/forms";
import {WikiRoutingModule} from "./wiki-routing.module";
import {WikiComponent} from "./wiki.component";
import {WikiCrtComponent} from "./wiki-crt/wiki.crt.component";
import {WikiEsbComponent} from "./wiki-esb/wiki-esb.component";
import {CopyPasteComponent} from "../../commons/copypaste/copy-paste.component";
import {ButtonModule} from "primeng/button";
import { WikiSystemsComponent } from './wiki-systems/wiki-systems.component';
import { WikiStackComponent } from './wiki-stack/wiki-stack.component';
import { WikiMetricsComponent } from './wiki-metrics/wiki-metrics.component';
import {
    CodeHighlighterModule,
    DialogModule,
    FieldsetModule, InputTextareaModule,
    InputTextModule,MultiSelect, SelectItem,
    StepsModule,
    TabViewModule,
    TooltipModule
} from "primeng/primeng";
import {DataViewModule} from "primeng/dataview";
import { WikiMicroservicesComponent } from './wiki-microservices/wiki-microservices.component';
import {StatusPipe} from "./wiki-systems/StatusPipe";
import {TableModule} from "primeng/table";
import { WikiRequimentsComponent } from './wiki-requiments/wiki-requiments.component';
import { WikiInfrastructureComponent } from './wiki-infrastructure/wiki-infrastructure.component';


@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ButtonModule,
        TooltipModule,
        StepsModule,
        WikiRoutingModule,
        FieldsetModule,
        DataViewModule,
        TableModule,
        TabViewModule,
        DialogModule,
        InputTextModule,
        InputTextareaModule,
        CodeHighlighterModule
    ],
    declarations: [
        MultiSelect,
        WikiComponent,
        WikiCrtComponent,
        WikiEsbComponent,
        CopyPasteComponent,
        WikiSystemsComponent,
        WikiStackComponent,
        WikiMetricsComponent,
        WikiMicroservicesComponent,
        StatusPipe,
        WikiRequimentsComponent,
        WikiInfrastructureComponent,
        // CodeHighlighter
    ]
})
export class WikiModule {

}