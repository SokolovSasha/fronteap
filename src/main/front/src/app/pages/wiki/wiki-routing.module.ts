import {RouterModule, Routes} from "@angular/router";
import {NgModule} from "@angular/core";
import {WikiCrtComponent} from "./wiki-crt/wiki.crt.component";
import {WikiComponent} from "./wiki.component";
import {WikiEsbComponent} from "./wiki-esb/wiki-esb.component";
import {WikiSystemsComponent} from "./wiki-systems/wiki-systems.component";
import {WikiStackComponent} from "./wiki-stack/wiki-stack.component";
import {WikiMetricsComponent} from "./wiki-metrics/wiki-metrics.component";
import {WikiMicroservicesComponent} from "./wiki-microservices/wiki-microservices.component";
import {WikiRequimentsComponent} from "./wiki-requiments/wiki-requiments.component";
import {WikiInfrastructureComponent} from "./wiki-infrastructure/wiki-infrastructure.component";

const wikiRoutes: Routes = [
    {path: '', redirectTo: 'wiki/esb', pathMatch: 'full'},
    {
        path: 'wiki', component: WikiComponent,
        children: [
            {path: 'esb', component: WikiEsbComponent},
            {path: 'crt', component: WikiCrtComponent},
            {path: 'infrastructure', component: WikiInfrastructureComponent},
            {path: 'metrics', component: WikiMetricsComponent},
            {path: 'microservices', component: WikiMicroservicesComponent},
            {path: 'requiments', component: WikiRequimentsComponent},
            {path: 'systems', component: WikiSystemsComponent},
            {path: 'stack', component: WikiStackComponent}
        ]
    }
    ];

@NgModule({
    imports: [
        RouterModule.forChild(wikiRoutes)
    ],
    exports: [
        RouterModule
    ]
})
export class WikiRoutingModule {

}