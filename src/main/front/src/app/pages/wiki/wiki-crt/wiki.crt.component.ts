import {Component} from "@angular/core";
import {MessageService} from "primeng/api";

@Component({
    selector: 'app-wiki-crt',
    templateUrl: './wiki.crt.component.html',
    styleUrls: ['./wiki.crt.component.css'],
    providers: [MessageService]
})
export class WikiCrtComponent {

    generateKeyPairCommand = 'keytool -genkeypair -keyalg RSA -alias "mcrest" -dname "CN=fuse-mc-rest.mobilcard.svc, OU=Architect, O=EmDev, L=SPB, C=RU" -ext SAN=dns:fuse-mc-rest-mobilcard.kspd-apps.demo.rht.ru,dns:kspd-apps.demo.rht.ru -keystore "mcks.jks" -storepass "changeit" -keypass "changeit" ';


    generateCaKey = 'openssl genrsa -out rootCA.key 2048';

    generateCaCert = 'openssl req -x509 -new -key rootCA.key -days 10000 -out rootCA.crt';

    generateAppKey = 'openssl genrsa -out amq.key 2048';

    signRequest = 'openssl req -new -key amq.key -out amq.csr';

    signCommand = 'openssl x509 -req -in amq.csr -CA rootCA.crt -CAkey rootCA.key -CAcreateserial -out amq.crt -days 5000';

    generateP12keystore = 'openssl pkcs12 -export -in amq.crt -inkey amq.key -name "amqkey" -out keystore.p12';

    importP12 = 'keytool -importkeystore -srckeystore keystore.p12 -srcstoretype pkcs12 -destkeystore keystore.jks -deststoretype JKS';

    importCaCert = 'keytool -import -alias "root" -keystore "keystore.jks" -file "rootCA.crt" -storepass "changeit" -trustcacerts -noprompt';

    importAppCert = 'keytool -import -alias "amq" -keystore "keystore.jks" -file "amq.crt" -storepass "changeit" -trustcacerts -noprompt';

    generateAppSecret = 'oc create secret generic amq-server-jks --from-file="/tmp/aaa/keystore.jks"';

    clientCertAdd = 'keytool -import -alias "root" -keystore "client.ts" -file "rootCA.crt" -storepass "changeit" -trustcacerts -noprompt';
    clientCertAdd2 = 'keytool -import -alias "amq" -keystore "client.ts" -file "amq.crt" -storepass "changeit" -trustcacerts -noprompt';

    generateClientSecret = 'oc create secret generic amq-client --from-file="/tmp/aaa/client.ts"';

    mounting ='volumeMounts:\n' +
        '            - mountPath: /var/run/secrets/amq/serverkeystores\n' +
        '              name: amq-server\n' +
        '\n' +
        '      volumes:\n' +
        '        - name: amq-server\n' +
        '          secret:\n' +
        '            defaultMode: 420\n' +
        '            secretName: amq-server-secret\n';

}