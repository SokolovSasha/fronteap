import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {environment} from "../../environments/environment";
import {Observable} from "rxjs";
import {McAzs} from "../model/mc/mcAzs.model";
import {McShift} from "../model/mc/mcShift.model";
import {McShiftReport} from "../model/mc/mcShiftReport.model";
import {Microservice} from "../pages/wiki/model/microservice.model";

@Injectable()
export class EsbBackService {
    constructor(private http: HttpClient) { }

    private serviceURL = environment.esbBackUrl;
    private esbServices = this.serviceURL + '/services';

    public getServicesList(): Observable<Microservice[]> {
        return this.http.get<Microservice[]>(this.esbServices + '/getList' + '?');
    }

    // public getShiftsByDateRange(azsId: number, startDate: string, endDate: string): Observable<McShift[]> {
    //     return this.http.get<McShift[]>(this.mcGetShiftByDateUrl + '?azsId=' + azsId +
    //         '&startDate=' + startDate + '&endDate=' + endDate);
    // }
    //
    // public getShiftReport(azsId: number, startShiftId: number, endShiftId: number): Observable<McShiftReport> {
    //     return this.http.get<McShiftReport>(this.mcGetShiftReportUrl + '?azsId=' + azsId
    //         + '&startId=' + startShiftId + '&endId=' + endShiftId);
    // }

}