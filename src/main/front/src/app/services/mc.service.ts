import {Injectable} from "@angular/core";
import {HttpClient, HttpHeaders, HttpParams} from "@angular/common/http";
import {environment} from "../../environments/environment";
import {Observable} from "rxjs";
import {AzsList} from "../model/azs-list.model";
import {McAzs} from "../model/mc/mcAzs.model";
import {McShift} from "../model/mc/mcShift.model";
import {McShiftReport} from "../model/mc/mcShiftReport.model";

@Injectable()
export class McService {
    constructor(private http: HttpClient) {

        this.httpOptions = {
            headers: new HttpHeaders({
                'accept': 'application/json',
                'content-type': 'application/json',
                'Authorization': 'Basic: bWM6bWM'
            })
        };
    }

    private httpOptions: {headers?: HttpHeaders | {
            [header: string]: string | string[];
        }
    };

    private serviceURL = environment.mcUrl;
    private mcGetAzsUrl = this.serviceURL + '/mc/api/getAzsList';
    private mcGetShiftByDateUrl = this.serviceURL + '/mc/api/getShiftsByDateRange';
    private mcGetShiftReportUrl = this.serviceURL + '/mc/api/getShiftReport';

    public getAzsList(): Observable<McAzs[]> {
        return this.http.get<McAzs[]>(this.mcGetAzsUrl, this.httpOptions);
    }

    public getShiftsByDateRange(azsId: number, startDate: string, endDate: string): Observable<McShift[]> {
        return this.http.get<McShift[]>(this.mcGetShiftByDateUrl + '?azsId=' + azsId +
            '&startDate=' + startDate + '&endDate=' + endDate, this.httpOptions);
    }

    public getShiftReport(azsId: number, startShiftId: number, endShiftId: number): Observable<McShiftReport> {
        return this.http.get<McShiftReport>(this.mcGetShiftReportUrl + '?azsId=' + azsId
        + '&startId=' + startShiftId + '&endId=' + endShiftId, this.httpOptions);
    }

}