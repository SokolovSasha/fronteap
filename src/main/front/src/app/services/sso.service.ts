import {Injectable} from '@angular/core';
import {Router} from "@angular/router";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Observable} from "rxjs";
import {TokenResponseModel} from "../model/auth/token-response.model";
import {UserInfoModel} from "../model/auth/user-info.model";

@Injectable()
export class SsoService {
    token: string;
    private tokenUri = 'http://localhost:8080/auth/sso/token2';
    private ssoAdapterUrl = 'http://localhost:8080/auth/sso';
    constructor(private router: Router, private http: HttpClient) {
    }
    public getTokenByAuthCode2(code: string): Observable<TokenResponseModel> {
            console.log('Запросили токен по CODE: ' + code);
        return this.http.get<TokenResponseModel>(this.tokenUri + '?authCode2=' + code);
    }

    public getUserInfo(bearerToken: string): Observable<UserInfoModel> {
        const httpOptions = {
            headers: new HttpHeaders({
                'accept': 'application/json',
                'content-type': 'application/json',
                'X-BEARER-TOKEN': bearerToken
            })
        };

        return this.http.get<UserInfoModel>(this.ssoAdapterUrl + '/userInfo', httpOptions);
    }
}
