import {Router} from "@angular/router";
import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {environment} from "../../environments/environment";
import {LoginrequestModel} from "../model/auth/loginrequest.model";
import {Observable} from "rxjs";
import {CorrelationId} from "../model/correlation-id.model";
import {TokenResponseModel} from "../model/auth/token-response.model";
import {LogoutRequestModel} from "../model/auth/logout-request.model";
import {AuthRolesModel} from "../model/auth/auth-roles.model";

@Injectable()
export class AuthService {
    token: string;
    refreshToken: string;
    arrayRoles: string[];
    codeMode: boolean = false;
    public roles: AuthRolesModel;
    private serviceURL = environment.authUrl;
    private authURL = this.serviceURL + '/auth/auth';
    private auth2fURL = this.serviceURL + '/auth/auth2fa';
    private checkURL = this.serviceURL + '/auth/checkKey';
    private rolesURL = this.serviceURL + '/auth/roles';
    private getResponseURL = this.serviceURL + '/auth/getResponse';
    private logoutUrl = this.serviceURL + '/auth/logout';

    private kspdServiceUrl = environment.authKspdUrl;
    constructor(private router: Router, private http: HttpClient) {
        // this.token = 'token mocken!';
    }

    /**
     * Авторизация пользователя. Выполняется асинхронно в два шага.
     * @param email - он же username
     * @param password - пароль.
     */
    public signinUser (email: string, password: string) {
        this.token = null;
        this.authUser(email, password).subscribe(
            (response: CorrelationId) => {
                let corId = response.correlationId;
                console.log('COR_ID: ' + corId);
                this.asyncGetCircout(corId);

            },
            error => {
                        console.log('Ошибка обращения к сервису авторизации!');
                        console.log(error);
                    }
            );
    }

    /**
     * Авторизация пользователя. Выполняется асинхронно в два шага.
     * @param email - он же username
     * @param password - пароль.
     */
    public signin2faUser (login: string, password: string) {
        this.token = null;
        this.authUser2fa(login, password).subscribe(
            (response: CorrelationId) => {
                let corId = response.correlationId;
                console.log('COR_ID: ' + corId);
                if (response.correlationId != null) {
                    this.codeMode = true;
                }

            },
            error => {
                console.log('Ошибка обращения к сервису авторизации!');
                console.log(error);
            }
        );
    }

    public signInKspd(login: string, password: string) {
        this.token = null;
        this.authUser2fa(login, password).subscribe(
            (response: CorrelationId) => {
                let corId = response.correlationId;
                console.log('COR_ID: ' + corId);
                if (response.correlationId != null) {
                    this.codeMode = true;
                }

            },
            error => {
                console.log('Ошибка обращения к сервису авторизации!');
                console.log(error);
            }
        );
    }


    public checkToken (secretCode: string) {
        this.getAuthTokenByCode(secretCode).subscribe(
            (response: any) => {
                console.log('Пришел ответ по secretCode: ' + secretCode);
                if (response.access_token != null && this.token === null) {
                    this.token = response.access_token;
                    this.refreshToken = response.refresh_token;
                    this.getRoles();
                    console.log('Авторизовались!');
                }

                if (response.code != null) {
                    console.log('Код:' + response.code + '  ' + response.description);
                }

            },
            error => {
                console.log('Ошибка обращения к сервису: ' + error);
            }
        );
    }

    /**
     * Разлогинить пользователя. И на бэке, и на фронте.
     */
    public logout (){
        console.log('Выход из аккаунта');
        if(this.token != null && this.refreshToken != null){
            this.logoutPost().subscribe(
                ( ) => {
                    console.log('Выполняется разлогирование!');
                    },error1 => {
                    console.log('Ошибка обращения к сервису авторизации!');
                    console.log(error1);
                }, () => {
                    this.token = null;
                    this.refreshToken = null;
                    this.codeMode = false;
                    this.roles = null;
                    this.arrayRoles = null;
                    console.log('Пользователь разлогинен!');
                }

            )
        }
    }

    public async getRoles(){
        console.log('Получение ролей');
        this.roles = null;
        if(this.token != null){
            this.postRequestRoles().subscribe(
                (response: CorrelationId) => {
                    let corId = response.correlationId;
                    // let corId = '123';
                    console.log('COR_ID: ' + corId);
                    this.asyncGetRolesCircout(corId);
                },error1 => {
                    console.log('Ошибка обращения к сервису авторизации!');
                    console.log(error1);
                }
            )
        }
    }

    //методы могут отличаться различными условиями выхода из цикла, количеством попыток и задержками по времени.
    private async asyncGetCircout(correlationId: string){
        console.log('Получение токена. Пока токен:' + this.token);
        let i = 0;
        while (i < 5){
            i++;
            if(this.token != null){
                this.getRoles();
                return;
            }
            console.log('Запуск попытки: ' + i);
            await this.delayedGetResponse(100, correlationId, i);
            console.log('Конец цикла');
        }
    }

    private async asyncGetRolesCircout(correlationId: string){
        console.log('Получение ролей. Пока роли:' + this.roles);
        let i = 0;
        while (i < 5){
            i++;
            if(this.roles != null){
                return;
            }
            console.log('Запуск попытки: ' + i);
            await this.delayedGetResponse(100, correlationId, i);
            console.log('Конец цикла');
        }
    }

    private async asyncGetMailSendResponse(correlationId: string){
        console.log('Получение статуса отправки кода');
        let i = 0;
        while (i < 5){
            i++;
            if(this.codeMode != false){
                return;
            }
            console.log('Запуск попытки: ' + i);
            await this.delayedGetResponse(100, correlationId, i);
            console.log('Конец цикла');
        }
    }

    private async delayedGetResponse(ms: number, correlationId: string, attempt: number) {
        await new Promise((resolve) => {setTimeout(()=>resolve(), ms*attempt);
            console.log("Выждали " + ms*attempt + ' мс');
            this.getRequest(correlationId).subscribe(
                (response: any) => {
                    console.log('Пришел ответ по corId: ' + correlationId);
                    if (response.access_token != null && this.token === null) {
                        this.token = response.access_token;
                        this.refreshToken = response.refresh_token;
                        console.log('Токен получен с попытки: ' + attempt);
                    }

                    if (response.roles != null) {
                        console.log('ROLES: ' + response.roles);
                        console.log('ИМЯ: ' + response.preferred_username);
                        this.roles = response;
                        this.arrayRoles = response.roles;
                        // this.arrayRoles = this.splitRoles(response);


                    }

                    if (response.code != null) {
                        console.log('Код:' + response.code + '  ' + response.description);
                        console.log('Неуспешная попытка: ' + attempt);
                    }

                },
                error => {
                    console.log('Ошибка обращения к сервису: ' + error);
                }
            );
        })
    }

    private splitRoles(response: any) :string[] {
        if (response.roles === null) {
            return null;
        }
        let inroles = response.roles;
        let cutted = inroles.replace('[', '');
        let cutted2 = cutted.replace(']', '');

        return cutted2.split(',')
    }

    private authUser(login: string, password: string): Observable<CorrelationId> {
        let body = new LoginrequestModel();
        body.username = login;
        body.password = password;
        console.log('LOGIN' + body.username + '  ' + login);
            return this.http.post<CorrelationId>(this.authURL, body);

    }

    private authUser2fa(login: string, password: string): Observable<CorrelationId> {
        let body = new LoginrequestModel();
        body.username = login;
        body.password = password;
        console.log('LOGIN' + body.username);
        return this.http.post<CorrelationId>(this.auth2fURL, body);

    }

    private authKspd(login: string, password: string) {
        let body = new LoginrequestModel();
        body.username = login;
        body.password = password;
        // return this.http.post()
    }

    private logoutPost(){
        let body = new LogoutRequestModel();
        body.accessToken = this.token;
        body.refreshToken = this.refreshToken;
        return this.http.post(this.logoutUrl, body);
    }

    private postRequestRoles(): Observable<CorrelationId>{
        if (this.isAuthenticated()){
            let body = {accessToken: this.token};
            return this.http.post<CorrelationId>(this.rolesURL, body);
        }
        return null;
    }

    private getRolesRequest(correlationId: string): Observable<AuthRolesModel> {
        console.log('Запросили роли по COR_ID ' + correlationId);
        return this.http.get<AuthRolesModel>(this.getResponseURL + '/' + correlationId);
    }

    private getAuthToken(correlationId: string): Observable<TokenResponseModel> {
        console.log('Запросили токен по COR_ID ' + correlationId);
        return this.http.get<TokenResponseModel>(this.getResponseURL + '/' + correlationId);
    }

    private getAuthTokenByCode(secretCode: string): Observable<TokenResponseModel> {
        console.log('Запросили токен по коду ' + secretCode);
        return this.http.get<TokenResponseModel>(this.checkURL + '/' + secretCode);
    }

    private getRequest(correlationId: string): Observable<Object> {
        console.log('Запросили Object по COR_ID ' + correlationId);
        return this.http.get<Object>(this.getResponseURL + '/' + correlationId);
    }

    isAuthenticated() {
        return this.token != null;
    }

    isCodeMode() {
        return this.codeMode === true;
    }
}