import {HttpClient, HttpHeaders} from "@angular/common/http";
import {environment} from "../../environments/environment";
import {Observable} from "rxjs";
import {InvoiceRequest} from "../model/etran/invoice-request.model";
import {Injectable} from "@angular/core";


@Injectable()
export class EtranService {
    constructor(private http: HttpClient) {
    }

    private serviceURL = environment.etranUrl;
    private invoidUrl = this.serviceURL + '/etran/api/getInvoice';

    public getInvoice(requestBody: InvoiceRequest): Observable<Object> {
        //stub - возвращает заглушку
        // const httpOptions = {
        //    headers: new HttpHeaders({
        //        'accept': 'application/json',
        //        'content-type': 'application/json',
        //        'Stub': 'notnull'
        //     })
        // };

        return this.http.post<Object>(this.invoidUrl, requestBody);
    }
}