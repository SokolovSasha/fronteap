import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Injectable} from "@angular/core";
import {environment} from "../../environments/environment";
import {AuthRequest} from "../model/ofd/AuthRequest";
import {AuthResponse} from "../model/ofd/AuthResponse";
import {KktsListResponse} from "../model/ofd/KktsListResponse";
import {Observable} from "rxjs";
import {DetailedReceiptsListWithStatResponse} from "../model/ofd/DetailedReceiptsListWithStatResponse";

@Injectable()
export class OfdService {
    constructor(private http: HttpClient) {

    }

    private serviceURL = environment.ofdUrl;
    private authUrl = this.serviceURL + '/ofd/api/authorization/createAuthToken';
    private innUrl = this.serviceURL + '/ofd/api/inn/';
    private authToken: string;
    private tokenExpDate: Date;

    private httpOptions: {headers?: HttpHeaders | {
            [header: string]: string | string[];
        }
    };


    /**
     * Авторизация.
     * AHTUNG:  Хард-код логин пароля.
     */
    public authorise(){
        this.authToken = null;
        const login = 'esb';
        const password = 'esb123';
        this.postAuth(login, password).subscribe(
            (response: AuthResponse) => {
                if (response != null){
                    this.authToken = response.AuthToken;
                    this.tokenExpDate = response.ExpirationDateUtc;
                }

            },
            error  => {
                console.log('Ошибка обращения к сервису авторизации!');
                console.log(error);
            }
        );
    }

    public logout(){
        this.authToken = null;
    }



    private postAuth(login: string, password: string):Observable<AuthResponse>{
        let body = new AuthRequest();
        body.login = login;
        body.password = password;
        return this.http.post<AuthResponse>(this.authUrl, body);
    }

    public getKkts(inn: string): Observable<KktsListResponse>{
        let httpOptions = {
            headers: new HttpHeaders({
                'accept': 'application/json',
                'content-type': 'application/json',
                'AuthToken': this.authToken
            })
        };
        return this.http.get<KktsListResponse>(this.innUrl + inn + '/kkts', httpOptions);
    }

    public getReceiptsStatistic(inn: string, kkt: string,  marker: string, dateFrom: Date, dateTo: Date):Observable<DetailedReceiptsListWithStatResponse> {
        let httpOptions = {
            headers: new HttpHeaders({
                'accept': 'application/json',
                'content-type': 'application/json',
                'AuthToken': this.authToken
            })
        };

        let pathstring = this.parseDates(dateFrom, dateTo);
        return this.http.get<DetailedReceiptsListWithStatResponse>(this.innUrl + inn + '/kkt/' + kkt +
            '/statistics' + pathstring +
            '&marker='+marker, httpOptions);

    }

    private parseDates (dateFrom_in: Date, dateTo_in: Date): string {
        let parsed = '?dateFrom=' + dateFrom_in.getFullYear() + '-' + dateFrom_in.getMonth()
            + '-' + dateFrom_in.getDate() + 'T' + this.localise(dateFrom_in.getHours())+':'
            + this.localise( dateFrom_in.getMinutes())+ ':' + '00' +
            '&dateTo=' + dateTo_in.getFullYear() + '-' + dateTo_in.getMonth() + '-' + dateTo_in.getDate() + 'T'+
            this.localise(dateTo_in.getHours())+':' + this.localise(dateTo_in.getMinutes()) +':' + '00';
        return parsed;
    }

    private localise(digit: number): string{
        return digit.toLocaleString('ru-RU', { minimumIntegerDigits: 2 });
    }



    public getReceiptsList(inn: string, kkt: string, dateFrom: Date, dateTo) {
        let httpOptions = {
            headers: new HttpHeaders({
                'accept': 'application/json',
                'content-type': 'application/json',
                'AuthToken': this.authToken
            })
        };
        return this.http.get<KktsListResponse>(this.innUrl + inn + '/kkts', httpOptions);

    }



    isAuthenticated():boolean {
        return this.authToken != null;
    }






}